﻿using DashboardVisualization.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;

namespace DashboardVisualization.UserControls
{
    public partial class UcMain : UserControl
    {
        bool skipDatePickerEventHandling;

        public UcMain()
        {
            InitializeComponent();
        }
               

        /// <summary>
        /// Main form data validation
        /// </summary>
        /// <returns></returns>
        public bool ValidateInputs()
        {
            bool inputsValid = true;
            string InvalidField = string.Empty;


            #region project details tab validation

            //validation for  project name
            if (string.IsNullOrWhiteSpace(txtProjectName.Text))
            {
                InvalidField = string.Concat(InvalidField, "\t", Constants.TabProjectDetails, " > Project Name", Constants.NewLineChar);

                //MessageBox.Show("Please enter project name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //txtProjectName.Focus();
                //inputsValid = false;
            }

            //validation for category
            //if (cmbCategory.SelectedItem != null && string.IsNullOrWhiteSpace(cmbCategory.SelectedItem.ToString()))
            //{
            //    InvalidField = string.Concat(InvalidField, "\t", Constants.TabProjectDetails, " > Category", Constants.NewLineChar);

            //    //MessageBox.Show("Please enter category.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    //txtCategory.Focus();
            //    //inputsValid = false;
            //}

            // validation for other category
            if (cmbCategory.SelectedItem != null && (cmbCategory.SelectedItem.ToString().Equals("Other") && string.IsNullOrWhiteSpace(txtOtherCategory.Text)))
            {
                InvalidField = string.Concat(InvalidField, "\t", Constants.TabProjectDetails, " > Specify Other Category", Constants.NewLineChar);
            }

            //validation for study duration
            //if (string.IsNullOrWhiteSpace(txtStudyDuration.Text))
            //{
            //    InvalidField = string.Concat(InvalidField, "\t", Constants.TabProjectDetails, " > Study Duration", Constants.NewLineChar);               
            //}

            #endregion

            #region studyInfo tab validation

            if (string.IsNullOrWhiteSpace(txtCSLName.Text))
            {
                InvalidField = string.Concat(InvalidField, "\t", Constants.TabStudyInfo, " > CSL Name", Constants.NewLineChar);              
            }

            if (string.IsNullOrWhiteSpace(txtGCLName.Text))
            {
                InvalidField = string.Concat(InvalidField, "\t", Constants.TabStudyInfo, " > GCL Name", Constants.NewLineChar);
            }

            if (string.IsNullOrWhiteSpace(txtCSSName.Text))
            {
                InvalidField = string.Concat(InvalidField, "\t", Constants.TabStudyInfo, " > CSS Name", Constants.NewLineChar);
            }

            if(cmbCRO.SelectedItem == null)
            {
                InvalidField = string.Concat(InvalidField, "\t", Constants.TabStudyInfo, " > CRO", Constants.NewLineChar);
            }

            if (cmbCountry.SelectedItem == null)
            {
                InvalidField = string.Concat(InvalidField, "\t", Constants.TabStudyInfo, " > Country", Constants.NewLineChar);
            }

            // validation for other CRO
            if (cmbCRO.SelectedItem != null && (cmbCRO.SelectedItem.ToString().Equals("Other") && string.IsNullOrWhiteSpace(txtOtherCRO.Text)))
            {
                InvalidField = string.Concat(InvalidField, "\t", Constants.TabStudyInfo, " > Specify Other CRO", Constants.NewLineChar);
            }

            // validation for other country
            if ((cmbCountry.SelectedItem != null) && cmbCountry.SelectedItem.ToString().Equals("Other") && string.IsNullOrWhiteSpace(txtOtherCountry.Text))
            {
                InvalidField = string.Concat(InvalidField, "\t", Constants.TabStudyInfo, " > Specify Other Country", Constants.NewLineChar);
            }

            //validation for data management
            if (chkDataManagement.Checked && string.IsNullOrWhiteSpace(txtDataManagement.Text))
            {
                InvalidField = string.Concat(InvalidField, "\t", Constants.TabStudyInfo, " > Data Management", Constants.NewLineChar);              
            }

            //validation for CDISC
            if (chkCDISC.Checked && string.IsNullOrWhiteSpace(txtCDISC.Text))
            {
                InvalidField = string.Concat(InvalidField, "\t", Constants.TabStudyInfo, " > CDISC", Constants.NewLineChar);
            }

            //validation for statistics
            if (chkStatisics.Checked && string.IsNullOrWhiteSpace(txtStatisics.Text))
            {
                InvalidField = string.Concat(InvalidField, "\t", Constants.TabStudyInfo, " > Statistics", Constants.NewLineChar); 
            }

            #endregion

            if(cmbStudyStatus.SelectedItem == null)
            {
                InvalidField = string.Concat(InvalidField, "\t", "Milestones", " > Study Status", Constants.NewLineChar);
            }

            if (!string.IsNullOrWhiteSpace(InvalidField))
            {
                InvalidField = string.Concat("Values are not specified for the following fields:", Constants.NewLineChar,  InvalidField);
                MessageBox.Show(InvalidField, Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                inputsValid = false;
                tbStudyInfo.Show();
            }

            return inputsValid;
        }


        /// <summary>
        /// collect main form data into string seperated by ###
        /// </summary>
        public ProjectDetails CollectData()
        {
            Constants.MainFormData = string.Empty;
            string ProjDetailsTabData = string.Empty, StudyInfoTabData = string.Empty, MilestoneTabData = string.Empty, CommentTabData = string.Empty;

            //ProjDetailsTabData = String.Join(Constants.Seperator, txtProjectName.Text, txtCategory.Text, txtDescription.Text, txtCSSName.Text);
            ProjDetailsTabData = String.Join(Constants.Seperator, txtProjectName.Text, cmbCategory.SelectedItem.ToString().Equals("Other") ? txtOtherCategory.Text : cmbCategory.SelectedItem.ToString(),  txtDescription.Text);

            //StudyInfoTabData = String.Join(Constants.Seperator, dtAssignedOn.Text, chkDataManagement.Checked.ToString(), txtDataManagement.Text, 
            //                    chkCDISC.Checked.ToString(),txtCDISC.Text,chkStatisics.Checked.ToString(),txtStatisics.Text);
            //Updated by Chetna
            StudyInfoTabData = String.Join(Constants.Seperator, txtCSLName.Text, txtGCLName.Text, txtCSSName.Text, 
                                cmbCRO.SelectedItem.ToString().Equals("Other") ? txtOtherCRO.Text : cmbCRO.SelectedItem.ToString(), 
                                cmbCountry.SelectedItem.ToString().Equals("Other") ? txtOtherCountry.Text : cmbCountry.SelectedItem.ToString(), 
                                chkDataManagement.Checked ? "Yes": "No", txtDataManagement.Text,
                                chkCDISC.Checked ? "Yes" : "No", txtCDISC.Text, 
                                chkStatisics.Checked ? "Yes" : "No", txtStatisics.Text);
           //Collect Actuals if Visible
          
            MilestoneTabData = String.Join(Constants.Seperator,cmbStudyStatus.Text, dtAssignedOn.Text, 
                                dtPlanedGoLive.Text, dtActualGoLive.Text, 
                                dtPlanedFPFV.Text, dtActualFPFV.Text,
                                //dtPlanedLPFV.Text, dtActualLPFV.Text, 
                                //txtStudyDuration.Text, 
                                dtPlanedLPLV.Text, dtActualLPLV.Text, 
                                dtPlanedDBL.Text, dtActualDBL.Text,
                                dtActualSDTM.Text, dtActualSDTM.Text, 
                                dtPlanedSAP.Text, dtActualSAP.Text, 
                                dtActualBDR.Text, dtActualBDR.Text,
                                dtActualBDRReview.Text, dtActualBDRReview.Text, 
                                dtPlanedSAR.Text, dtActualSAR.Text,
                                //Days
                                txtPlanedGoLive.Text, txtActualGoLive.Text, 
                                txtPlanedFPFV.Text, txtActualFPFV.Text,
                                //txtPlanedLPFV.Text, txtActualLPFV.Text, 
                                txtPlanedLPLV.Text, txtActualLPLV.Text, 
                                txtPlanedDBL.Text, txtActualDBL.Text,
                                txtPlanedSDTM.Text, txtActualSDTM.Text, 
                                txtPlanedSAP.Text, txtActualSAP.Text, 
                                txtActualBDR.Text, txtPlanedBDR.Text, 
                                txtPlanedBDRReview.Text, txtActualBDRReview.Text, 
                                txtPlanedSAR.Text, txtActualSAR.Text);

            CommentTabData = String.Join(Constants.Seperator, txtClientComment.Text, txtCytelComment.Text);

            //concat all tab data
            Constants.MainFormData = string.Concat(ProjDetailsTabData, Constants.Seperator, StudyInfoTabData, Constants.Seperator, MilestoneTabData, Constants.Seperator, CommentTabData);

            // validate inputs

            // after successful validation, get project details
            return GetProjectDetails();
        }

        /// <summary>
        /// Enables textbox on checked og checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkDataManagement_CheckedChanged(object sender, EventArgs e)
        {
            txtDataManagement.Enabled = chkDataManagement.Checked ? true : false;
            if(chkDataManagement.Checked == false) txtDataManagement.Text = string.Empty;
        }

        /// <summary>
        /// Enables textbox on checked og checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkCDISC_CheckedChanged(object sender, EventArgs e)
        {
            txtCDISC.Enabled = chkCDISC.Checked ? true : false;
            if(chkCDISC.Checked == false) txtCDISC.Text = string.Empty;
        }

        /// <summary>
        /// Enables textbox on checked og checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkStatisics_CheckedChanged(object sender, EventArgs e)
        {
            txtStatisics.Enabled = chkStatisics.Checked ? true : false;
            if(chkStatisics.Checked == false) txtStatisics.Text = string.Empty;
        }


        List<Control> DatePickerControls;

        /// <summary>
        /// set current date to all datetimepicker control
        /// </summary>
        /// <param name="control"></param>
        public void SetDefaultDates(bool initializeActualDates = false)
        {
            // set default study duration
            //int studyDuration = string.IsNullOrEmpty(txtStudyDuration.Text) ? 0 : Convert.ToInt32(txtStudyDuration.Text);

            DatePickerControls = new List<Control>();

            GetAllDateTimePickers(this);

            skipDatePickerEventHandling = true;

            foreach (var datePicker in DatePickerControls)
            {
                ((DateTimePicker)datePicker).Value = DateTime.Now;
            }

            // Setting default planned dates
            // Planned LPLV = Planned LPFV + Study duration
            //dtPlanedLPLV.Value = dtPlanedLPFV.Value.AddDays(studyDuration);

            // Planned DBL = Planned LPLV + 4 working days
            dtPlanedDBL.Value = dtPlanedLPLV.Value.AddWorkdays(4);

            // Planned CDISC = Planned DBL + 6 
            dtPlanedSDTM.Value = dtPlanedDBL.Value.AddWorkdays(6);

            // Planned SAP = Similar to DBL
            dtPlanedSAP.Value = dtPlanedDBL.Value;

            // Planned BDR = Planned CDISC + 3
            dtPlanedBDR.Value = dtPlanedSDTM.Value.AddWorkdays(3);

            // Planned BDR Review = Planned BDR + 2
            dtPlanedBDRReview.Value = dtPlanedBDR.Value.AddWorkdays(2);

            // Planned SAR = Planned BDR Review + 8
            dtPlanedSAR.Value = dtPlanedBDRReview.Value.AddWorkdays(8);

            // set actual dates same as planned dates
            if (initializeActualDates)
            {
                dtActualBDR.Value = dtPlanedBDR.Value;
                dtActualBDRReview.Value = dtPlanedBDRReview.Value; ;
                dtActualDBL.Value = dtPlanedDBL.Value;
                dtActualFPFV.Value = dtPlanedFPFV.Value;
                dtActualGoLive.Value = dtPlanedGoLive.Value;
                //dtActualLPFV.Value = dtPlanedLPFV.Value;
                dtActualLPLV.Value = dtPlanedLPLV.Value;
                dtActualSAP.Value = dtPlanedSAP.Value;
                dtActualSAR.Value = dtPlanedSAR.Value;
                dtActualSDTM.Value = dtPlanedSDTM.Value;
            }

            skipDatePickerEventHandling = false;

            //set default value to set no of days text box
            //chkDefaultDays.Checked = true;
            //txtDefaultDays.Enabled = true;
            //txtDefaultDays.Text = "5";            
        }
        
        /// <summary>
        /// This is a generic method that can run across all container controls inside given container control parameter,
        /// and add all datepicker controls to a collection.
        /// </summary>
        /// <param name="container">represents a container usercontrol to search datepicker controls</param>
        private void GetAllDateTimePickers(Control container)
        {
            foreach (Control control in container.Controls)
            {
                if (control is Panel || control is GroupBox || control is TableLayoutPanel || control is TabControl || control is TabPage) 
                    GetAllDateTimePickers(control);

                if(control is DateTimePicker)
                    DatePickerControls.Add(control);
            }
        }

        /// <summary>
        /// This method is used for setting all input fields to project parameters.
        /// It is called for edit project operation.
        /// </summary>
        /// <param name="projectData">Represent project object containing details of project being edited.</param>
        internal void SetProjectDetails(ProjectDetails projectData)
        {
            txtProjectName.Enabled = false;

            txtProjectName.Text = projectData.ProjectDescription.ProjectName;
            //cmbCategory.SelectedItem = projectData.ProjectDescription.ProjectCategory;
            txtDescription.Text = projectData.ProjectDescription.Description;
            //txtStudyDuration.Text = projectData.ProjectDescription.StudyDuration;
            //Constants.studyDurationValue = Convert.ToInt16(projectData.ProjectDescription.StudyDuration);
            //cmbStudyDuration.SelectedIndex = projectData.ProjectDescription.StudyDurationUnit;
            //txtCSSName.Text = projectData.ProjectDescription.CSSName;

            if (cmbCategory.Items.Contains(projectData.ProjectDescription.ProjectCategory))
            {
                cmbCategory.SelectedItem = projectData.ProjectDescription.ProjectCategory;
                txtOtherCategory.Visible = false;
            }
            else
            {
                cmbCategory.SelectedItem = "Other";
                txtOtherCategory.Text = projectData.ProjectDescription.ProjectCategory;
                txtOtherCategory.Visible = true;
            }

            //dtAssignedOn.Value = projectData.StudyInfo.;
            txtCSLName.Text = projectData.StudyInfo.CSLName;
            txtGCLName.Text = projectData.StudyInfo.GCLName;
            txtCSSName.Text = projectData.StudyInfo.CSSName;

            if (cmbCRO.Items.Contains(projectData.StudyInfo.Cro))
            {
                cmbCRO.SelectedItem = projectData.StudyInfo.Cro;
                txtOtherCRO.Visible = false;
            }
            else
            {
                cmbCRO.SelectedItem = "Other";
                txtOtherCRO.Text = projectData.StudyInfo.Cro;
                txtOtherCRO.Visible = true;
            }

            if (cmbCountry.Items.Contains(projectData.StudyInfo.Country))
            {
                cmbCountry.SelectedItem = projectData.StudyInfo.Country;
                txtOtherCountry.Visible = false;
            }
            else
            {
                cmbCountry.SelectedItem = "Other";
                txtOtherCountry.Text = projectData.StudyInfo.Country;
                txtOtherCountry.Visible = true;
            }

            chkDataManagement.Checked = projectData.StudyInfo.DataManagementAwarded;
            chkCDISC.Checked = projectData.StudyInfo.CDISCAwarded;
            chkStatisics.Checked = projectData.StudyInfo.StatisticsAwarded;
            txtDataManagement.Text = projectData.StudyInfo.DataManagementPoCName; ;
            txtCDISC.Text = projectData.StudyInfo.CDISCPoCName; ;
            txtStatisics.Text = projectData.StudyInfo.StatisticsPoCName; ;

            cmbStudyStatus.Text = projectData.Milestones.StudyStatus;
            dtActualGoLive.Value = projectData.Milestones.ActualGoLive;
            dtPlanedGoLive.Value = projectData.Milestones.PlannedGoLive;
            dtActualFPFV.Value = projectData.Milestones.ActualFPFV;
            dtPlanedFPFV.Value = projectData.Milestones.PlannedFPFV;
            //dtActualLPFV.Value = projectData.Milestones.ActualLPFV;
            //dtPlanedLPFV.Value = projectData.Milestones.PlannedLPFV;
            dtActualLPLV.Value = projectData.Milestones.ActualLPLV;
            dtPlanedLPLV.Value = projectData.Milestones.PlannedLPLV;
            dtActualDBL.Value = projectData.Milestones.ActualDBL;
            dtPlanedDBL.Value = projectData.Milestones.PlannedDBL;
            dtActualSDTM.Value = projectData.Milestones.ActualCDISCSDTM;
            dtPlanedSDTM.Value = projectData.Milestones.PlannedCDISCSDTM;
            dtActualSAP.Value = projectData.Milestones.ActualSAP;
            dtPlanedSAP.Value = projectData.Milestones.PlannedSAP;
            dtActualBDR.Value = projectData.Milestones.ActualBDR;
            dtPlanedBDR.Value = projectData.Milestones.PlannedBDR;
            dtActualBDRReview.Value = projectData.Milestones.ActualBDRReview;
            dtPlanedBDRReview.Value = projectData.Milestones.PlannedBDRReview;
            dtActualSAR.Value = projectData.Milestones.ActualSAR;
            dtPlanedSAR.Value = projectData.Milestones.PlannedSAR;

            skipDatePickerEventHandling = true;

            dtActualGoLive.Visible = txtActualGoLive.Visible = chkSameGoLive.Checked = projectData.Milestones.IsActualGoLiveSpecified;
            dtActualFPFV.Visible = txtActualFPFV.Visible = chkSameFPFV.Checked = projectData.Milestones.IsActualFPFVSpecified;
            //dtActualLPFV.Visible = txtActualLPFV.Visible = chkSameLPFV.Checked = projectData.Milestones.IsActualLPFVSpecified;
            dtActualLPLV.Visible = txtActualLPLV.Visible = chkSameLPLV.Checked = projectData.Milestones.IsActualLPLVSpecified;
            dtActualDBL.Visible = txtActualDBL.Visible = chkSameDBL.Checked = projectData.Milestones.IsActualDBLSpecified;
            dtActualSDTM.Visible = txtActualSDTM.Visible = chkSameSDTM.Checked = projectData.Milestones.IsActualCDISCSDTMSpecified;
            dtActualSAP.Visible = txtActualSAP.Visible = chkSameSAP.Checked = projectData.Milestones.IsActualSAPSpecified;
            dtActualBDR.Visible = txtActualBDR.Visible = chkSameBDR.Checked = projectData.Milestones.IsActualBDRSpecified;
            dtActualBDRReview.Visible = txtActualBDRReview.Visible = chkSameBDRReview.Checked = projectData.Milestones.IsActualBDRReviewSpecified;
            dtActualSAR.Visible = txtActualSAR.Visible = chkSameSAR.Checked = projectData.Milestones.IsActualSARSpecified;

            skipDatePickerEventHandling = false;

            //chkAddActuals.Checked = projectData.Milestones.AreActualDatesIncluded;

            //earlier it was storing wrong value here
            //txtActualGoLive.Text = projectData.Milestones.ActualSARDays.ToString();
            txtActualSAR.Text = projectData.Milestones.ActualSARDays.ToString();
            txtPlanedSAR.Text = projectData.Milestones.PlannedSARDays.ToString();
            txtActualBDR.Text = projectData.Milestones.ActualBDRDays.ToString();
            txtPlanedBDR.Text = projectData.Milestones.PlannedBDRDays.ToString();
            txtActualBDRReview.Text = projectData.Milestones.ActualBDRReviewDays.ToString();
            txtPlanedBDRReview.Text = projectData.Milestones.PlannedBDRReviewDays.ToString();
            txtActualSAP.Text = projectData.Milestones.ActualSAPDays.ToString();
            txtPlanedSAP.Text = projectData.Milestones.PlannedSAPDays.ToString();
            txtActualSDTM.Text = projectData.Milestones.ActualCDISCSDTMDays.ToString();
            txtPlanedSDTM.Text = projectData.Milestones.PlannedCDISCSDTMDays.ToString();
            txtActualDBL.Text = projectData.Milestones.ActualDBLDays.ToString();
            txtPlanedDBL.Text = projectData.Milestones.PlannedDBLDays.ToString();
            txtActualLPLV.Text = projectData.Milestones.ActualLPLVDays.ToString();
            txtPlanedLPLV.Text = projectData.Milestones.PlannedLPLVDays.ToString();
            //txtActualLPFV.Text = projectData.Milestones.ActualLPFVDays.ToString();
            //txtPlanedLPFV.Text = projectData.Milestones.PlannedLPFVDays.ToString();
            txtActualFPFV.Text = projectData.Milestones.ActualFPFVDays.ToString();
            txtPlanedFPFV.Text = projectData.Milestones.PlannedFPFVDays.ToString();
            txtActualGoLive.Text = projectData.Milestones.ActualGoLiveDays.ToString();
            txtPlanedGoLive.Text = projectData.Milestones.PlannedGoLiveDays.ToString();


            txtClientComment.Text = projectData.Comments.ClientComments;
            txtCytelComment.Text = projectData.Comments.CytelComments;
        }

		public ProjectDetails GetProjectDetails()
        {
            ProjectDetails projectDetails = new ProjectDetails();

            projectDetails.ProjectDescription = new ProjectDescription(txtProjectName.Text, cmbCategory.SelectedItem.ToString(), txtDescription.Text /*, txtStudyDuration.Text,
                Constants.StudyDurationUnit.Days*/, txtOtherCategory.Text);//, txtCSSName.Text);

            projectDetails.StudyInfo = new StudyInfo(dtAssignedOn.Value, txtCSLName.Text, txtGCLName.Text, txtCSSName.Text, cmbCRO.SelectedItem.ToString(), cmbCountry.SelectedItem.ToString(), 
                txtDataManagement.Text, txtCDISC.Text, txtStatisics.Text, txtOtherCRO.Text, txtOtherCountry.Text);
                        
            projectDetails.Milestones = new Milestones(cmbStudyStatus.Text,
                dtPlanedGoLive.Value, dtActualGoLive.Value, 
                dtPlanedFPFV.Value, dtActualFPFV.Value, 
                //dtPlanedLPFV.Value, dtActualLPFV.Value,
                dtPlanedLPLV.Value, dtActualLPLV.Value, 
                dtPlanedDBL.Value, dtActualDBL.Value, 
                dtPlanedSDTM.Value, dtActualSDTM.Value, 
                dtPlanedSAP.Value, dtActualSAP.Value,
                dtPlanedBDR.Value, dtActualBDR.Value, 
                dtPlanedBDRReview.Value, dtActualBDRReview.Value, 
                dtPlanedSAR.Value, dtActualSAR.Value,
                
                //Days
                Convert.ToInt16(txtPlanedGoLive.Text), Convert.ToInt16(txtActualGoLive.Text), Convert.ToInt16(txtPlanedFPFV.Text), Convert.ToInt16(txtActualFPFV.Text),
                /*Convert.ToInt16(txtPlanedLPFV.Text), Convert.ToInt16(txtActualLPFV.Text),*/ Convert.ToInt16(txtPlanedLPLV.Text), Convert.ToInt16(txtActualLPLV.Text),
                Convert.ToInt16(txtPlanedDBL.Text), Convert.ToInt16(txtActualDBL.Text), Convert.ToInt16(txtActualSDTM.Text), Convert.ToInt16(txtActualSDTM.Text),
                Convert.ToInt16(txtPlanedSAP.Text), Convert.ToInt16(txtActualSAP.Text), Convert.ToInt16(txtPlanedBDR.Text), Convert.ToInt16(txtActualBDR.Text), 
                Convert.ToInt16(txtPlanedBDRReview.Text), Convert.ToInt16(txtActualBDRReview.Text), 
                Convert.ToInt16(txtPlanedSAR.Text), Convert.ToInt16(txtActualSAR.Text),
               
                //Actual Days Go-Live

                chkSameGoLive.Checked, chkSameFPFV.Checked, /*chkSameLPFV.Checked,*/ chkSameLPLV.Checked, chkSameDBL.Checked, 
                chkSameSDTM.Checked, chkSameSAP.Checked, chkSameBDR.Checked , chkSameBDRReview.Checked , chkSameSAR.Checked ,

                areActualDatesIncluded: chkAddActuals.Checked);

            projectDetails.Comments = new Comments(txtClientComment.Text, txtCytelComment.Text);

            return projectDetails;
        }

        //private void txtDefaultDays_TextChanged(object sender, EventArgs e)
        public void SetDefaultDay()
        {
            string defaultdays = string.Empty;
            //defaultdays = txtDefaultDays.Text;

            //as per discussion with Snatanu on 24-May-21 default value for days 5 and remove default days
            defaultdays = Constants.DefaultNoOfDays.ToString();

            txtPlanedGoLive.Text = txtActualGoLive.Text = txtPlanedFPFV.Text = txtActualFPFV.Text = // txtPlanedLPFV.Text = txtActualLPFV.Text =
            txtPlanedLPLV.Text = txtActualLPLV.Text = txtPlanedDBL.Text = txtActualDBL.Text = txtPlanedSDTM.Text = txtActualSDTM.Text =
            txtPlanedSAP.Text = txtActualSAP.Text = txtPlanedBDRReview.Text = txtActualBDRReview.Text = txtPlanedBDR.Text = txtActualBDR.Text 
            = txtPlanedSAR.Text = txtActualSAR.Text = defaultdays;
        }

        #region Mouse hover mothods 

        private void SetToolTip(Control control, string tooltipText = "Tooltip not available for this control")
        {
            if (control == null)
                return;

            toolTip1.SetToolTip(control, tooltipText);
        }
        
        private string GetHelpText_AddProject(string controlName)
        {
            if (string.IsNullOrWhiteSpace(controlName)) return string.Empty;

            string helpText = string.Empty;
            
            switch (controlName)
            {
                #region UC Main - Add project

                case Constants.ControlName_txtProjectName:
                    helpText = Constants.HelpText_txtProjectName;
                    break;

                case Constants.ControlName_txtCategory:
                    helpText = Constants.HelpText_txtCategory;
                    break;

                case Constants.ControlName_txtDescription:
                    helpText = Constants.HelpText_txtDescription;
                    break;

                case Constants.ControlName_txtStudyDuration:
                    helpText = Constants.HelpText_txtStudyDuration;
                    break;
                #endregion

                #region study/Personnel Info

                case Constants.ControlName_dtAssignedOn:
                    helpText = Constants.HelpText_dtAssignedOn;
                    break;

                case Constants.ControlName_txtCSLName:
                    helpText = Constants.HelpText_txtCSLName;
                    break;

                case Constants.ControlName_txtGCLName:
                    helpText = Constants.HelpText_txtGCLName;
                    break;

                case Constants.ControlName_txtCSSName:
                    helpText = Constants.HelpText_txtCSSName;
                    break;

                case Constants.ControlName_txtCROName:
                    helpText = Constants.HelpText_txtCROName;
                    break;

                case Constants.ControlName_txtCountryName:
                    helpText = Constants.HelpText_txtCountryName;
                    break;

                case Constants.ControlName_chkDataManagement:
                    helpText = Constants.HelpText_chkDataManagement;
                    break;

                case Constants.ControlName_txtDataManagement:
                    helpText = Constants.HelpText_txtDataManagement;
                    break;

                case Constants.ControlName_chkCDISC:
                    helpText = Constants.HelpText_chkCDISC;
                    break;

                case Constants.ControlName_txtCDISC:
                    helpText = Constants.HelpText_txtCDISC;
                    break;

                case Constants.ControlName_chkStatisics:
                    helpText = Constants.HelpText_chkStatisics;
                    break;

                case Constants.ControlName_txtStatisics:
                    helpText = Constants.HelpText_txtStatisics;
                    break;
                #endregion

                #region Milestones

                //case Constants.ControlName_chkDefaultDays:
                //    helpText = Constants.HelpText_chkDefaultDays;
                //    break;

                //case Constants.ControlName_txtDefaultDays:
                //    helpText = Constants.HelpText_DefaultDays;
                //    break;

                case Constants.ControlName_cmbStudyStatus:
                    helpText = Constants.HelpText_cmbStudyStatus;
                    break;

                case Constants.ControlName_dtPlanedGoLive:
                    helpText = Constants.HelpText_dtPlanedGoLive;
                    break;

                case Constants.ControlName_txtPlanedGoLive:
                    helpText = Constants.HelpText_txtPlanedGoLive;
                    break;


                case Constants.ControlName_dtActualGoLive:
                    helpText = Constants.HelpText_dtActualGoLive;
                    break;

                case Constants.ControlName_txtActualGoLive:
                    helpText = Constants.HelpText_txtActualGoLive;
                    break;

                case Constants.ControlName_dtPlannedFPFV:
                    helpText = Constants.HelpText_dtPlannedFPFV;
                    break;

                case Constants.ControlName_txtPlanedFPFV:
                    helpText = Constants.HelpText_txtPlanedFPFV;
                    break;

                case Constants.ControlName_dtActualFPFV:
                    helpText = Constants.HelpText_dtActualFPFV;
                    break;

                case Constants.ControlName_textActualFPFV:
                    helpText = Constants.HelpText_textActualFPFV;
                    break;


                case Constants.ControlName_dtPlannedLPFV:
                    helpText = Constants.HelpText_dtPlannedLPFV;
                    break;

                case Constants.ControlName_txtPlanedLPFV:
                    helpText = Constants.HelpText_txtPlanedLPFV;
                    break;

                case Constants.ControlName_dtActualLPFV:
                    helpText = Constants.HelpText_dtActualLPFV;
                    break;

                case Constants.ControlName_textActualLPFV:
                    helpText = Constants.HelpText_textActualLPFV;
                    break;


                case Constants.ControlName_dtPlannedLPLV:
                    helpText = Constants.HelpText_dtPlannedLPLV;
                    break;

                case Constants.ControlName_txtPlanedLPLV:
                    helpText = Constants.HelpText_txtPlanedLPLV;
                    break;

                case Constants.ControlName_dtActualLPLV:
                    helpText = Constants.HelpText_dtActualLPLV;
                    break;

                case Constants.ControlName_textActualLPLV:
                    helpText = Constants.HelpText_textActualLPLV;
                    break;


                case Constants.ControlName_dtPlannedDBL:
                    helpText = Constants.HelpText_dtPlannedDBL;
                    break;

                case Constants.ControlName_txtPlanedDBL:
                    helpText = Constants.HelpText_txtPlanedDBL;
                    break;

                case Constants.ControlName_dtActualDBL:
                    helpText = Constants.HelpText_dtActualDBL;
                    break;

                case Constants.ControlName_textActualDBL:
                    helpText = Constants.HelpText_textActualDBL;
                    break;


                case Constants.ControlName_dtPlanedSDTM:
                    helpText = Constants.HelpText_dtPlanedSDTM;
                    break;

                case Constants.ControlName_txtPlanedSDTM:
                    helpText = Constants.HelpText_txtPlanedSDTM;
                    break;

                case Constants.ControlName_dtActualSDTM:
                    helpText = Constants.HelpText_dtActualSDTM;
                    break;

                case Constants.ControlName_textActualSDTM:
                    helpText = Constants.HelpText_textActualSDTM;
                    break;


                case Constants.ControlName_dtPlanedSAP:
                    helpText = Constants.HelpText_dtPlanedSAP;
                    break;

                case Constants.ControlName_txtPlanedSAP:
                    helpText = Constants.HelpText_txtPlanedSAP;
                    break;

                case Constants.ControlName_dtActualSAP:
                    helpText = Constants.HelpText_dtActualSAP;
                    break;

                case Constants.ControlName_textActualSAP:
                    helpText = Constants.HelpText_textActualSAP;
                    break;


                case Constants.ControlName_dtPlanedBDR:
                    helpText = Constants.HelpText_dtPlanedBDR;
                    break;

                case Constants.ControlName_txtPlanedBDR:
                    helpText = Constants.HelpText_txtPlanedBDR;
                    break;

                case Constants.ControlName_dtActualBDR:
                    helpText = Constants.HelpText_dtActualBDR;
                    break;

                case Constants.ControlName_textActualBDR:
                    helpText = Constants.HelpText_textActualBDR;
                    break;


                case Constants.ControlName_dtPlanedBDRReview:
                    helpText = Constants.HelpText_dtPlanedBDRReview;
                    break;

                case Constants.ControlName_txtPlanedBDRReview:
                    helpText = Constants.HelpText_txtPlanedBDRReview;
                    break;

                case Constants.ControlName_dtActualBDRReview:
                    helpText = Constants.HelpText_dtActualBDRReview;
                    break;

                case Constants.ControlName_textActualBDRReview:
                    helpText = Constants.HelpText_textActualBDRReview;
                    break;

                case Constants.ControlName_dtPlanedSAR:
                    helpText = Constants.HelpText_dtPlanedSAR;
                    break;

                case Constants.ControlName_txtPlanedSAR:
                    helpText = Constants.HelpText_txtPlanedSAR;
                    break;

                case Constants.ControlName_dtActualSAR:
                    helpText = Constants.HelpText_dtActualSAR;
                    break;

                case Constants.ControlName_textActualSAR:
                    helpText = Constants.HelpText_textActualSAR;
                    break;

                #endregion
                
                #region Comments

                case Constants.ControlName_txtClientComment:
                    helpText = Constants.HelpText_txtClientComment;
                    break;

                case Constants.ControlName_txtCytelComment:
                    helpText = Constants.HelpText_txtCytelComment;
                    break;

                #endregion

                default:
                    helpText = "";
                    break;
            }

            return helpText;
        }

        /// <summary>
        /// Assign help text to all control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MouseHover_AddProject(object sender, EventArgs e)
        {
            SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        }

        #endregion
        
        #region Keypress event method for all textboxes- single event
        /// <summary>
        /// method to validate keypress event of all textboxes, for digit only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ValidateKeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                //text box that the key press event was handled, do not process it
                e.Handled = true;
            }
        }

        #endregion

        #region Checkbox change method- single event

        private void CheckBox_ValueChanged(object sender, EventArgs e)
        {
            if (skipDatePickerEventHandling) return;

            string relateddtPickerPlanedControlName = ((CheckBox)sender).Name.Replace("chkSame", "dtPlaned");
            string relateddtPickeractualControlName = ((CheckBox)sender).Name.Replace("chkSame", "dtActual");
            string relatedtxtBxactualControlName = ((CheckBox)sender).Name.Replace("chkSame", "txtActual");

            DateTimePicker plannedDate = (DateTimePicker)GetControlByName(panel2, relateddtPickerPlanedControlName);
            DateTimePicker actualDate = (DateTimePicker)GetControlByName(panel2, relateddtPickeractualControlName);
            TextBox txtPlannedDays = (TextBox)GetControlByName(panel2, relatedtxtBxactualControlName);
           
            MakeActualDateSameAsPlanned(plannedDate, actualDate, txtPlannedDays, ((CheckBox)sender).Checked);
        }
        #endregion


        private void MakeActualDateSameAsPlanned(DateTimePicker dtPlanned, DateTimePicker dtActual, TextBox txtActual, bool showActualDate=false )
        {
            skipDatePickerEventHandling = true;
            
            //Visibility of Controls - Checkbox
            //pnlActualDates.Visible = true;
            dtActual.Visible = showActualDate;
            txtActual.Visible = showActualDate;
            
            dtActual.Value = dtPlanned.Value;

            skipDatePickerEventHandling = false;
        }

        private void dtActual_ValueChanged(object sender, EventArgs e)
        {
            if (skipDatePickerEventHandling) return;

            string relatedCheckboxControlName = ((DateTimePicker)sender).Name.Replace("dtActual", "chkSame");
            
            CheckBox relativeCheckBox = (CheckBox)GetControlByName(panel2, relatedCheckboxControlName);
            
            if (relativeCheckBox != null)
            {
                string relatedPlannedDateControlName = ((DateTimePicker)sender).Name.Replace("dtActual", "dtPlaned");

                DateTimePicker plannedDate = (DateTimePicker)GetControlByName(panel2, relatedPlannedDateControlName);

                if (((DateTimePicker)sender).Value != plannedDate.Value)
                    relativeCheckBox.Checked = false;                
            }
        }

        private void dtPlaned_ValueChanged(object sender, EventArgs e)
        {
            if (skipDatePickerEventHandling) return;

            string relatedCheckboxControlName = ((DateTimePicker)sender).Name.Replace("dtPlaned", "chkSame");

            CheckBox relativeCheckBox = (CheckBox)GetControlByName(panel2, relatedCheckboxControlName);

            if (relativeCheckBox != null)
            {
                string relatedActualDateControlName = ((DateTimePicker)sender).Name.Replace("dtPlaned", "dtActual");

                DateTimePicker ActualDate = (DateTimePicker)GetControlByName(panel2, relatedActualDateControlName);

                if (((DateTimePicker)sender).Value != ActualDate.Value)
                    relativeCheckBox.Checked = false;
            }

            //this will perform calculations of below date pickers
            if (Constants.flgDtPicker == true)
            {
                PerformCalculations(((DateTimePicker)sender).Name);
            }
        }

        
        private Control GetControlByName(Control container, string controlName)
        {
            foreach (Control control in container.Controls)
            {
                if (control is Panel || control is GroupBox || control is TableLayoutPanel || control is TabControl || control is TabPage)
                    return GetControlByName(control, controlName);

                if (control.Name.Equals(controlName))
                    return control;
            }

            return null;
        }

        /// <summary>
        /// //this will perform calculations of below date pickers which are present under golive , fpfv, lpfv 
        /// as first three controls are manual
        /// </summary>
        public void PerformCalculations(string ChangControlName)
        {
            if (ChangControlName == "dtPlanedGoLive")
            {
                dtPlanedSAP.Value = DateTime.ParseExact(dtPlanedGoLive.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);           
            }           
            //else if (ChangControlName == "dtPlanedLPFV")
            //{                
            //    //LPLV=lpfv+study durtan
            //    //endDate = endDate.AddDays(addedDays);
            //    DateTime dt = DateTime.ParseExact(dtPlanedLPFV.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);  
            //    dtPlanedLPLV.Value = dt.AddDays(Constants.studyDurationValue);              
            //}
            else if (ChangControlName == "dtPlanedLPLV")
            {
                //DBL=LPLV+5 work days                
                DateTime dt = DateTime.ParseExact(dtPlanedLPLV.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);  
                dtPlanedDBL.Value = DateTimeExtensions.AddWorkdays(dt, 5);
            }
            else if (ChangControlName == "dtPlanedDBL")
            {
                //CDISC=DBL + 6 work days               
                DateTime dt = DateTime.ParseExact(dtPlanedDBL.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);  
                dtPlanedSDTM.Value = DateTimeExtensions.AddWorkdays(dt, 6);
            }            
            else if (ChangControlName == "dtPlanedSDTM")
            {
                //BDR=CDISC+3 work days              
                DateTime dt = DateTime.ParseExact(dtPlanedSDTM.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);  
                dtPlanedBDR.Value = DateTimeExtensions.AddWorkdays(dt, 3);
            }
            else if (ChangControlName == "dtPlanedBDR")
            {
                //'BDR Review'= BDR + 3 work days            
                DateTime dt = DateTime.ParseExact(dtPlanedBDR.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);  
                dtPlanedBDRReview.Value = DateTimeExtensions.AddWorkdays(dt, 3);
            }
            else if (ChangControlName == "dtPlanedBDRReview")
            {
                //sar= 'BDR Review + 6 work days           
                DateTime dt = DateTime.ParseExact(dtPlanedBDRReview.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);  
                dtPlanedSAR.Value = DateTimeExtensions.AddWorkdays(dt, 6);
            }
        }
       
        private void txtStudyDuration_TextChanged(object sender, EventArgs e)
        {
            //if (!string.IsNullOrWhiteSpace(txtStudyDuration.Text))
            //{
            //    Constants.studyDurationValue = Convert.ToInt16(txtStudyDuration.Text);
            //    SetDefaultDates();
            //}
        }

        //private void chkDefaultDays_CheckedChanged(object sender, EventArgs e)
        //{
        //    txtDefaultDays.Enabled = chkDefaultDays.Checked ? true : false;

        //}

        #region Unused Code
        #region Commented: Earlier mouse hover events

        //private void txtProjectName_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}        

        //private void txtCategory_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void txtDescription_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void txtStudyDuration_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtAssignedOn_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void txtCSLName_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void txtGCLName_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void txtCSSName_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void chkDataManagement_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void txtDataManagement_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void chkCDISC_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void txtCDISC_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void chkStatisics_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void txtStatisics_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtPlanedGoLive_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtPlanedFPFV_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtPlanedLPFV_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtPlanedLPLV_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtPlanedDBL_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtPlanedSDTM_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtPlanedSAP_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtPlanedBDR_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtPlanedSAR_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtActualGoLive_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtActualFPFV_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtActualLPFV_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtActualLPLV_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtActualDBL_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtActualSDTM_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtActualSAP_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtActualBDR_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void dtActualSAR_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void txtClientComment_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void txtCytelComment_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}

        //private void txtDefaultDays_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_AddProject(((Control)sender).Name));
        //}
        #endregion

        #region Commented: Earlier Keypress event of all textboxes

        /// <summary>
        /// validation for study duration field: enter number only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void txtStudyDuration_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtStudyDuration.Name, e);
        //}

        //private void txtDefaultDays_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtDefaultDays.Name, e);
        //}

        //private void txtPlanedGoLive_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtPlanedGoLive.Name, e);
        //}

        //private void txtActualGoLive_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtActualGoLive.Name, e);
        //}

        //private void txtPlanedFPFV_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtPlanedFPFV.Name, e);
        //}

        //private void txtActualFPFV_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtActualFPFV.Name, e);
        //}

        //private void txtActualLPFV_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtActualLPFV.Name, e);
        //}

        //private void txtPlanedLPFV_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtPlanedLPFV.Name, e);
        //}

        //private void txtPlanedLPLV_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtPlanedLPLV.Name, e);
        //}

        //private void txtActualLPLV_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtActualLPLV.Name, e);
        //}

        //private void txtPlanedDBL_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtPlanedDBL.Name, e);
        //}

        //private void txtActualDBL_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtActualDBL.Name, e);
        //}

        //private void txtPlanedSDTM_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtPlanedSDTM.Name, e);
        //}

        //private void txtActualSDTM_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtActualSDTM.Name, e);
        //}

        //private void txtPlanedSAP_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtPlanedSAP.Name, e);
        //}

        //private void txtActualSAP_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtActualSAP.Name, e);
        //}

        //private void txtPlanedBDR_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtPlanedBDR.Name, e);
        //}

        //private void txtActualBDR_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtActualBDR.Name, e);
        //}

        //private void txtPlanedSAR_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtPlanedSAR.Name, e);
        //}

        //private void txtActualSAR_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ValidateKeyPress(txtActualSAR.Name, e);            
        //}

        #endregion

        #region Commented: Earlier Checkbox change events

        //private void chkSameGoLive_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkSameGoLive.Checked)
        //        MakeActualDateSameAsPlanned(dtPlanedGoLive, dtActualGoLive);
        //}       

        //private void chkSameFPFV_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkSameFPFV.Checked)
        //        MakeActualDateSameAsPlanned(dtPlanedFPFV, dtActualFPFV);
        //}

        //private void chkSameLPFV_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkSameLPFV.Checked)
        //        MakeActualDateSameAsPlanned(dtPlanedLPFV, dtActualLPFV);
        //}

        //private void chkSameLPLV_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkSameLPLV.Checked)
        //        MakeActualDateSameAsPlanned(dtPlanedLPLV, dtActualLPLV);
        //}

        //private void chkSameDBL_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkSameDBL.Checked)
        //        MakeActualDateSameAsPlanned(dtPlanedDBL, dtActualDBL);
        //}

        //private void chkSameCDISC_CheckedChanged(object sender, EventArgs e)
        //{ 
        //    if (chkSameCDISC.Checked)
        //        MakeActualDateSameAsPlanned(dtPlanedSDTM, dtActualSDTM);
        //}

        //private void chkSameSAP_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkSameSAP.Checked)
        //        MakeActualDateSameAsPlanned(dtPlanedSAP, dtActualSAP);
        //}

        //private void chkSameBDR_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkSameBDR.Checked)
        //        MakeActualDateSameAsPlanned(dtPlanedBDR, dtActualBDR);
        //}

        //private void chkSameSAR_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkSameSAR.Checked)
        //        MakeActualDateSameAsPlanned(dtPlanedSAR, dtActualSAR);
        //}
        #endregion

        #endregion

        private void txtPlanedGoLive_TextChanged(object sender, EventArgs e)
        {

        }

        private void chkAddActuals_CheckedChanged(object sender, EventArgs e)
        {
            //pnlActualDates.Visible = chkAddActuals.Checked;
        }

        private void cmbCRO_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool showOtherCRO = cmbCRO.SelectedItem.ToString().Equals("Other");
            lblOtherCRO.Visible = txtOtherCRO.Visible = showOtherCRO;
        }

        private void cmbCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool showOtherCountry = cmbCountry.SelectedItem.ToString().Equals("Other");
            lblOtherCountry.Visible = txtOtherCountry.Visible = showOtherCountry;
        }

        private void cmbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool showOtherCategory = cmbCategory.SelectedItem.ToString().Equals("Other");
            lblOtherCategory.Visible = txtOtherCategory.Visible = showOtherCategory;
        }
    }
}
