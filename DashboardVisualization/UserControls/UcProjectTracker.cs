﻿using DashboardVisualization.Containers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Zuby.ADGV;

namespace DashboardVisualization.UserControls
{
    using DashboardVisualization.Models;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using TLFs_Prism.Containers;
    using GridColumns = Models.Constants.GridColumns;

    public partial class UcProjectTracker : UserControl
    {
        #region ADGV declarations
        private DataTable _dataTable = null;
        private DataSet _dataSet = null;

        private SortedDictionary<int, string> _filtersaved = new SortedDictionary<int, string>();
        private SortedDictionary<int, string> _sortsaved = new SortedDictionary<int, string>();

        private bool _testtranslations = false;
        private bool _testtranslationsFromFile = false;

        private static bool MemoryTestEnabled = true;
        private const int MemoryTestFormsNum = 100;
        private bool _memorytest = false;
               
        private object[][] _inrows = new object[][] { };
        private Timer _memorytestclosetimer = new Timer
        {
            Interval = 100
        };

        private Timer _timermemoryusage = new Timer
        {
            Interval = 2000
        };

        private static bool CollectGarbageOnTimerMemoryUsageUpdate = true;
        #endregion

        public int selectedPrjID_shtRow;
                
        public UcProjectTracker()
        {
            InitializeComponent();

            InitializeDataGrid();

            //ucMain.OnButtonClick += new UcExecTracker.OnButtonClick(ucMain_OnButtonClick);
        }

        private void InitializeDataGrid()
        {
            //trigger the memory usage show
            //_timermemoryusage_Tick(null, null);

            //DoneCount = SuccessCount = 0;

            //set localization strings
            Dictionary<string, string> translations = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> translation in AdvancedDataGridView.Translations)
            {
                if (!translations.ContainsKey(translation.Key))
                    translations.Add(translation.Key, "." + translation.Value);
            }

            foreach (KeyValuePair<string, string> translation in AdvancedDataGridViewSearchToolBar.Translations)
            {
                if (!translations.ContainsKey(translation.Key))
                    translations.Add(translation.Key, "." + translation.Value);
            }

            if (_testtranslations)
            {
                AdvancedDataGridView.SetTranslations(translations);
                AdvancedDataGridViewSearchToolBar.SetTranslations(translations);
            }

            if (_testtranslationsFromFile)
            {
                AdvancedDataGridView.SetTranslations(AdvancedDataGridView.LoadTranslationsFromFile("lang.json"));
                AdvancedDataGridViewSearchToolBar.SetTranslations(AdvancedDataGridViewSearchToolBar.LoadTranslationsFromFile("lang.json"));
            }

            //set filter and sort saved
            if (_filtersaved.ContainsKey(0) == false) _filtersaved.Add(0, "");
            if (_sortsaved.ContainsKey(0) == false) _sortsaved.Add(0, "");
           
            //set memory test button
            //button_memorytest.Enabled = MemoryTestEnabled;

            //initialize dataset
            _dataTable = new DataTable();
            _dataSet = new DataSet();

            //initialize bindingsource
            bindingSource_main.DataSource = _dataSet;

            //initialize datagridview
            advancedDataGridView1.SetDoubleBuffered();
            advancedDataGridView1.DataSource = bindingSource_main;

            //set bindingsource
            // SetTestData();

            _dataTable = _dataSet.Tables.Add("TableTest");
            _dataTable.Columns.Add(GridColumns.Select, typeof(bool));
            _dataTable.Columns.Add(GridColumns.ProjectName, typeof(string));
            _dataTable.Columns.Add(GridColumns.DMAwarded, typeof(string));
            _dataTable.Columns.Add(GridColumns.DMPOC, typeof(string));
            _dataTable.Columns.Add(GridColumns.CDISC_Awarded, typeof(string));
            _dataTable.Columns.Add(GridColumns.CDISC_POC, typeof(string));
            _dataTable.Columns.Add(GridColumns.Stat_Awarded, typeof(string));
            _dataTable.Columns.Add(GridColumns.Stat_POC, typeof(string));
            _dataTable.Columns.Add(GridColumns.ProgAssignedOn, typeof(string));
            _dataTable.Columns.Add(GridColumns.GoLivePlanned, typeof(string));
            _dataTable.Columns.Add(GridColumns.GoLiveActual, typeof(string));
            _dataTable.Columns.Add(GridColumns.FPFVPlanned, typeof(string));
            _dataTable.Columns.Add(GridColumns.FPFVActual, typeof(string));
            //_dataTable.Columns.Add(GridColumns.LPFVPlanned, typeof(string));
            //_dataTable.Columns.Add(GridColumns.LPFVActual, typeof(string));
            //_dataTable.Columns.Add(GridColumns.StudyDuration, typeof(string));
            _dataTable.Columns.Add(GridColumns.LPLVPlanned, typeof(string));
            _dataTable.Columns.Add(GridColumns.LPLVActual, typeof(string));
            _dataTable.Columns.Add(GridColumns.DBLPlanned, typeof(string));
            _dataTable.Columns.Add(GridColumns.DBLActual, typeof(string));
            _dataTable.Columns.Add(GridColumns.CDISCPlanned, typeof(string));
            _dataTable.Columns.Add(GridColumns.CDISCActual, typeof(string));
            _dataTable.Columns.Add(GridColumns.SAPPlanned, typeof(string));
            _dataTable.Columns.Add(GridColumns.SAPActual, typeof(string));
            _dataTable.Columns.Add(GridColumns.BDRPlanned, typeof(string));
            _dataTable.Columns.Add(GridColumns.BDRActual, typeof(string));
            _dataTable.Columns.Add(GridColumns.SARPlanned, typeof(string));
            _dataTable.Columns.Add(GridColumns.SARActual, typeof(string));
            _dataTable.Columns.Add(GridColumns.Client_Comments, typeof(string));
            _dataTable.Columns.Add(GridColumns.Cytel_Comments, typeof(string));                       
        }

        //private void InitializeDataGrid()
            //{
            //    //initialize dataset
            //    _dataTable = new DataTable();
            //    _dataSet = new DataSet();


            //    #region AddColumns
            //    _dataTable = _dataSet.Tables.Add("ProjectData");
            //    _dataTable.Columns.Add((Constants.UCGridColumns.Select.GetEnumDescription()), typeof(bool));
            //    _dataTable.Columns.Add(Constants.UCGridColumns.ProjectName.GetEnumDescription(), typeof(string));
            //    _dataTable.Columns.Add(Constants.UCGridColumns.CSLName.GetEnumDescription(), typeof(string));
            //    _dataTable.Columns.Add(Constants.UCGridColumns.GCLName.GetEnumDescription(), typeof(string));
            //    _dataTable.Columns.Add(Constants.UCGridColumns.CSSName.GetEnumDescription(), typeof(string));

            //    _dataTable.Columns.Add(ADGVGridColumns.DMAwarded, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.DMPOC, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.CDISC_Awarded, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.CDISC_POC, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.Stat_Awarded, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.Stat_POC, typeof(string));

            //    _dataTable.Columns.Add(ADGVGridColumns.ProgAssignedOn, typeof(string));

            //    _dataTable.Columns.Add(ADGVGridColumns.GoLivePlanned, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.GoLiveActual, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.FPFVPlanned, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.FPFVActual, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.LPFVPlanned, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.LPFVActual, typeof(string));

            //    _dataTable.Columns.Add(ADGVGridColumns.StudyDuration, typeof(string));

            //    _dataTable.Columns.Add(ADGVGridColumns.LPLVPlanned, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.LPLVActual, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.DBLPlanned, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.DBLActual, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.CDISCPlanned, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.CDISCActual, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.SAPPlanned, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.SAPActual, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.BDRPlanned, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.BDRActual, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.SARPlanned, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.SARActual, typeof(string));
            //    //Added by Chetna  Date:23-Mar-2021
            //    _dataTable.Columns.Add(ADGVGridColumns.Client_Comments, typeof(string));
            //    _dataTable.Columns.Add(ADGVGridColumns.Cytel_Comments, typeof(string));
            //    #endregion

            //    gvProjectData.Rows.Clear();
            //    gvProjectData.Refresh();

            //    gvProjectData.DataSource = _dataTable;
            //    gvProjectData.Refresh();
            //}

        private void btnAddProject_Click(object sender, EventArgs e)
        {
            FrmMain main = new FrmMain();

            //main.SetDefaulltDate(FrmMain);

            DialogResult result = main.ShowDialog();

            if (result == DialogResult.OK)
            {
                // add form data into grid control
                // FillGridRows();
                // write form data into excel workspace file
                // WriteGridDataIntoExcel();

                //added condition : if form data is not valid then after msgbox invalid form name gives error
                //"object reference not set to an instance"
                if (Constants.ValidFormData)
                {
                    bool projectAddedInGrid = AddProjectRecordInGrid();

                    // After record is successfully reflected in grid, add the same record in workspace file also.
                    if (projectAddedInGrid)
                        AddProjectRecordInWorkspace();
                }
            }
            main.Close();
        }

        /// <summary>
        /// open main form to edit the ptoject details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEditProject_Click(object sender, EventArgs e)
        {
            //check if grid row is selected -loop required
            int selectedRowsCount = 0, rowIndex = 0, selectedRowIndex = 0;
            string selectedProjectName = string.Empty;

            foreach (DataGridViewRow gridRow in advancedDataGridView1.Rows)
            {
                if (Convert.ToBoolean(gridRow.Cells[GridColumns.Select].Value))
                {
                    selectedRowsCount++;
                    selectedRowIndex = rowIndex;
                    selectedProjectName = gridRow.Cells[GridColumns.ProjectName].Value.ToString();
                }
                rowIndex++;
            }

            if (selectedRowsCount > 1)
            {
                MessageBox.Show("More that one project record is selected in table. Please select only one project record in order to edit its details.", 
                    Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (selectedRowsCount < 1)
            {
                MessageBox.Show("There isn't any project selected in table. Please select only one project record in order to edit its details.",
                    Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (string.IsNullOrWhiteSpace(selectedProjectName) == false)  // (gvProjectData.SelectedRows.Count >0 )
            {                
                {
                    //one method for - extract current data from Excel workspace
                    ProjectDetails projectDetails =  ReadProjectFromWorkspace(selectedProjectName);

                    if (selectedProjectName == null)
                    {
                        MessageBox.Show("The selected project is not found in worksplace, and therefore it cannot be edited.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    // ToDo
                    // do not let the project name editable
                    FrmMain main = new FrmMain(true, projectDetails);
                    DialogResult result = main.ShowDialog();

                    // ToDo
                    // use object comparison to decide if the object before edit and after edit are different.
                    // if they are different then update the project record in workspace
                
                    //second method for - edit form data view
                    if (result == DialogResult.OK)
                    {
                        if (Constants.ValidFormData)
                        {
                            //Update the selected project id related data in both workspace and grid
                            bool projectUpdatedInGrid = UpdateProjectInGrid(selectedRowIndex);

                            if (projectUpdatedInGrid)
                                UpdateProjectInWorkspace(selectedProjectName);
                        }
                    }

                    main.Close();
                }
            }            
        }
       

        /// <summary>
        /// write form data into excel workspace file
        /// </summary>
        /// <param name="DashBoardPortFolio"></param>
        /*
        private void WriteGridDataIntoExcel(bool DashBoardPortFolio = false, bool editMode =false  )
        {
            int rowCount = 0;

            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook ExcelWorkBook = app.Workbooks.Open(Constants.WorkSpaceFilePath);

            try
            {               
                app.DisplayAlerts = false;
                object misValue = System.Reflection.Missing.Value;
                app.Visible = false;                
                app.ActiveWindow.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMinimized;

                // Excelsheet in workbook  
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                worksheet = ExcelWorkBook.Worksheets[Constants.ShtProjData];

                //get last row of worksheet
                Microsoft.Office.Interop.Excel.Range usedRange1 = worksheet.UsedRange;
                //Code changes done by chetna
                if (editMode == false)
                {
                    rowCount = usedRange1.Rows.Count;
                }
                else if (editMode == true ) 
                {
                    rowCount = selectedPrjID_shtRow;
                }

                string[] lines = Constants.MainFormData.Split(new[] { Constants.Seperator }, StringSplitOptions.None);
                int cellCnt = Constants.ProjectIdCol;
                
                //Sr. No. inster serial number by formula
                worksheet.Cells[rowCount + 1, cellCnt - 1].Formula ="=ROW()-1";
                usedRange1.Calculate();
                foreach (string value in lines)
                {
                    worksheet.Cells[rowCount + 1,  cellCnt] = value;
                    cellCnt++;
                }
                #region commented earlier code: grid data into excel
                //// storing header part in Excel  
                //if (DashBoardPortFolio)
                //{
                //    for (int i = 1; i < gvProjectData.Columns.Count + 1; i++)
                //    {
                //        worksheet.Cells[1, i] = gvProjectData.Columns[i - 1].HeaderText;
                //    }
                //}

                //int selectColIndex = GetColumnIndexByName(gvProjectData, Constants.UCGridColumns.Select.GetEnumDescription());

                // storing Each row and column value to excel sheet  
                //for (int i = 0; i < gvProjectData.Rows.Count; i++)
                //{
                //    //bool isSelected = Convert.ToBoolean(gvProjectData.Rows[i].Cells[selectColIndex].Value);
                //    for (int j = 0; j < gvProjectData.Columns.Count; j++)
                //    {
                //        {
                //            worksheet.Cells[rowCount+1, j + 1] = gvProjectData.Rows[i].Cells[j].Value.ToString();
                //        }
                //    }
                //}
                #endregion

                ExcelWorkBook.Save();
                ExcelWorkBook.Close(true, System.Reflection.Missing.Value, System.Reflection.Missing.Value);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                //return false;
            }
            finally
            {                
                app.Quit();
                if (ExcelWorkBook != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelWorkBook); }
                if (app != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(app); }
            }
           // return true;
        }
        */
                
        /// <summary>
        /// This methods loads an existing workspace, as specified in entry form.
        /// </summary>
        internal void LoadExistingWorkspace()
        {
            // read all projects details from workspace in a collection
            List<ProjectDetails> allProjectDetails = ReadAllProjectsFromWorkspace(false);

            if (allProjectDetails == null || allProjectDetails.Count == 0)
                return;

            // Populate grid with the projects details from collection
            AddAllProjectRecordsInGrid(allProjectDetails);
        }

        /// <summary>
        /// This method reads information for all projects in current workspace.
        /// </summary>
        /// <returns>Collection of all projects' details from current workspace.</returns>
        private List<ProjectDetails> ReadAllProjectsFromWorkspace(bool importData)
        {
            List<ProjectDetails> allProjectDetails = new List<ProjectDetails>();

            int rowCount, colCount;

            Microsoft.Office.Interop.Excel.Application Excelapp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook ExcelWorkBook = Excelapp.Workbooks.Open(importData ? Constants.ImportFilePath : Constants.WorkSpaceFilePath);

            try
            {
                Excelapp.DisplayAlerts = false;
                object misValue = System.Reflection.Missing.Value;
                Excelapp.Visible = false;
                Excelapp.ActiveWindow.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMinimized;

                // Excelsheet in workbook  
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                worksheet = ExcelWorkBook.Worksheets[importData ? "Sheet1" : Constants.ShtProjData];

                //get last row of worksheet
                Microsoft.Office.Interop.Excel.Range usedRange1 = worksheet.UsedRange;
                //Changed by Chetna as last row count was calculating wrong
                //rowCount = usedRange1.Rows.Count; 
                rowCount = worksheet.Cells.Find("*", System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByRows, Microsoft.Office.Interop.Excel.XlSearchDirection.xlPrevious, false, System.Reflection.Missing.Value, System.Reflection.Missing.Value).Row;
                //rowCount = worksheet.Cells.SpecialCells(Microsoft.Office.Interop.Excel.XlCellType.xlCellTypeLastCell, Type.Missing).Row;
                colCount = usedRange1.Columns.Count;
                int rowIndex = importData ? 3 : 2;//4 : 2; 
                //int colIndex = 2;

                Constants.MainFormData = string.Empty;
                string ProjIDDetailsData = string.Empty;
                for (; rowIndex <= rowCount; rowIndex++)
                {
                    ProjectDetails projectDetails = new ProjectDetails();
                    projectDetails.ProjectDescription = new ProjectDescription();
                    projectDetails.ProjectDescription.ProjectName = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProjectName].Value);
                    projectDetails.ProjectDescription.ProjectCategory = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProjectCategory].Value);
                    projectDetails.ProjectDescription.Description = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProjectDescription].Value);
                    // ToDo - add study duration and study duration unit in workspace; and read it belos
                    //projectDetails.ProjectDescription.StudyDuration = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.StudyDuration].Value);
                    //projectDetails.ProjectDescription.StudyDurationUnit = (int)Constants.StudyDurationUnit.Days;

                    projectDetails.StudyInfo = new StudyInfo();
                    projectDetails.StudyInfo.CSLName = worksheet.Cells[rowIndex, Constants.WorkspaceFields.CSLName].Value == null ? "Not Specified" : 
                        Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CSLName].Value);
                    projectDetails.StudyInfo.GCLName = worksheet.Cells[rowIndex, Constants.WorkspaceFields.GCLName].Value == null ? "Not Specified" :
                        Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.GCLName].Value);
                    projectDetails.StudyInfo.CSSName = worksheet.Cells[rowIndex, Constants.WorkspaceFields.CSSName].Value == null ? "Not Specified" :
                        Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CSSName].Value);
                    projectDetails.StudyInfo.Cro = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CROName].Value);
                    projectDetails.StudyInfo.Country = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CountryName].Value);

                    bool dateReadStatus = false;

                    //projectDetails.StudyInfo.DataManagementAwarded = Convert.ToBoolean(worksheet.Cells[rowIndex, Constants.WorkspaceFields.DMAwarded].Value);
                    projectDetails.StudyInfo.DataManagementAwarded = (Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.DMAwarded].Value) == "Yes");
                    projectDetails.StudyInfo.DataManagementPoCName = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.DMPOC].Value);
                    projectDetails.StudyInfo.CDISCAwarded = (Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISC_Awarded].Value) == "Yes");
                    projectDetails.StudyInfo.CDISCPoCName = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISC_POC].Value);
                    projectDetails.StudyInfo.StatisticsAwarded = (Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.Stat_Awarded].Value) == "Yes");
                    projectDetails.StudyInfo.StatisticsPoCName = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.Stat_POC].Value);
                    projectDetails.StudyInfo.StudyAssignedOnDate = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.ProgAssignedOn, out dateReadStatus);
                    
                    projectDetails.Milestones = new Milestones();
                    bool areActualDatesIncluded = projectDetails.Milestones.AreActualDatesIncluded = (Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.ActualDatesIncluded].Value) == "Yes");
                    projectDetails.Milestones.StudyStatus = worksheet.Cells[rowIndex, Constants.WorkspaceFields.StudyStatus].Value == null ? "NA" :
                        Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.StudyStatus].Value);

                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLivePlanned].Value) != null)
                    {
                        projectDetails.Milestones.PlannedGoLive = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.GoLivePlanned, out dateReadStatus);
                        projectDetails.Milestones.IsPlannedGoLiveSpecified = dateReadStatus;
                    }
                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLiveActual].Value) != null)
                    {
                        projectDetails.Milestones.ActualGoLive = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.GoLiveActual, out dateReadStatus);
                        projectDetails.Milestones.IsActualGoLiveSpecified = dateReadStatus;
                    }

                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVPlanned].Value) != null)
                    {
                        projectDetails.Milestones.PlannedFPFV = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.FPFVPlanned, out dateReadStatus);
                        projectDetails.Milestones.IsPlannedFPFVSpecified = dateReadStatus;
                    }
                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVActual].Value) != null)
                    {
                        projectDetails.Milestones.ActualFPFV = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.FPFVActual, out dateReadStatus);
                        projectDetails.Milestones.IsActualFPFVSpecified = dateReadStatus;
                    }

                    //projectDetails.Milestones.PlannedLPFV = Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVPlanned].Value);
                    ////projectDetails.Milestones.ActualLPFV = areActualDatesIncluded ? Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVActual].Value)
                    ////    : projectDetails.Milestones.PlannedLPFV;
                    //if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVActual].Value) != null)
                    //{
                    //    projectDetails.Milestones.ActualLPFV = Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVActual].Value);
                    //    projectDetails.Milestones.IsActualLPFVSpecified = true;
                    //}

                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVPlanned].Value) != null)
                    {
                        projectDetails.Milestones.PlannedLPLV = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.LPLVPlanned, out dateReadStatus);
                        projectDetails.Milestones.IsPlannedLPLVSpecified = dateReadStatus;
                    }
                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVActual].Value) != null)
                    {
                        projectDetails.Milestones.ActualLPLV = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.LPLVActual, out dateReadStatus);                        
                        projectDetails.Milestones.IsActualLPLVSpecified = dateReadStatus;
                    }

                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLPlanned].Value) != null)
                    {
                        projectDetails.Milestones.PlannedDBL = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.DBLPlanned, out dateReadStatus);
                        projectDetails.Milestones.IsPlannedDBLSpecified = dateReadStatus;
                    }
                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLActual].Value) != null)
                    {
                        projectDetails.Milestones.ActualDBL = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.DBLActual, out dateReadStatus);
                        projectDetails.Milestones.IsActualDBLSpecified = dateReadStatus;
                    }

                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCPlanned].Value) != null)
                    {
                        projectDetails.Milestones.PlannedCDISCSDTM = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.CDISCPlanned, out dateReadStatus);
                        projectDetails.Milestones.IsPlannedCDISCSDTMSpecified = dateReadStatus;
                    }
                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCActual].Value) != null)
                    {
                        projectDetails.Milestones.ActualCDISCSDTM = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.CDISCActual, out dateReadStatus); 
                        projectDetails.Milestones.IsActualCDISCSDTMSpecified = dateReadStatus;
                    }

                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPPlanned].Value) != null)
                    {
                        projectDetails.Milestones.PlannedSAP = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.SAPPlanned, out dateReadStatus);
                        projectDetails.Milestones.IsPlannedSAPSpecified = dateReadStatus;
                    }
                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPActual].Value) != null)
                    {
                        projectDetails.Milestones.ActualSAP = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.SAPActual, out dateReadStatus);
                        projectDetails.Milestones.IsActualSAPSpecified = dateReadStatus;
                    }

                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRPlanned].Value) != null)
                    {
                        projectDetails.Milestones.PlannedBDR = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.BDRPlanned, out dateReadStatus);
                        projectDetails.Milestones.IsPlannedBDRSpecified = dateReadStatus;
                    }
                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRActual].Value) != null)
                    {
                        projectDetails.Milestones.ActualBDR = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.BDRActual, out dateReadStatus); 
                        projectDetails.Milestones.IsActualBDRSpecified = dateReadStatus;
                    }

                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewPlanned].Value) != null)
                    {
                        projectDetails.Milestones.PlannedBDRReview = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.BDRReviewPlanned, out dateReadStatus);
                        projectDetails.Milestones.IsPlannedBDRReviewSpecified = dateReadStatus;
                    }
                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewActual].Value) != null)
                    {
                        projectDetails.Milestones.ActualBDRReview = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.BDRReviewActual, out dateReadStatus);
                        projectDetails.Milestones.IsActualBDRReviewSpecified = dateReadStatus;
                    }

                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARPlanned].Value) != null)
                    {
                        projectDetails.Milestones.PlannedSAR = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.SARPlanned, out dateReadStatus);
                        projectDetails.Milestones.IsPlannedSARSpecified = dateReadStatus;
                    }
                    if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARActual].Value) != null)
                    {
                        projectDetails.Milestones.ActualSAR = ReadDate(worksheet, rowIndex, (int)Constants.WorkspaceFields.SARActual, out dateReadStatus);
                        projectDetails.Milestones.IsActualSARSpecified = dateReadStatus;
                    }

                    if (importData)
                    {
                        //days - hardcoding to 1, as these won't be specified in file to be imported - as confirmed by Santanu on 25-Mar-22
                        projectDetails.Milestones.PlannedGoLiveDays = 1; //string.IsNullOrEmpty(Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLivePlannedDays].Value)) ? 1 : Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLivePlannedDays].Value);
                        projectDetails.Milestones.PlannedFPFVDays = 1; //string.IsNullOrEmpty(Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVPlannedDays].Value)) ? 1 : Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVPlannedDays].Value);
                        //projectDetails.Milestones.PlannedLPFVDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVPlannedDays].Value);
                        projectDetails.Milestones.PlannedLPLVDays = 1; //string.IsNullOrEmpty(Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVPlannedDays].Value)) ? 1 : Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVPlannedDays].Value);
                        projectDetails.Milestones.PlannedDBLDays = 1; //string.IsNullOrEmpty(Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLPlannedDays].Value)) ? 1 : Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLPlannedDays].Value);
                        projectDetails.Milestones.PlannedCDISCSDTMDays = 1; //string.IsNullOrEmpty(Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCPlannedDays].Value)) ? 1 : Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCPlannedDays].Value);
                        projectDetails.Milestones.PlannedSAPDays = 1; //string.IsNullOrEmpty(Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPPlannedDays].Value)) ? 1 : Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPPlannedDays].Value);
                        projectDetails.Milestones.PlannedBDRDays = 1; //string.IsNullOrEmpty(Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRPlannedDays].Value)) ? 1 : Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRPlannedDays].Value);
                        projectDetails.Milestones.PlannedBDRReviewDays = 1; //string.IsNullOrEmpty(Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewPlannedDays].Value)) ? 1 : Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewPlannedDays].Value);
                        projectDetails.Milestones.PlannedSARDays = 1; //string.IsNullOrEmpty(Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARPlannedDays].Value)) ? 1 : Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARPlannedDays].Value);

                        //if (areActualDatesIncluded)
                        //{
                        projectDetails.Milestones.ActualGoLiveDays = 1; //projectDetails.Milestones.IsActualGoLiveSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLiveActualDays].Value) : 1;
                        projectDetails.Milestones.ActualFPFVDays = 1; //projectDetails.Milestones.IsActualFPFVSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVActualDays].Value) : 1;
                        //projectDetails.Milestones.ActualLPFVDays = projectDetails.Milestones.IsActualLPFVSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVActualDays].Value) : 0;
                        projectDetails.Milestones.ActualLPLVDays = 1; //projectDetails.Milestones.IsActualLPLVSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVActualDays].Value) : 1;
                        projectDetails.Milestones.ActualDBLDays = 1; //projectDetails.Milestones.IsActualDBLSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLActualDays].Value) : 1;
                        projectDetails.Milestones.ActualCDISCSDTMDays = 1; //projectDetails.Milestones.IsActualCDISCSDTMSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCActualDays].Value) : 1;
                        projectDetails.Milestones.ActualSAPDays = 1; //projectDetails.Milestones.IsActualSAPSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPActualDays].Value) : 1;
                        projectDetails.Milestones.ActualBDRDays = 1; //projectDetails.Milestones.IsActualBDRSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRActualDays].Value) : 1;
                        projectDetails.Milestones.ActualBDRReviewDays = 1; //projectDetails.Milestones.IsActualBDRReviewSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewActualDays].Value) : 1;
                        projectDetails.Milestones.ActualSARDays = 1; //projectDetails.Milestones.IsActualSARSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARActualDays].Value) : 1;
                    }
                    else
                    {
                        projectDetails.Milestones.PlannedGoLiveDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLivePlannedDays].Value);
                        projectDetails.Milestones.PlannedFPFVDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVPlannedDays].Value);
                        //projectDetails.Milestones.PlannedLPFVDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVPlannedDays].Value);
                        projectDetails.Milestones.PlannedLPLVDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVPlannedDays].Value);
                        projectDetails.Milestones.PlannedDBLDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLPlannedDays].Value);
                        projectDetails.Milestones.PlannedCDISCSDTMDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCPlannedDays].Value);
                        projectDetails.Milestones.PlannedSAPDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPPlannedDays].Value);
                        projectDetails.Milestones.PlannedBDRDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRPlannedDays].Value);
                        projectDetails.Milestones.PlannedBDRReviewDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewPlannedDays].Value);
                        projectDetails.Milestones.PlannedSARDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARPlannedDays].Value);

                        //if (areActualDatesIncluded)
                        //{
                        projectDetails.Milestones.ActualGoLiveDays = projectDetails.Milestones.IsActualGoLiveSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLiveActualDays].Value) : 1;
                        projectDetails.Milestones.ActualFPFVDays = projectDetails.Milestones.IsActualFPFVSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVActualDays].Value) : 1;
                        //projectDetails.Milestones.ActualLPFVDays = projectDetails.Milestones.IsActualLPFVSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVActualDays].Value) : 0;
                        projectDetails.Milestones.ActualLPLVDays = projectDetails.Milestones.IsActualLPLVSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVActualDays].Value) : 1;
                        projectDetails.Milestones.ActualDBLDays = projectDetails.Milestones.IsActualDBLSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLActualDays].Value) : 1;
                        projectDetails.Milestones.ActualCDISCSDTMDays = projectDetails.Milestones.IsActualCDISCSDTMSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCActualDays].Value) : 1;
                        projectDetails.Milestones.ActualSAPDays = projectDetails.Milestones.IsActualSAPSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPActualDays].Value) : 1;
                        projectDetails.Milestones.ActualBDRDays = projectDetails.Milestones.IsActualBDRSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRActualDays].Value) : 1;
                        projectDetails.Milestones.ActualBDRReviewDays = projectDetails.Milestones.IsActualBDRReviewSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewActualDays].Value) : 1;
                        projectDetails.Milestones.ActualSARDays = projectDetails.Milestones.IsActualSARSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARActualDays].Value) : 1;
                        //}
                    }
                    
                    projectDetails.Comments = new Comments();
                    projectDetails.Comments.ClientComments = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.Client_Comments].Value);
                    projectDetails.Comments.CytelComments = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.Cytel_Comments].Value);
                    
                    //added by chetna as some blank rows are getting calculated so to avoide empty details in colllection check if project name is not blank
                    if (projectDetails.ProjectDescription.ProjectName != null )
                    {
                        allProjectDetails.Add(projectDetails);
                    }

                }

                return allProjectDetails;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return null;
            }
            finally
            {
                Excelapp.Quit();
                if (ExcelWorkBook != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelWorkBook); }
                if (Excelapp != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(Excelapp); }
            }            
        }
               
        private DateTime ReadDate(Microsoft.Office.Interop.Excel._Worksheet worksheet, int rowIndex, int colIndex, out bool dateReadStatus)
        {
            try
            {
                dateReadStatus = false;
                if (worksheet.Cells[rowIndex, colIndex].Value == null) return new DateTime(2021, 1, 1);

                DateTime date = Convert.ToDateTime(worksheet.Cells[rowIndex, colIndex].Value);

                dateReadStatus = true;

                if (date == new DateTime(1, 1, 1)) dateReadStatus = false;

                return date;
            }
            catch (Exception e)
            {
                dateReadStatus = false;
                return new DateTime(2021, 1, 1);
            }
        }            

        /// <summary>
        /// Read form data into excel workspace file based on the Project ID
        /// </summary>
        /// <param name="selectedProjectName">selected project id on the grid</param>
        private ProjectDetails ReadProjectFromWorkspace(string selectedProjectName)
        {
            int rowCount, colCount;

            Microsoft.Office.Interop.Excel.Application Excelapp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook ExcelWorkBook = Excelapp.Workbooks.Open(Constants.WorkSpaceFilePath);

            try
            {
                Excelapp.DisplayAlerts = false;
                object misValue = System.Reflection.Missing.Value;
                Excelapp.Visible = false;
                Excelapp.ActiveWindow.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMinimized;

                // Excelsheet in workbook  
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                worksheet = ExcelWorkBook.Worksheets[Constants.ShtProjData];

                //get last row of worksheet
                Microsoft.Office.Interop.Excel.Range usedRange1 = worksheet.UsedRange;
                rowCount = worksheet.Cells.Find("*", System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByRows, Microsoft.Office.Interop.Excel.XlSearchDirection.xlPrevious, false, System.Reflection.Missing.Value, System.Reflection.Missing.Value).Row; ;
                colCount = usedRange1.Columns.Count;
                int rowIndex = 2, colIndex = 2;

                Constants.MainFormData = string.Empty;
                string ProjIDDetailsData = string.Empty;
                ProjectDetails projectDetails = new ProjectDetails();

                for (; rowIndex <= rowCount; rowIndex++)
                {
                    if (Convert.ToString(worksheet.Cells[rowIndex, colIndex].Value).Equals(selectedProjectName))
                    {
                        projectDetails.ProjectDescription = new ProjectDescription();
                        projectDetails.ProjectDescription.ProjectName = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProjectName].Value);
                        projectDetails.ProjectDescription.ProjectCategory = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProjectCategory].Value);
                        projectDetails.ProjectDescription.Description = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProjectDescription].Value);
                        // ToDo - add study duration and study duration unit in workspace; and read it belos
                        //projectDetails.ProjectDescription.StudyDuration = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.StudyDuration].Value);
                        //projectDetails.ProjectDescription.StudyDurationUnit = (int)Constants.StudyDurationUnit.Days;

                        projectDetails.StudyInfo = new StudyInfo();
                        projectDetails.StudyInfo.CSLName = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CSLName].Value);
                        projectDetails.StudyInfo.GCLName = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.GCLName].Value);
                        projectDetails.StudyInfo.CSSName = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CSSName].Value);
                        projectDetails.StudyInfo.Cro = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CROName].Value);
                        projectDetails.StudyInfo.Country = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CountryName].Value);

                        projectDetails.StudyInfo.DataManagementAwarded = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.DMAwarded].Value) == "Yes";
                        projectDetails.StudyInfo.DataManagementPoCName = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.DMPOC].Value);
                        projectDetails.StudyInfo.CDISCAwarded = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISC_Awarded].Value) == "Yes";
                        projectDetails.StudyInfo.CDISCPoCName = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISC_POC].Value);
                        projectDetails.StudyInfo.StatisticsAwarded = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.Stat_Awarded].Value) == "Yes";
                        projectDetails.StudyInfo.StatisticsPoCName = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.Stat_POC].Value);
                        projectDetails.StudyInfo.StudyAssignedOnDate = Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProgAssignedOn].Value);

                        projectDetails.Milestones = new Milestones();
                        bool areActualDatesIncluded = projectDetails.Milestones.AreActualDatesIncluded = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.ActualDatesIncluded].Value) == "Yes";
                        projectDetails.Milestones.StudyStatus = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.StudyStatus].Value);
                        
                        projectDetails.Milestones.PlannedGoLive = Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLivePlanned].Value);
                        //projectDetails.Milestones.ActualGoLive = areActualDatesIncluded ? Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLiveActual].Value)
                        //    : projectDetails.Milestones.PlannedGoLive;
                        if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLiveActual].Value) != null)
                        {
                            projectDetails.Milestones.IsActualGoLiveSpecified = true;
                        }
                        projectDetails.Milestones.ActualGoLive = projectDetails.Milestones.IsActualGoLiveSpecified ? Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLiveActual].Value)
                            : projectDetails.Milestones.PlannedGoLive;

                        projectDetails.Milestones.PlannedFPFV = Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVPlanned].Value);
                        if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVActual].Value) != null)
                        {
                            projectDetails.Milestones.IsActualFPFVSpecified = true;
                        }
                        projectDetails.Milestones.ActualFPFV = projectDetails.Milestones.IsActualFPFVSpecified ? Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVActual].Value)
                            : projectDetails.Milestones.PlannedFPFV;
                        
                        //projectDetails.Milestones.PlannedLPFV = Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVPlanned].Value);
                        //if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVActual].Value) != null)
                        //{
                        //    projectDetails.Milestones.IsActualLPFVSpecified = true;
                        //}
                        //projectDetails.Milestones.ActualLPFV = projectDetails.Milestones.IsActualLPFVSpecified ? Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVActual].Value)
                        //    : projectDetails.Milestones.PlannedLPFV;

                        projectDetails.Milestones.PlannedLPLV = Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVPlanned].Value);
                        if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVActual].Value) != null)
                        {
                            projectDetails.Milestones.IsActualLPLVSpecified = true;
                        }
                        projectDetails.Milestones.ActualLPLV = projectDetails.Milestones.IsActualLPLVSpecified ? Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVActual].Value)
                            : projectDetails.Milestones.PlannedLPLV;

                        projectDetails.Milestones.PlannedDBL = Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLPlanned].Value);
                        if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLActual].Value) != null)
                        {
                            projectDetails.Milestones.IsActualDBLSpecified = true;
                        }
                        projectDetails.Milestones.ActualDBL = projectDetails.Milestones.IsActualDBLSpecified ? Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLActual].Value)
                            : projectDetails.Milestones.PlannedDBL;

                        projectDetails.Milestones.PlannedCDISCSDTM = Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCPlanned].Value);
                        if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCActual].Value) != null)
                        {
                            projectDetails.Milestones.IsActualCDISCSDTMSpecified = true;
                        }
                        projectDetails.Milestones.ActualCDISCSDTM = projectDetails.Milestones.IsActualCDISCSDTMSpecified ? Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCActual].Value)
                            : projectDetails.Milestones.PlannedCDISCSDTM;

                        projectDetails.Milestones.PlannedSAP = Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPPlanned].Value);
                        if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPActual].Value) != null)
                        {
                            projectDetails.Milestones.IsActualSAPSpecified = true;
                        }
                        projectDetails.Milestones.ActualSAP = projectDetails.Milestones.IsActualSAPSpecified ? Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPActual].Value)
                            : projectDetails.Milestones.PlannedSAP;

                        projectDetails.Milestones.PlannedBDR = Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRPlanned].Value);
                        if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRActual].Value) != null)
                        {
                            projectDetails.Milestones.IsActualBDRSpecified= true;
                        }
                        projectDetails.Milestones.ActualBDR = projectDetails.Milestones.IsActualBDRSpecified ? Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRActual].Value)
                            : projectDetails.Milestones.PlannedBDR;

                        projectDetails.Milestones.PlannedBDRReview = Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewPlanned].Value);
                        if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewActual].Value) != null)
                        {
                            projectDetails.Milestones.IsActualBDRReviewSpecified = true;
                        }
                        projectDetails.Milestones.ActualBDRReview = projectDetails.Milestones.IsActualBDRReviewSpecified ? Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewActual].Value)
                            : projectDetails.Milestones.PlannedBDRReview;

                        projectDetails.Milestones.PlannedSAR = Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARPlanned].Value);
                        if ((worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARActual].Value) != null)
                        {
                            projectDetails.Milestones.IsActualSARSpecified= true;
                        }
                        projectDetails.Milestones.ActualSAR = projectDetails.Milestones.IsActualSARSpecified ? Convert.ToDateTime(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARActual].Value)
                            : projectDetails.Milestones.PlannedSAR;

                        projectDetails.Milestones.PlannedGoLiveDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLivePlannedDays].Value);
                        projectDetails.Milestones.PlannedFPFVDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVPlannedDays].Value);
                        //projectDetails.Milestones.PlannedLPFVDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVPlannedDays].Value);
                        projectDetails.Milestones.PlannedLPLVDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVPlannedDays].Value);
                        projectDetails.Milestones.PlannedDBLDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLPlannedDays].Value);
                        projectDetails.Milestones.PlannedCDISCSDTMDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCPlannedDays].Value);
                        projectDetails.Milestones.PlannedSAPDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPPlannedDays].Value);
                        projectDetails.Milestones.PlannedBDRDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRPlannedDays].Value);
                        projectDetails.Milestones.PlannedBDRReviewDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewPlannedDays].Value);
                        projectDetails.Milestones.PlannedSARDays = Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARPlannedDays].Value);

                        //if (areActualDatesIncluded)
                        //{
                        projectDetails.Milestones.ActualGoLiveDays = projectDetails.Milestones.IsActualGoLiveSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLiveActualDays].Value) : 1;
                        projectDetails.Milestones.ActualFPFVDays = projectDetails.Milestones.IsActualFPFVSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVActualDays].Value) : 1;
                        //projectDetails.Milestones.ActualLPFVDays = projectDetails.Milestones.IsActualLPFVSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVActualDays].Value) : 0;
                        projectDetails.Milestones.ActualLPLVDays = projectDetails.Milestones.IsActualLPLVSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVActualDays].Value) : 1;
                        projectDetails.Milestones.ActualDBLDays = projectDetails.Milestones.IsActualDBLSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLActualDays].Value) : 1;
                        projectDetails.Milestones.ActualCDISCSDTMDays = projectDetails.Milestones.IsActualCDISCSDTMSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCActualDays].Value) : 1;
                        projectDetails.Milestones.ActualSAPDays = projectDetails.Milestones.IsActualSAPSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPActualDays].Value) : 1;
                        projectDetails.Milestones.ActualBDRDays = projectDetails.Milestones.IsActualBDRSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRActualDays].Value) : 1;
                        projectDetails.Milestones.ActualBDRReviewDays = projectDetails.Milestones.IsActualBDRReviewSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewActualDays].Value) : 1;
                        projectDetails.Milestones.ActualSARDays = projectDetails.Milestones.IsActualSARSpecified ? Convert.ToInt32(worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARActualDays].Value) : 1;
                        //}

                        projectDetails.Comments = new Comments();
                        projectDetails.Comments.ClientComments = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.Client_Comments].Value);
                        projectDetails.Comments.CytelComments = Convert.ToString(worksheet.Cells[rowIndex, Constants.WorkspaceFields.Cytel_Comments].Value);

                        break;
                    }
                }

                return projectDetails;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return null;
            }
            finally
            {
                Excelapp.Quit();
                if (ExcelWorkBook != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelWorkBook); }
                if (Excelapp != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(Excelapp); }
            }
        }

        /// <summary>
        /// This method populates grid with received project details collection.
        /// </summary>
        /// <param name="projectDetails">Collection of project details</param>
        void AddAllProjectRecordsInGrid(List<ProjectDetails> projectDetails)
        {
            if (projectDetails == null || projectDetails.Count == 0) return;

            foreach (ProjectDetails project in projectDetails)
            {
                string[] rowNew = new string[Constants.GridColumnCount];
                int colIndex = 0;

                rowNew[colIndex++] = "false";
                rowNew[colIndex++] = project.ProjectDescription.ProjectName;
                rowNew[colIndex++] = project.StudyInfo.DataManagementAwarded ? "Yes" : "No";
                rowNew[colIndex++] = project.StudyInfo.DataManagementPoCName;
                rowNew[colIndex++] = project.StudyInfo.CDISCAwarded ? "Yes" : "No";
                rowNew[colIndex++] = project.StudyInfo.CDISCPoCName;
                rowNew[colIndex++] = project.StudyInfo.StatisticsAwarded ? "Yes" : "No";
                rowNew[colIndex++] = project.StudyInfo.StatisticsPoCName;
                rowNew[colIndex++] = project.StudyInfo.StudyAssignedOnDate.ToString(Constants.DateFormat);

                //Not used any where
                //bool areActualDatesIncluded = project.Milestones.AreActualDatesIncluded;
                //Changed flag logic to add the Actulas as default values

                rowNew[colIndex++] = project.Milestones.IsPlannedGoLiveSpecified ? project.Milestones.PlannedGoLive.ToString(Constants.DateFormat) : string.Empty;
                rowNew[colIndex++] = project.Milestones.IsActualGoLiveSpecified ? project.Milestones.ActualGoLive.ToString(Constants.DateFormat) : string.Empty;

                rowNew[colIndex++] = project.Milestones.IsPlannedFPFVSpecified ? project.Milestones.PlannedFPFV.ToString(Constants.DateFormat) : string.Empty;
                rowNew[colIndex++] = project.Milestones.IsActualFPFVSpecified ? project.Milestones.ActualFPFV.ToString(Constants.DateFormat) : string.Empty;

                //rowNew[colIndex++] = project.Milestones.PlannedLPFV.ToString(Constants.DateFormat);
                //rowNew[colIndex++] = project.Milestones.IsActualLPFVSpecified ? project.Milestones.ActualLPFV.ToString(Constants.DateFormat) : string.Empty;

                //rowNew[colIndex++] = project.ProjectDescription.StudyDuration;

                rowNew[colIndex++] = project.Milestones.IsPlannedLPLVSpecified ? project.Milestones.PlannedLPLV.ToString(Constants.DateFormat) : string.Empty;
                rowNew[colIndex++] = project.Milestones.IsActualLPLVSpecified ? project.Milestones.ActualLPLV.ToString(Constants.DateFormat) : string.Empty;

                rowNew[colIndex++] = project.Milestones.IsPlannedDBLSpecified ? project.Milestones.PlannedDBL.ToString(Constants.DateFormat) : string.Empty;
                rowNew[colIndex++] = project.Milestones.IsActualDBLSpecified ? project.Milestones.ActualDBL.ToString(Constants.DateFormat) : string.Empty;

                rowNew[colIndex++] = project.Milestones.IsPlannedCDISCSDTMSpecified ? project.Milestones.PlannedCDISCSDTM.ToString(Constants.DateFormat) : string.Empty;
                rowNew[colIndex++] = project.Milestones.IsActualCDISCSDTMSpecified ? project.Milestones.ActualCDISCSDTM.ToString(Constants.DateFormat) : string.Empty;

                rowNew[colIndex++] = project.Milestones.IsPlannedBDRSpecified ? project.Milestones.PlannedBDR.ToString(Constants.DateFormat) : string.Empty;
                rowNew[colIndex++] = project.Milestones.IsActualBDRSpecified ? project.Milestones.ActualBDR.ToString(Constants.DateFormat) : string.Empty;

                rowNew[colIndex++] = project.Milestones.IsPlannedSAPSpecified ? project.Milestones.PlannedSAP.ToString(Constants.DateFormat) : string.Empty;
                rowNew[colIndex++] = project.Milestones.IsActualSAPSpecified ? project.Milestones.ActualSAP.ToString(Constants.DateFormat) : string.Empty;

                rowNew[colIndex++] = project.Milestones.IsPlannedSARSpecified ? project.Milestones.PlannedSAR.ToString(Constants.DateFormat) : string.Empty;
                rowNew[colIndex++] = project.Milestones.IsActualSARSpecified ? project.Milestones.ActualSAR.ToString(Constants.DateFormat) : string.Empty;
                
                rowNew[colIndex++] = project.Comments.ClientComments;
                rowNew[colIndex++] = project.Comments.CytelComments;

                _dataTable.Rows.Add(rowNew);
            }
            
            bindingSource_main.DataMember = _dataTable.TableName;

            foreach (DataGridViewRow Gridrow in advancedDataGridView1.Rows)
            {
                Gridrow.Cells[GridColumns.Select].ReadOnly = false;
                Gridrow.Cells[GridColumns.DMAwarded].ReadOnly = true;
                Gridrow.Cells[GridColumns.DMPOC].ReadOnly = true;
                Gridrow.Cells[GridColumns.CDISC_Awarded].ReadOnly = true;
                Gridrow.Cells[GridColumns.CDISC_POC].ReadOnly = true;
                Gridrow.Cells[GridColumns.Stat_Awarded].ReadOnly = true;
                Gridrow.Cells[GridColumns.Stat_POC].ReadOnly = true;
                Gridrow.Cells[GridColumns.ProgAssignedOn].ReadOnly = true;
                Gridrow.Cells[GridColumns.GoLivePlanned].ReadOnly = true;
                Gridrow.Cells[GridColumns.GoLiveActual].ReadOnly = true;
                Gridrow.Cells[GridColumns.FPFVPlanned].ReadOnly = true;
                Gridrow.Cells[GridColumns.FPFVActual].ReadOnly = true;
                //Gridrow.Cells[GridColumns.LPFVPlanned].ReadOnly = true;
                //Gridrow.Cells[GridColumns.LPFVActual].ReadOnly = true;
                //Gridrow.Cells[GridColumns.StudyDuration].ReadOnly = true;
                Gridrow.Cells[GridColumns.LPLVPlanned].ReadOnly = true;
                Gridrow.Cells[GridColumns.LPLVActual].ReadOnly = true;
                Gridrow.Cells[GridColumns.DBLPlanned].ReadOnly = true;
                Gridrow.Cells[GridColumns.DBLActual].ReadOnly = true;
                Gridrow.Cells[GridColumns.CDISCPlanned].ReadOnly = true;
                Gridrow.Cells[GridColumns.CDISCActual].ReadOnly = true;
                Gridrow.Cells[GridColumns.SAPPlanned].ReadOnly = true;
                Gridrow.Cells[GridColumns.SAPActual].ReadOnly = true;
                Gridrow.Cells[GridColumns.BDRPlanned].ReadOnly = true;
                Gridrow.Cells[GridColumns.BDRActual].ReadOnly = true;
                Gridrow.Cells[GridColumns.SARPlanned].ReadOnly = true;
                Gridrow.Cells[GridColumns.SARActual].ReadOnly = true;
                Gridrow.Cells[GridColumns.Client_Comments].ReadOnly = true;
                Gridrow.Cells[GridColumns.Cytel_Comments].ReadOnly = true;
            }

            //set columns minimum width
            SetColumnWidth();
        }

        bool AddProjectRecordInGrid()
        {
            string[] rowNew = new string[Constants.GridColumnCount];
            int colIndex = 0;

            try
            {
                rowNew[colIndex++] = "true";
                rowNew[colIndex++] = Constants.ProjectDetails.ProjectDescription.ProjectName;
                rowNew[colIndex++] = Constants.ProjectDetails.StudyInfo.DataManagementAwarded ? "Yes" : "No";
                rowNew[colIndex++] = Constants.ProjectDetails.StudyInfo.DataManagementPoCName;
                rowNew[colIndex++] = Constants.ProjectDetails.StudyInfo.CDISCAwarded ? "Yes" : "No";
                rowNew[colIndex++] = Constants.ProjectDetails.StudyInfo.CDISCPoCName;
                rowNew[colIndex++] = Constants.ProjectDetails.StudyInfo.StatisticsAwarded ? "Yes" : "No";
                rowNew[colIndex++] = Constants.ProjectDetails.StudyInfo.StatisticsPoCName;
                rowNew[colIndex++] = Constants.ProjectDetails.StudyInfo.StudyAssignedOnDate.ToString(Constants.DateFormat);

                bool areActualDatesIncluded = Constants.ProjectDetails.Milestones.AreActualDatesIncluded;

                rowNew[colIndex++] = Constants.ProjectDetails.Milestones.PlannedGoLive.ToString(Constants.DateFormat);
                rowNew[colIndex++] = areActualDatesIncluded ? Constants.ProjectDetails.Milestones.ActualGoLive.ToString(Constants.DateFormat) : string.Empty;
                
                rowNew[colIndex++] = Constants.ProjectDetails.Milestones.PlannedFPFV.ToString(Constants.DateFormat);
                rowNew[colIndex++] = areActualDatesIncluded ? Constants.ProjectDetails.Milestones.ActualFPFV.ToString(Constants.DateFormat) : string.Empty;

                //rowNew[colIndex++] = Constants.ProjectDetails.Milestones.PlannedLPFV.ToString(Constants.DateFormat);
                //rowNew[colIndex++] = areActualDatesIncluded ? Constants.ProjectDetails.Milestones.ActualLPFV.ToString(Constants.DateFormat) : string.Empty;

                //rowNew[colIndex++] = Constants.ProjectDetails.ProjectDescription.StudyDuration;
                
                rowNew[colIndex++] = Constants.ProjectDetails.Milestones.PlannedLPLV.ToString(Constants.DateFormat);
                rowNew[colIndex++] = areActualDatesIncluded ? Constants.ProjectDetails.Milestones.ActualLPLV.ToString(Constants.DateFormat) : string.Empty;

                rowNew[colIndex++] = Constants.ProjectDetails.Milestones.PlannedDBL.ToString(Constants.DateFormat);
                rowNew[colIndex++] = areActualDatesIncluded ? Constants.ProjectDetails.Milestones.ActualDBL.ToString(Constants.DateFormat) : string.Empty;

                rowNew[colIndex++] = Constants.ProjectDetails.Milestones.PlannedCDISCSDTM.ToString(Constants.DateFormat);
                rowNew[colIndex++] = areActualDatesIncluded ? Constants.ProjectDetails.Milestones.ActualCDISCSDTM.ToString(Constants.DateFormat) : string.Empty;

                rowNew[colIndex++] = Constants.ProjectDetails.Milestones.PlannedBDR.ToString(Constants.DateFormat);
                rowNew[colIndex++] = areActualDatesIncluded ? Constants.ProjectDetails.Milestones.ActualBDR.ToString(Constants.DateFormat) : string.Empty;

                rowNew[colIndex++] = Constants.ProjectDetails.Milestones.PlannedSAP.ToString(Constants.DateFormat);
                rowNew[colIndex++] = areActualDatesIncluded ? Constants.ProjectDetails.Milestones.ActualSAP.ToString(Constants.DateFormat) : string.Empty;

                rowNew[colIndex++] = Constants.ProjectDetails.Milestones.PlannedSAR.ToString(Constants.DateFormat);
                rowNew[colIndex++] = areActualDatesIncluded ? Constants.ProjectDetails.Milestones.ActualSAR.ToString(Constants.DateFormat) : string.Empty;

                rowNew[colIndex++] = Constants.ProjectDetails.Comments.ClientComments;
                rowNew[colIndex++] = Constants.ProjectDetails.Comments.CytelComments;

                _dataTable.Rows.Add(rowNew);
                bindingSource_main.DataMember = _dataTable.TableName;

                foreach (DataGridViewRow Gridrow in advancedDataGridView1.Rows)
                {
                    Gridrow.Cells[GridColumns.Select].ReadOnly = false;
                    Gridrow.Cells[GridColumns.ProjectName].ReadOnly = true;
                    Gridrow.Cells[GridColumns.DMAwarded].ReadOnly = true;
                    Gridrow.Cells[GridColumns.DMPOC].ReadOnly = true;
                    Gridrow.Cells[GridColumns.CDISC_Awarded].ReadOnly = true;
                    Gridrow.Cells[GridColumns.CDISC_POC].ReadOnly = true;
                    Gridrow.Cells[GridColumns.Stat_Awarded].ReadOnly = true;
                    Gridrow.Cells[GridColumns.Stat_POC].ReadOnly = true;
                    Gridrow.Cells[GridColumns.ProgAssignedOn].ReadOnly = true;
                    Gridrow.Cells[GridColumns.GoLivePlanned].ReadOnly = true;
                    Gridrow.Cells[GridColumns.GoLiveActual].ReadOnly = true;
                    Gridrow.Cells[GridColumns.FPFVPlanned].ReadOnly = true;
                    Gridrow.Cells[GridColumns.FPFVActual].ReadOnly = true;
                    //Gridrow.Cells[GridColumns.LPFVPlanned].ReadOnly = true;
                    //Gridrow.Cells[GridColumns.LPFVActual].ReadOnly = true;
                    //Gridrow.Cells[GridColumns.StudyDuration].ReadOnly = true;
                    Gridrow.Cells[GridColumns.LPLVPlanned].ReadOnly = true;
                    Gridrow.Cells[GridColumns.LPLVActual].ReadOnly = true;
                    Gridrow.Cells[GridColumns.DBLPlanned].ReadOnly = true;
                    Gridrow.Cells[GridColumns.DBLActual].ReadOnly = true;
                    Gridrow.Cells[GridColumns.CDISCPlanned].ReadOnly = true;
                    Gridrow.Cells[GridColumns.CDISCActual].ReadOnly = true;
                    Gridrow.Cells[GridColumns.SAPPlanned].ReadOnly = true;
                    Gridrow.Cells[GridColumns.SAPActual].ReadOnly = true;
                    Gridrow.Cells[GridColumns.BDRPlanned].ReadOnly = true;
                    Gridrow.Cells[GridColumns.BDRActual].ReadOnly = true;
                    Gridrow.Cells[GridColumns.SARPlanned].ReadOnly = true;
                    Gridrow.Cells[GridColumns.SARActual].ReadOnly = true;
                    Gridrow.Cells[GridColumns.Client_Comments].ReadOnly = true;
                    Gridrow.Cells[GridColumns.Cytel_Comments].ReadOnly = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        /// <summary>
        /// function sets column width of grid, minimum column width and freeze first two columns
        /// </summary>
        private void SetColumnWidth()
        {
            try
            {
                foreach (DataGridViewColumn column in advancedDataGridView1.Columns)
                {
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    column.MinimumWidth = 40;
                }

                //freeze column
                advancedDataGridView1.Columns[GridColumns.Select].Frozen = true;
                advancedDataGridView1.Columns[GridColumns.ProjectName].Frozen = true;

                advancedDataGridView1.ScrollBars = ScrollBars.Both;
            }
            catch (Exception ex)
            {
                return;
            }
        }


        private bool UpdateProjectInGrid(int selectedRowIndex)
        {
            try
            {
                int colIndex = 1;
                
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.ProjectDescription.ProjectName;
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.StudyInfo.DataManagementAwarded ? "Yes" : "No";
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.StudyInfo.DataManagementPoCName;
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.StudyInfo.CDISCAwarded ? "Yes" : "No";
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.StudyInfo.CDISCPoCName;
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.StudyInfo.StatisticsAwarded ? "Yes" : "No";
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.StudyInfo.StatisticsPoCName;
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.StudyInfo.StudyAssignedOnDate.ToString(Constants.DateFormat);
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.Milestones.PlannedGoLive.ToString(Constants.DateFormat);
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.Milestones.ActualGoLive.ToString(Constants.DateFormat);
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.Milestones.PlannedFPFV.ToString(Constants.DateFormat);
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.Milestones.ActualFPFV.ToString(Constants.DateFormat);
                //advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.Milestones.PlannedLPFV.ToString(Constants.DateFormat);
                //advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.Milestones.ActualLPFV.ToString(Constants.DateFormat);
                //advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.ProjectDescription.StudyDuration;

                bool areActualDatesIncluded = Constants.ProjectDetails.Milestones.AreActualDatesIncluded;

                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.Milestones.PlannedLPLV.ToString(Constants.DateFormat);
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = areActualDatesIncluded ? Constants.ProjectDetails.Milestones.ActualLPLV.ToString(Constants.DateFormat) : string.Empty;
                
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.Milestones.PlannedDBL.ToString(Constants.DateFormat);
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = areActualDatesIncluded ? Constants.ProjectDetails.Milestones.ActualDBL.ToString(Constants.DateFormat) : string.Empty;

                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.Milestones.PlannedCDISCSDTM.ToString(Constants.DateFormat);
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = areActualDatesIncluded ? Constants.ProjectDetails.Milestones.ActualCDISCSDTM.ToString(Constants.DateFormat) : string.Empty;

                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.Milestones.PlannedBDR.ToString(Constants.DateFormat);
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = areActualDatesIncluded ? Constants.ProjectDetails.Milestones.ActualBDR.ToString(Constants.DateFormat) : string.Empty;

                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.Milestones.PlannedSAP.ToString(Constants.DateFormat);
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = areActualDatesIncluded ? Constants.ProjectDetails.Milestones.ActualSAP.ToString(Constants.DateFormat) : string.Empty;

                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.Milestones.PlannedSAR.ToString(Constants.DateFormat);
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = areActualDatesIncluded ? Constants.ProjectDetails.Milestones.ActualSAR.ToString(Constants.DateFormat) : string.Empty;

                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.Comments.ClientComments;
                advancedDataGridView1.Rows[selectedRowIndex].Cells[colIndex++].Value = Constants.ProjectDetails.Comments.CytelComments;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        /// <summary>
        /// This method adds current project details (available after 'Add Project') into current workspace.
        /// </summary>
        /// <returns>True on success, otherwise false.</returns>
        bool AddProjectRecordInWorkspace()
        {
            int rowCount = 0;

            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook ExcelWorkBook = app.Workbooks.Open(Constants.WorkSpaceFilePath); 

            try
            {
                app.DisplayAlerts = false;
                object misValue = System.Reflection.Missing.Value;
                app.Visible = false;
                app.ActiveWindow.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMinimized;

                // Excelsheet in workbook  
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                worksheet = ExcelWorkBook.Worksheets[Constants.ShtProjData];

                //get last row of worksheet
                Microsoft.Office.Interop.Excel.Range usedRange1 = worksheet.UsedRange;

                //Code changes done by chetna
                rowCount = worksheet.Cells.Find("*", System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByRows, Microsoft.Office.Interop.Excel.XlSearchDirection.xlPrevious, false, System.Reflection.Missing.Value, System.Reflection.Missing.Value).Row; 

                //string[] lines = Constants.MainFormData.Split(new[] { Constants.Seperator }, StringSplitOptions.None);
                int cellCnt = Constants.ProjectIdCol;

                //Sr. No. inster serial number by formula
                worksheet.Cells[rowCount + 1, cellCnt - 1].Formula = "=ROW()-1";
                usedRange1.Calculate();
                //foreach (string value in projectDetails)
                //{
                //    worksheet.Cells[rowCount + 1, cellCnt] = value;
                //    cellCnt++;
                //}
                int rowIndex = rowCount + 1;

                //commented below line  as it was giving wrong sr no
                //worksheet.Cells[rowIndex, Constants.WorkspaceFields.Select].Value = rowIndex.ToString();
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProjectName].Value = Constants.ProjectDetails.ProjectDescription.ProjectName;

                // ToDo - reverse flow to read from object and store in cells

                worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProjectCategory].Value = Constants.ProjectDetails.ProjectDescription.ProjectCategory.Equals("Other")? Constants.ProjectDetails.ProjectDescription.OtherCategory : Constants.ProjectDetails.ProjectDescription.ProjectCategory;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProjectDescription].Value = Constants.ProjectDetails.ProjectDescription.Description;
                //worksheet.Cells[rowIndex, Constants.WorkspaceFields.StudyDuration].Value = Constants.ProjectDetails.ProjectDescription.StudyDuration.ToString();
                //Constants.ProjectDetails.ProjectDescription.StudyDurationUnit = (int) Constants.StudyDurationUnit.Days;

                worksheet.Cells[rowIndex, Constants.WorkspaceFields.CSLName].Value = Constants.ProjectDetails.StudyInfo.CSLName;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.GCLName].Value = Constants.ProjectDetails.StudyInfo.GCLName;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.CSSName].Value = Constants.ProjectDetails.StudyInfo.CSSName;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.CROName].Value = (Constants.ProjectDetails.StudyInfo.Cro.Equals("Other")) ? Constants.ProjectDetails.StudyInfo.OtherCRO : Constants.ProjectDetails.StudyInfo.Cro;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.CountryName].Value = Constants.ProjectDetails.StudyInfo.Country.Equals("Other") ? Constants.ProjectDetails.StudyInfo.OtherCountry : Constants.ProjectDetails.StudyInfo.Country;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.StudyStatus].Value = Constants.ProjectDetails.Milestones.StudyStatus;

                worksheet.Cells[rowIndex, Constants.WorkspaceFields.DMAwarded].Value = Constants.ProjectDetails.StudyInfo.DataManagementAwarded ? "Yes" : "No";
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.DMPOC].Value = Constants.ProjectDetails.StudyInfo.DataManagementPoCName;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISC_Awarded].Value = Constants.ProjectDetails.StudyInfo.CDISCAwarded ? "Yes" : "No";
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISC_POC].Value = Constants.ProjectDetails.StudyInfo.CDISCPoCName;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.Stat_Awarded].Value = Constants.ProjectDetails.StudyInfo.StatisticsAwarded ? "Yes" : "No";
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.Stat_POC].Value = Constants.ProjectDetails.StudyInfo.StatisticsPoCName;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProgAssignedOn].Value =  Constants.ProjectDetails.StudyInfo.StudyAssignedOnDate.ToString(Constants.DateFormat);

                bool areActualDatesIncluded = Constants.ProjectDetails.Milestones.AreActualDatesIncluded;
                
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLivePlanned].Value = Constants.ProjectDetails.Milestones.IsPlannedGoLiveSpecified ? Constants.ProjectDetails.Milestones.PlannedGoLive.ToString(Constants.DateFormat) : string.Empty;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLiveActual].Value = Constants.ProjectDetails.Milestones.IsActualGoLiveSpecified ? Constants.ProjectDetails.Milestones.ActualGoLive.ToString(Constants.DateFormat) : string.Empty;
                
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVPlanned].Value = Constants.ProjectDetails.Milestones.IsPlannedFPFVSpecified ? Constants.ProjectDetails.Milestones.PlannedFPFV.ToString(Constants.DateFormat) : string.Empty;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVActual].Value = Constants.ProjectDetails.Milestones.IsActualFPFVSpecified ? Constants.ProjectDetails.Milestones.ActualFPFV.ToString(Constants.DateFormat) : string.Empty;

                //worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVPlanned].Value = Constants.ProjectDetails.Milestones.PlannedLPFV.ToString(Constants.DateFormat);
                //worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVActual].Value = Constants.ProjectDetails.Milestones.IsActualLPFVSpecified ? Constants.ProjectDetails.Milestones.ActualLPFV.ToString(Constants.DateFormat) : string.Empty;

                worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVPlanned].Value = Constants.ProjectDetails.Milestones.IsPlannedLPLVSpecified ? Constants.ProjectDetails.Milestones.PlannedLPLV.ToString(Constants.DateFormat) : string.Empty;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVActual].Value = Constants.ProjectDetails.Milestones.IsActualLPLVSpecified ? Constants.ProjectDetails.Milestones.ActualLPLV.ToString(Constants.DateFormat) : string.Empty;

                worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLPlanned].Value = Constants.ProjectDetails.Milestones.IsPlannedDBLSpecified ? Constants.ProjectDetails.Milestones.PlannedDBL.ToString(Constants.DateFormat) : string.Empty;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLActual].Value = Constants.ProjectDetails.Milestones.IsActualDBLSpecified ? Constants.ProjectDetails.Milestones.ActualDBL.ToString(Constants.DateFormat) : string.Empty;

                worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCPlanned].Value = Constants.ProjectDetails.Milestones.IsPlannedCDISCSDTMSpecified ? Constants.ProjectDetails.Milestones.PlannedCDISCSDTM.ToString(Constants.DateFormat) : string.Empty;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCActual].Value = Constants.ProjectDetails.Milestones.IsActualCDISCSDTMSpecified ? Constants.ProjectDetails.Milestones.ActualCDISCSDTM.ToString(Constants.DateFormat) : string.Empty;

                worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPPlanned].Value = Constants.ProjectDetails.Milestones.IsPlannedSAPSpecified ? Constants.ProjectDetails.Milestones.PlannedSAP.ToString(Constants.DateFormat) : string.Empty;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPActual].Value = Constants.ProjectDetails.Milestones.IsActualSAPSpecified ? Constants.ProjectDetails.Milestones.ActualSAP.ToString(Constants.DateFormat) : string.Empty;

                worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRPlanned].Value = Constants.ProjectDetails.Milestones.IsPlannedBDRSpecified ? Constants.ProjectDetails.Milestones.PlannedBDR.ToString(Constants.DateFormat) : string.Empty;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRActual].Value = Constants.ProjectDetails.Milestones.IsActualBDRSpecified ? Constants.ProjectDetails.Milestones.ActualBDR.ToString(Constants.DateFormat) : string.Empty;

                worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewPlanned].Value = Constants.ProjectDetails.Milestones.IsPlannedBDRReviewSpecified ? Constants.ProjectDetails.Milestones.PlannedBDRReview.ToString(Constants.DateFormat) : string.Empty;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewActual].Value = Constants.ProjectDetails.Milestones.IsActualBDRReviewSpecified ? Constants.ProjectDetails.Milestones.ActualBDRReview.ToString(Constants.DateFormat) : string.Empty;

                worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARPlanned].Value = Constants.ProjectDetails.Milestones.IsPlannedSARSpecified ? Constants.ProjectDetails.Milestones.PlannedSAR.ToString(Constants.DateFormat) : string.Empty;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARActual].Value = Constants.ProjectDetails.Milestones.IsActualSARSpecified ? Constants.ProjectDetails.Milestones.ActualSAR.ToString(Constants.DateFormat) : string.Empty;

                worksheet.Cells[rowIndex, Constants.WorkspaceFields.Client_Comments].Value = Constants.ProjectDetails.Comments.ClientComments;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.Cytel_Comments].Value = Constants.ProjectDetails.Comments.CytelComments;
                //days
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLivePlannedDays].Value = Constants.ProjectDetails.Milestones.IsPlannedGoLiveSpecified ? Constants.ProjectDetails.Milestones.PlannedGoLiveDays : 1;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVPlannedDays].Value = Constants.ProjectDetails.Milestones.IsPlannedFPFVSpecified ? Constants.ProjectDetails.Milestones.PlannedFPFVDays : 1;
                //worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVPlannedDays].Value = Constants.ProjectDetails.Milestones.PlannedLPFVDays;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVPlannedDays].Value = Constants.ProjectDetails.Milestones.IsPlannedLPLVSpecified ? Constants.ProjectDetails.Milestones.PlannedLPLVDays : 1;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLPlannedDays].Value = Constants.ProjectDetails.Milestones.IsPlannedDBLSpecified ? Constants.ProjectDetails.Milestones.PlannedDBLDays : 1;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCPlannedDays].Value = Constants.ProjectDetails.Milestones.IsPlannedCDISCSDTMSpecified ? Constants.ProjectDetails.Milestones.PlannedCDISCSDTMDays : 1;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPPlannedDays].Value = Constants.ProjectDetails.Milestones.IsPlannedSAPSpecified ? Constants.ProjectDetails.Milestones.PlannedSAPDays : 1;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRPlannedDays].Value = Constants.ProjectDetails.Milestones.IsPlannedBDRSpecified ? Constants.ProjectDetails.Milestones.PlannedBDRDays : 1;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewPlannedDays].Value = Constants.ProjectDetails.Milestones.IsPlannedBDRReviewSpecified ? Constants.ProjectDetails.Milestones.PlannedBDRReviewDays : 1;
                worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARPlannedDays].Value = Constants.ProjectDetails.Milestones.IsPlannedSARSpecified ? Constants.ProjectDetails.Milestones.PlannedSARDays : 1;

                //if (areActualDatesIncluded)
                {
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLiveActualDays].Value = Constants.ProjectDetails.Milestones.IsActualGoLiveSpecified ? Constants.ProjectDetails.Milestones.ActualGoLiveDays : 1;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVActualDays].Value = Constants.ProjectDetails.Milestones.IsActualFPFVSpecified ? Constants.ProjectDetails.Milestones.ActualFPFVDays : 1;
                    //worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVActualDays].Value = Constants.ProjectDetails.Milestones.IsActualLPFVSpecified ? Constants.ProjectDetails.Milestones.ActualLPFVDays : 0;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVActualDays].Value = Constants.ProjectDetails.Milestones.IsActualLPLVSpecified ? Constants.ProjectDetails.Milestones.ActualLPLVDays : 1;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLActualDays].Value = Constants.ProjectDetails.Milestones.IsActualDBLSpecified ? Constants.ProjectDetails.Milestones.ActualDBLDays : 1;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCActualDays].Value = Constants.ProjectDetails.Milestones.IsActualCDISCSDTMSpecified ? Constants.ProjectDetails.Milestones.ActualCDISCSDTMDays : 1;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPActualDays].Value = Constants.ProjectDetails.Milestones.IsActualSAPSpecified ? Constants.ProjectDetails.Milestones.ActualSAPDays : 1;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRActualDays].Value = Constants.ProjectDetails.Milestones.IsActualBDRSpecified ? Constants.ProjectDetails.Milestones.ActualBDRDays : 1;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewActualDays].Value = Constants.ProjectDetails.Milestones.IsActualBDRReviewSpecified ? Constants.ProjectDetails.Milestones.ActualBDRReviewDays : 1;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARActualDays].Value = Constants.ProjectDetails.Milestones.IsActualSARSpecified ? Constants.ProjectDetails.Milestones.ActualSARDays : 1;
                }

                worksheet.Cells[rowIndex, Constants.WorkspaceFields.ActualDatesIncluded].Value = Constants.ProjectDetails.Milestones.AreActualDatesIncluded ? "Yes" : "No";


                ExcelWorkBook.Save();
                ExcelWorkBook.Close(true, System.Reflection.Missing.Value, System.Reflection.Missing.Value);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                app.Quit();
                if (ExcelWorkBook != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelWorkBook); }
                if (app != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(app); }
            }

            return true;
        }


        bool AddImportedProjectRecordInWorkspace(List<ProjectDetails> importedProjectDetails)
        {
            int rowCount = 0;

            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook ExcelWorkBook = app.Workbooks.Open(Constants.WorkSpaceFilePath);

            try
            {
                app.DisplayAlerts = false;
                object misValue = System.Reflection.Missing.Value;
                app.Visible = false;
                app.ActiveWindow.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMinimized;

                // Excelsheet in workbook  
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                worksheet = ExcelWorkBook.Worksheets[Constants.ShtProjData];                

                foreach (ProjectDetails projectDetails in importedProjectDetails)
                {
                    //get last row of worksheet
                    Microsoft.Office.Interop.Excel.Range usedRange1 = worksheet.UsedRange;

                    //Code changes done by chetna
                    rowCount = worksheet.Cells.Find("*", System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByRows, Microsoft.Office.Interop.Excel.XlSearchDirection.xlPrevious, false, System.Reflection.Missing.Value, System.Reflection.Missing.Value).Row;

                    //string[] lines = Constants.MainFormData.Split(new[] { Constants.Seperator }, StringSplitOptions.None);
                    int cellCnt = Constants.ProjectIdCol;

                    //Sr. No. inster serial number by formula
                    worksheet.Cells[rowCount + 1, cellCnt - 1].Formula = "=ROW()-1";
                    usedRange1.Calculate();
                    //foreach (string value in projectDetails)
                    //{
                    //    worksheet.Cells[rowCount + 1, cellCnt] = value;
                    //    cellCnt++;
                    //}
                    int rowIndex = rowCount + 1;

                    //commented below line  as it was giving wrong sr no
                    //worksheet.Cells[rowIndex, Constants.WorkspaceFields.Select].Value = rowIndex.ToString();
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProjectName].Value = projectDetails.ProjectDescription.ProjectName;

                    // ToDo - reverse flow to read from object and store in cells

                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProjectCategory].Value = projectDetails.ProjectDescription.ProjectCategory.Equals("Other") ? projectDetails.ProjectDescription.OtherCategory : projectDetails.ProjectDescription.ProjectCategory;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProjectDescription].Value = projectDetails.ProjectDescription.Description;
                    //worksheet.Cells[rowIndex, Constants.WorkspaceFields.StudyDuration].Value = projectDetails.ProjectDescription.StudyDuration.ToString();
                    //projectDetails.ProjectDescription.StudyDurationUnit = (int) Constants.StudyDurationUnit.Days;

                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.CSLName].Value = projectDetails.StudyInfo.CSLName;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.GCLName].Value = projectDetails.StudyInfo.GCLName;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.CSSName].Value = projectDetails.StudyInfo.CSSName;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.CROName].Value = (projectDetails.StudyInfo.Cro.Equals("Other")) ? projectDetails.StudyInfo.OtherCRO : projectDetails.StudyInfo.Cro;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.CountryName].Value = projectDetails.StudyInfo.Country.Equals("Other") ? projectDetails.StudyInfo.OtherCountry : projectDetails.StudyInfo.Country;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.StudyStatus].Value = projectDetails.Milestones.StudyStatus;

                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.DMAwarded].Value = projectDetails.StudyInfo.DataManagementAwarded ? "Yes" : "No";
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.DMPOC].Value = projectDetails.StudyInfo.DataManagementPoCName;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISC_Awarded].Value = projectDetails.StudyInfo.CDISCAwarded ? "Yes" : "No";
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISC_POC].Value = projectDetails.StudyInfo.CDISCPoCName;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.Stat_Awarded].Value = projectDetails.StudyInfo.StatisticsAwarded ? "Yes" : "No";
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.Stat_POC].Value = projectDetails.StudyInfo.StatisticsPoCName;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProgAssignedOn].Value = projectDetails.StudyInfo.StudyAssignedOnDate.ToString(Constants.DateFormat);

                    //Not used any where
                    //bool areActualDatesIncluded = projectDetails.Milestones.AreActualDatesIncluded;
                    //Changed logic of the flag to add the defaul date value of the Actuals

                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLivePlanned].Value = projectDetails.Milestones.IsPlannedGoLiveSpecified ? projectDetails.Milestones.PlannedGoLive.ToString(Constants.DateFormat) : string.Empty;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLiveActual].Value = projectDetails.Milestones.IsActualGoLiveSpecified ? projectDetails.Milestones.ActualGoLive.ToString(Constants.DateFormat) : string.Empty;

                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVPlanned].Value = projectDetails.Milestones.IsPlannedFPFVSpecified ? projectDetails.Milestones.PlannedFPFV.ToString(Constants.DateFormat) : string.Empty;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVActual].Value = projectDetails.Milestones.IsActualFPFVSpecified ? projectDetails.Milestones.ActualFPFV.ToString(Constants.DateFormat) : string.Empty;

                    //worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVPlanned].Value = projectDetails.Milestones.PlannedLPFV.ToString(Constants.DateFormat);
                    //worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVActual].Value = projectDetails.Milestones.IsActualLPFVSpecified ? projectDetails.Milestones.ActualLPFV.ToString(Constants.DateFormat) : string.Empty;

                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVPlanned].Value = projectDetails.Milestones.IsPlannedLPLVSpecified ? projectDetails.Milestones.PlannedLPLV.ToString(Constants.DateFormat) : string.Empty;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVActual].Value = projectDetails.Milestones.IsActualLPLVSpecified ? projectDetails.Milestones.ActualLPLV.ToString(Constants.DateFormat) : string.Empty;

                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLPlanned].Value = projectDetails.Milestones.IsPlannedDBLSpecified ? projectDetails.Milestones.PlannedDBL.ToString(Constants.DateFormat) : string.Empty;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLActual].Value = projectDetails.Milestones.IsActualDBLSpecified ? projectDetails.Milestones.ActualDBL.ToString(Constants.DateFormat) : string.Empty;

                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCPlanned].Value = projectDetails.Milestones.IsPlannedCDISCSDTMSpecified ? projectDetails.Milestones.PlannedCDISCSDTM.ToString(Constants.DateFormat) : string.Empty;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCActual].Value = projectDetails.Milestones.IsActualCDISCSDTMSpecified ? projectDetails.Milestones.ActualCDISCSDTM.ToString(Constants.DateFormat) : string.Empty;

                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPPlanned].Value = projectDetails.Milestones.IsPlannedSAPSpecified ? projectDetails.Milestones.PlannedSAP.ToString(Constants.DateFormat) : string.Empty;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPActual].Value = projectDetails.Milestones.IsActualSAPSpecified ? projectDetails.Milestones.ActualSAP.ToString(Constants.DateFormat) : string.Empty;

                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRPlanned].Value = projectDetails.Milestones.IsPlannedBDRSpecified ? projectDetails.Milestones.PlannedBDR.ToString(Constants.DateFormat) : string.Empty;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRActual].Value = projectDetails.Milestones.IsActualBDRSpecified ? projectDetails.Milestones.ActualBDR.ToString(Constants.DateFormat) : string.Empty;

                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewPlanned].Value = projectDetails.Milestones.IsPlannedBDRReviewSpecified ? projectDetails.Milestones.PlannedBDRReview.ToString(Constants.DateFormat) : string.Empty;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewActual].Value = projectDetails.Milestones.IsActualBDRReviewSpecified ? projectDetails.Milestones.ActualBDRReview.ToString(Constants.DateFormat) : string.Empty;

                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARPlanned].Value = projectDetails.Milestones.IsPlannedSARSpecified ? projectDetails.Milestones.PlannedSAR.ToString(Constants.DateFormat) : string.Empty;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARActual].Value = projectDetails.Milestones.IsActualSARSpecified ? projectDetails.Milestones.ActualSAR.ToString(Constants.DateFormat) : string.Empty;

                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.Client_Comments].Value = projectDetails.Comments.ClientComments;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.Cytel_Comments].Value = projectDetails.Comments.CytelComments;

                    //days - hardcoding to 1, as these won't be specified in file to be imported - as confirmed by Santanu on 25-Mar-22
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLivePlannedDays].Value = 1; // projectDetails.Milestones.IsPlannedGoLiveSpecified ? projectDetails.Milestones.PlannedGoLiveDays : 1;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVPlannedDays].Value = 1; //projectDetails.Milestones.IsPlannedFPFVSpecified ? projectDetails.Milestones.PlannedFPFVDays : 1;
                    //worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVPlannedDays].Value = projectDetails.Milestones.PlannedLPFVDays;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVPlannedDays].Value = 1; //projectDetails.Milestones.IsPlannedLPLVSpecified ? projectDetails.Milestones.PlannedLPLVDays : 1;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLPlannedDays].Value = 1; //projectDetails.Milestones.IsPlannedDBLSpecified ? projectDetails.Milestones.PlannedDBLDays : 1;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCPlannedDays].Value = 1; //projectDetails.Milestones.IsPlannedCDISCSDTMSpecified ? projectDetails.Milestones.PlannedCDISCSDTMDays : 1;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPPlannedDays].Value = 1; //projectDetails.Milestones.IsPlannedSAPSpecified ? projectDetails.Milestones.PlannedSAPDays : 1;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRPlannedDays].Value = 1; //projectDetails.Milestones.IsPlannedBDRSpecified ? projectDetails.Milestones.PlannedBDRDays : 1;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewPlannedDays].Value = 1; //projectDetails.Milestones.IsPlannedBDRReviewSpecified ? projectDetails.Milestones.PlannedBDRReviewDays : 1;
                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARPlannedDays].Value = 1; //projectDetails.Milestones.IsPlannedSARSpecified ? projectDetails.Milestones.PlannedSARDays : 1;

                    //if (areActualDatesIncluded)
                    {
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLiveActualDays].Value = 1; //projectDetails.Milestones.IsActualGoLiveSpecified ? projectDetails.Milestones.ActualGoLiveDays : 1;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVActualDays].Value = 1; //projectDetails.Milestones.IsActualFPFVSpecified ? projectDetails.Milestones.ActualFPFVDays : 1;
                        //worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVActualDays].Value = projectDetails.Milestones.IsActualLPFVSpecified ? projectDetails.Milestones.ActualLPFVDays : 0;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVActualDays].Value = 1; //projectDetails.Milestones.IsActualLPLVSpecified ? projectDetails.Milestones.ActualLPLVDays : 1;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLActualDays].Value = 1; //projectDetails.Milestones.IsActualDBLSpecified ? projectDetails.Milestones.ActualDBLDays : 1;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCActualDays].Value = 1; //projectDetails.Milestones.IsActualCDISCSDTMSpecified ? projectDetails.Milestones.ActualCDISCSDTMDays : 1;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPActualDays].Value = 1; //projectDetails.Milestones.IsActualSAPSpecified ? projectDetails.Milestones.ActualSAPDays : 1;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRActualDays].Value = 1; //projectDetails.Milestones.IsActualBDRSpecified ? projectDetails.Milestones.ActualBDRDays : 1;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewActualDays].Value = 1; //projectDetails.Milestones.IsActualBDRReviewSpecified ? projectDetails.Milestones.ActualBDRReviewDays : 1;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARActualDays].Value = 1; //projectDetails.Milestones.IsActualSARSpecified ? projectDetails.Milestones.ActualSARDays : 1;
                    }

                    worksheet.Cells[rowIndex, Constants.WorkspaceFields.ActualDatesIncluded].Value = projectDetails.Milestones.AreActualDatesIncluded ? "Yes" : "No";

                    ExcelWorkBook.Save();
                }

                ExcelWorkBook.Close(true, System.Reflection.Missing.Value, System.Reflection.Missing.Value);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                app.Quit();
                if (ExcelWorkBook != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelWorkBook); }
                if (app != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(app); }
            }

            return true;
        }
        /// <summary>
        /// Update the data into project workspace
        /// </summary>
        /// <param name="selectedProjectName"></param>
        private void UpdateProjectInWorkspace(string selectedProjectName)
        {
            int rowCount, colCount;

            Microsoft.Office.Interop.Excel.Application Excelapp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook ExcelWorkBook = Excelapp.Workbooks.Open(Constants.WorkSpaceFilePath);

            try
            {
                Excelapp.DisplayAlerts = false;
                object misValue = System.Reflection.Missing.Value;
                Excelapp.Visible = false;
                Excelapp.ActiveWindow.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMinimized;

                // Excelsheet in workbook  
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                worksheet = ExcelWorkBook.Worksheets[Constants.ShtProjData];

                //get last row of worksheet
                Microsoft.Office.Interop.Excel.Range usedRange1 = worksheet.UsedRange;
                rowCount = worksheet.Cells.Find("*", System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByRows, Microsoft.Office.Interop.Excel.XlSearchDirection.xlPrevious, false, System.Reflection.Missing.Value, System.Reflection.Missing.Value).Row;
                colCount = usedRange1.Columns.Count;
                int rowIndex = 2, colIndex = 2;


                Constants.MainFormData = string.Empty;
                string ProjIDDetailsData = string.Empty;

                //foreach (Microsoft.Office.Interop.Excel.Range Rngrow in filteredRange.Rows)
                for (; rowIndex <= rowCount; rowIndex++)
                {
                    if (Convert.ToString(worksheet.Cells[rowIndex, colIndex].Value).Equals(selectedProjectName))
                    {
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProjectName].Value = Constants.ProjectDetails.ProjectDescription.ProjectName;

                        // ToDo - reverse flow to read from object and store in cells

                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProjectCategory].Value = Constants.ProjectDetails.ProjectDescription.ProjectCategory;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProjectDescription].Value = Constants.ProjectDetails.ProjectDescription.Description;
                        //worksheet.Cells[rowIndex, Constants.WorkspaceFields.StudyDuration].Value = Constants.ProjectDetails.ProjectDescription.StudyDuration.ToString();
                        //Constants.ProjectDetails.ProjectDescription.StudyDurationUnit = (int) Constants.StudyDurationUnit.Days;

                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.CSLName].Value = Constants.ProjectDetails.StudyInfo.CSLName;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.GCLName].Value = Constants.ProjectDetails.StudyInfo.GCLName ;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.CSSName].Value = Constants.ProjectDetails.StudyInfo.CSSName;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.CROName].Value = Constants.ProjectDetails.StudyInfo.Cro.Equals("Other") ? Constants.ProjectDetails.StudyInfo.OtherCRO : Constants.ProjectDetails.StudyInfo.Cro;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.CountryName].Value = Constants.ProjectDetails.StudyInfo.Country.Equals("Other") ? Constants.ProjectDetails.StudyInfo.OtherCountry : Constants.ProjectDetails.StudyInfo.Country;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.StudyStatus].Value = Constants.ProjectDetails.Milestones.StudyStatus;

                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.DMAwarded].Value = Constants.ProjectDetails.StudyInfo.DataManagementAwarded ? "Yes" : "No";
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.DMPOC].Value = Constants.ProjectDetails.StudyInfo.DataManagementPoCName;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISC_Awarded].Value = Constants.ProjectDetails.StudyInfo.CDISCAwarded ? "Yes" : "No";
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISC_POC].Value = Constants.ProjectDetails.StudyInfo.CDISCPoCName;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.Stat_Awarded].Value = Constants.ProjectDetails.StudyInfo.StatisticsAwarded ? "Yes" : "No";
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.Stat_POC].Value = Constants.ProjectDetails.StudyInfo.StatisticsPoCName;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.ProgAssignedOn].Value = Constants.ProjectDetails.StudyInfo.StudyAssignedOnDate.ToString(Constants.DateFormat);

                        bool areActualDatesIncluded = Constants.ProjectDetails.Milestones.AreActualDatesIncluded;

                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLivePlanned].Value = Constants.ProjectDetails.Milestones.PlannedGoLive.ToString(Constants.DateFormat);
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLiveActual].Value = Constants.ProjectDetails.Milestones.IsActualGoLiveSpecified ? Constants.ProjectDetails.Milestones.ActualGoLive.ToString(Constants.DateFormat) : string.Empty;
                        
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVPlanned].Value = Constants.ProjectDetails.Milestones.PlannedFPFV.ToString(Constants.DateFormat);
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVActual].Value = Constants.ProjectDetails.Milestones.IsActualFPFVSpecified ? Constants.ProjectDetails.Milestones.ActualFPFV.ToString(Constants.DateFormat) : string.Empty;
                        
                        //worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVPlanned].Value = Constants.ProjectDetails.Milestones.PlannedLPFV.ToString(Constants.DateFormat);
                        //worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVActual].Value = Constants.ProjectDetails.Milestones.IsActualLPFVSpecified ? Constants.ProjectDetails.Milestones.ActualLPFV.ToString(Constants.DateFormat) : string.Empty;
                        
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVPlanned].Value = Constants.ProjectDetails.Milestones.PlannedLPLV.ToString(Constants.DateFormat);
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVActual].Value = Constants.ProjectDetails.Milestones.IsActualLPLVSpecified ? Constants.ProjectDetails.Milestones.ActualLPLV.ToString(Constants.DateFormat) : string.Empty;
                        
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLPlanned].Value = Constants.ProjectDetails.Milestones.PlannedDBL.ToString(Constants.DateFormat);
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLActual].Value = Constants.ProjectDetails.Milestones.IsActualDBLSpecified ? Constants.ProjectDetails.Milestones.ActualDBL.ToString(Constants.DateFormat) : string.Empty;
                        
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCPlanned].Value = Constants.ProjectDetails.Milestones.PlannedCDISCSDTM.ToString(Constants.DateFormat);
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCActual].Value = Constants.ProjectDetails.Milestones.IsActualCDISCSDTMSpecified ? Constants.ProjectDetails.Milestones.ActualCDISCSDTM.ToString(Constants.DateFormat) : string.Empty;
                        
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPPlanned].Value = Constants.ProjectDetails.Milestones.PlannedSAP.ToString(Constants.DateFormat);
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPActual].Value = Constants.ProjectDetails.Milestones.IsActualSAPSpecified ? Constants.ProjectDetails.Milestones.ActualSAP.ToString(Constants.DateFormat) : string.Empty;
                        
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRPlanned].Value = Constants.ProjectDetails.Milestones.PlannedBDR.ToString(Constants.DateFormat);
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRActual].Value = Constants.ProjectDetails.Milestones.IsActualBDRSpecified ? Constants.ProjectDetails.Milestones.ActualBDR.ToString(Constants.DateFormat) : string.Empty;
                        
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewPlanned].Value = Constants.ProjectDetails.Milestones.PlannedBDRReview.ToString(Constants.DateFormat);
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewActual].Value = Constants.ProjectDetails.Milestones.IsActualBDRReviewSpecified ? Constants.ProjectDetails.Milestones.ActualBDRReview.ToString(Constants.DateFormat) : string.Empty;
                        
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARPlanned].Value = Constants.ProjectDetails.Milestones.PlannedSAR.ToString(Constants.DateFormat);
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARActual].Value = Constants.ProjectDetails.Milestones.IsActualSARSpecified ? Constants.ProjectDetails.Milestones.ActualSAR.ToString(Constants.DateFormat) : string.Empty;

                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLivePlannedDays].Value = Constants.ProjectDetails.Milestones.PlannedGoLiveDays.ToString();
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVPlannedDays].Value = Constants.ProjectDetails.Milestones.PlannedFPFVDays.ToString();
                        //worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVPlannedDays].Value = Constants.ProjectDetails.Milestones.PlannedLPFVDays.ToString();
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVPlannedDays].Value = Constants.ProjectDetails.Milestones.PlannedLPLVDays.ToString();
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLPlannedDays].Value = Constants.ProjectDetails.Milestones.PlannedDBLDays.ToString();
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCPlannedDays].Value = Constants.ProjectDetails.Milestones.PlannedCDISCSDTMDays.ToString();
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPPlannedDays].Value = Constants.ProjectDetails.Milestones.PlannedSAPDays.ToString();
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRPlannedDays].Value = Constants.ProjectDetails.Milestones.PlannedBDRDays.ToString();
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewPlannedDays].Value = Constants.ProjectDetails.Milestones.PlannedBDRReviewDays.ToString();
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARPlannedDays].Value = Constants.ProjectDetails.Milestones.PlannedSARDays.ToString();

                        //if (Constants.ProjectDetails.Milestones.AreActualDatesIncluded)
                        //{
                        //    worksheet.Cells[rowIndex, Constants.WorkspaceFields.GoLiveActualDays].Value = Constants.ProjectDetails.Milestones.ActualGoLiveDays.ToString();
                        //    worksheet.Cells[rowIndex, Constants.WorkspaceFields.FPFVActualDays].Value = Constants.ProjectDetails.Milestones.ActualFPFVDays.ToString();
                        //    worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPFVActualDays].Value = Constants.ProjectDetails.Milestones.ActualLPFVDays.ToString();
                        //    worksheet.Cells[rowIndex, Constants.WorkspaceFields.LPLVActualDays].Value = Constants.ProjectDetails.Milestones.ActualLPLVDays.ToString();
                        //    worksheet.Cells[rowIndex, Constants.WorkspaceFields.DBLActualDays].Value = Constants.ProjectDetails.Milestones.ActualDBLDays.ToString();
                        //    worksheet.Cells[rowIndex, Constants.WorkspaceFields.CDISCActualDays].Value = Constants.ProjectDetails.Milestones.ActualCDISCSDTMDays.ToString();
                        //    worksheet.Cells[rowIndex, Constants.WorkspaceFields.SAPActualDays].Value = Constants.ProjectDetails.Milestones.ActualSAPDays.ToString();
                        //    worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRActualDays].Value = Constants.ProjectDetails.Milestones.ActualBDRDays.ToString();
                        //    worksheet.Cells[rowIndex, Constants.WorkspaceFields.BDRReviewActualDays].Value = Constants.ProjectDetails.Milestones.ActualBDRReviewDays.ToString();
                        //    worksheet.Cells[rowIndex, Constants.WorkspaceFields.SARActualDays].Value = Constants.ProjectDetails.Milestones.ActualSARDays.ToString();
                        //}

                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.ActualDatesIncluded].Value = Constants.ProjectDetails.Milestones.AreActualDatesIncluded ? "Yes" : "No";

                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.Client_Comments].Value = Constants.ProjectDetails.Comments.ClientComments;
                        worksheet.Cells[rowIndex, Constants.WorkspaceFields.Cytel_Comments].Value = Constants.ProjectDetails.Comments.CytelComments;

                        break;
                    }                    
                }

                ExcelWorkBook.Save();
                ExcelWorkBook.Close(true, System.Reflection.Missing.Value, System.Reflection.Missing.Value);

                //concat all tab data
                Constants.MainFormData = string.Concat(ProjIDDetailsData, Constants.Seperator);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                //return false;
            }
            finally
            {
                Excelapp.Quit();
                if (ExcelWorkBook != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelWorkBook); }
                if (Excelapp != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(Excelapp); }
            }
        }
        

        private void btnProjDashboard_Click(object sender, EventArgs e)
        {
            if (advancedDataGridView1 == null || advancedDataGridView1.Rows.Count == 0)
            {
                MessageBox.Show("There are no project records available in table. Please add one or more projects, and select one of those to proceed with this operation.", Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return;
            }

            //Get the selected projectIDs in Array
            string[] selectedProjectIDs = GetSelectedProjectNames();

            bool noProjectSelected = (selectedProjectIDs.Where(n => n != null).Count() == 0);

            if (noProjectSelected)
            {
                MessageBox.Show("There is no project record selected in table. Please select one project record to proceed with this operation.", Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return;
            }

            FrmWaitForm loading = new FrmWaitForm();
            loading.StartPosition = FormStartPosition.CenterScreen; ;
            loading.Show(this);

            string outputWorkbookPath = EngineInvoker.RunTemplate(selectedProjectIDs, false);

            loading.Close();

            if (string.IsNullOrEmpty(outputWorkbookPath) == false)
            {
                MessageBox.Show("Project dashboard plot generated successfully for the selected projects. It will be opened after closing this message window.", Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                Process.Start(outputWorkbookPath);
            }
        }

        private void btnProjPortfolio_Click(object sender, EventArgs e)
        {
            if (advancedDataGridView1 == null || advancedDataGridView1.Rows.Count == 0)
            {
                MessageBox.Show("There are no projects available in table. Please add one or more projects to proceed with tis operation. ", Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return;
            }
            //Get the selected projectIDs in Array
            string[] selectedProjectIDs = GetSelectedProjectNames();

            bool noProjectSelected = (selectedProjectIDs.Where(n => n != null).Count() == 0);

            if (noProjectSelected)
            {
                MessageBox.Show("There is no project record selected in table. Please select one project record to proceed with this operation.", Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return;
            }

            FrmWaitForm loading = new FrmWaitForm();
            loading.StartPosition = FormStartPosition.CenterScreen; ;
            loading.Show(this);

            //Get the selected projectIDs in Array
           // string[] selectedProjectIDs = GetSelectedProjectNames();
            string outputWorkbookPath = EngineInvoker.RunTemplate(selectedProjectIDs, true);

            loading.Close();

            if (string.IsNullOrEmpty(outputWorkbookPath) == false)
            {
                MessageBox.Show("Project portfolio plot generated successfully for all projects in table. It will be opened after closing this message window.", Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                Process.Start(outputWorkbookPath);
            }
        }

        /// <summary>
        /// Add the selected project IDs to the Array
        /// </summary>
        private string[] GetSelectedProjectNames()
        {
            int ittr = 0;
            int prjcolIndex = 1; // GetColumnIndexByName(gvProjectData, Constants.UCGridColumns.ProjectName.GetEnumDescription());
                      

            int selectedRowCnt = advancedDataGridView1.Rows.Count;
            string[] selectedProjectsID = new string[selectedRowCnt];

            //for (int i = 0; i < gvProjectData.Rows.Count; i++)
            foreach (DataGridViewRow row in advancedDataGridView1.Rows)
            {
                bool isSelected = Convert.ToBoolean(row.Cells[0].Value);
                if (isSelected)
                {
                    selectedProjectsID[ittr] = Convert.ToString(row.Cells[prjcolIndex].Value);
                    ittr++;
                }
            }

            return selectedProjectsID;
        }        

        private void advancedDataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void advancedDataGridView1_SortStringChanged(object sender, AdvancedDataGridView.SortEventArgs e)
        {

        }

        private void advancedDataGridView1_FilterStringChanged(object sender, AdvancedDataGridView.FilterEventArgs e)
        {

        }

        private void bindingSource_main_ListChanged(object sender, System.ComponentModel.ListChangedEventArgs e)
        {

        }

        #region unused methods
        
        bool flg_UpdateExcel;

        private void gvProjectData_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //if (e.RowIndex >= 0 )
            //{
            //    DataGridViewRow row   = gvProjectData.Rows[e.RowIndex];
            //    selectedPrjID = row.Cells["Project name"].Value.ToString();
            //    selected_GridID = row.Index;
            //}
        }

        /*
        void ReadStoredProjectData(string projectID)
        {
            int rowCount, colCount;

            Microsoft.Office.Interop.Excel.Application Excelapp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook ExcelWorkBook = Excelapp.Workbooks.Open(Constants.WorkSpaceFilePath);

            try
            {
                Excelapp.DisplayAlerts = false;
                object misValue = System.Reflection.Missing.Value;
                Excelapp.Visible = false;
                Excelapp.ActiveWindow.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMinimized;

                // Excelsheet in workbook  
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                worksheet = ExcelWorkBook.Worksheets[Constants.ShtProjData];

                //get last row of worksheet
                Microsoft.Office.Interop.Excel.Range usedRange1 = worksheet.UsedRange;
                rowCount = usedRange1.Rows.Count;
                colCount = usedRange1.Columns.Count;

                //Apply filter on the Project ID
                Microsoft.Office.Interop.Excel.Range sourceRange = worksheet.UsedRange;
                sourceRange.AutoFilter(Constants.ProjectIdCol, projectID);
                //get the filtred range
                Microsoft.Office.Interop.Excel.Range filteredRange = sourceRange.SpecialCells(Microsoft.Office.Interop.Excel.XlCellType.xlCellTypeVisible);

                Constants.MainFormData = string.Empty;
                string ProjIDDetailsData = string.Empty;

                foreach (Microsoft.Office.Interop.Excel.Range Rngrow in filteredRange.Rows)
                {
                    Microsoft.Office.Interop.Excel.Range filteredCell = (Microsoft.Office.Interop.Excel.Range)Rngrow.Cells[1, 1];
                    selectedPrjID_shtRow = Rngrow.Row;

                    if (selectedPrjID_shtRow < Constants.Header_Row)
                        continue;
                    string value = string.Empty;

                    MessageBox.Show("" + selectedPrjID_shtRow, Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (flg_UpdateExcel == false)
                    {
                        //column loop - strating from loop from project id col
                        for (int col = Constants.ProjectIdCol; col < colCount; col++)
                        {
                            //get the filtred row values into the string to upload on the form
                            value = string.Join(Constants.Seperator, value, Convert.ToString(filteredRange.Cells[selectedPrjID_shtRow, col].Value));
                            //use value variable to upload the selected project id details on the Form to edit further
                        }
                    }
                    else
                    {
                        //Once slected project id values are updated after ok button update the selected row in excel 
                        WriteGridDataIntoExcel(false, true);
                    }
                }
                //concat all tab data
                Constants.MainFormData = string.Concat(ProjIDDetailsData, Constants.Seperator);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                //return false;
            }
            finally
            {
                Excelapp.Quit();
                if (ExcelWorkBook != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelWorkBook); }
                if (Excelapp != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(Excelapp); }
            }
        }
        */

        /// <summary>
        /// function will return column index
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private int GetColumnIndexByName(DataGridView grid, string name)
        {
            foreach (DataGridViewColumn col in grid.Columns)
            {
                if (col.HeaderText.ToLower().Trim() == name.ToLower().Trim())
                {
                    return grid.Columns.IndexOf(col);
                }
            }
            return -1;
        }

        /// <summary>
        /// load existing excel data of workspace into datatable
        /// assign datatable source to gridvieeew control
        /// </summary>
        /// <returns></returns>
        public bool LoadExcelData()
        {
            bool returnValue = false;

            //get excel data to datatable
            var dt = Utility.UtilityFunction.GetDataFromExcel(Constants.WorkSpaceFilePath);
            //gvProjectData.DataSource = dt;

            return returnValue;
        }

        /// <summary>
        /// add form data into grid control
        /// </summary>
        /*public void FillGridRows()
        {
            string[] lines = Constants.MainFormData.Split(new[] { Constants.Seperator }, StringSplitOptions.None);

            int rowCount = 0; // gvProjectData.Rows.Count;
            rowCount = rowCount + 1;
            int cellCnt = 1;

            // print each value in the array.
            DataTable dataTable = new DataTable(); // (DataTable)gvProjectData.DataSource;

            DataRow drToAdd = dataTable.NewRow();

            // add each value into each cell of data row
            foreach (string value in lines)
            {
                drToAdd[cellCnt] = value;
                cellCnt++;
            }

            dataTable.Rows.Add(drToAdd);
            dataTable.AcceptChanges();

            // Set de text of the textbox to the value of the textbox of form 2
            //txtForm1.Text = ((TextBox)sender).Text;
        }
        */
        #endregion

        private void btnShowHideCols_Click(object sender, EventArgs e)
        {
            FrmShowHideCols frmShowHideCols = new FrmShowHideCols();
            if (frmShowHideCols.ShowDialog() == DialogResult.OK)
            {
                foreach (var dictElement in Constants.ColumnVisibilityStates)
                {
                    advancedDataGridView1.Columns[dictElement.Key].Visible = dictElement.Value;
                }
            }
        }

        # region Tooltip methods

        private void SetToolTip(Control control, string tooltipText = "Tooltip not available for this control")
        {
            if (control == null)
                return;

            toolTip1.SetToolTip(control, tooltipText);
        }       

        private string GetHelpText_ProjectTracker(string controlName)
        {
            if (string.IsNullOrWhiteSpace(controlName)) return string.Empty;

            string helpText = string.Empty;
            switch (controlName)
            {
                case Constants.ControlName_advancedDataGridView1:
                    helpText = Constants.HelpText_advancedDataGridView1;
                    break;

                case Constants.ControlName_btnAddProject:
                    helpText = Constants.HelpText_btnAddProject;
                    break;

                case Constants.ControlName_btnEditProject:
                    helpText = Constants.HelpText_btnEditProject;
                    break;

                case Constants.ControlName_btnProjDashboard:
                    helpText = Constants.HelpText_btnProjDashboard;
                    break;

                case Constants.ControlName_btnProjPortfolio:
                    helpText = Constants.HelpText_btnProjPortfolio;
                    break;

                case Constants.ControlName_btnOpen:
                    helpText = Constants.HelpText_btnOpen;
                    break;

                case Constants.ControlName_btnShowHideCols:
                    helpText = Constants.HelpText_btnShowHideCols;
                    break;

                default:
                    helpText = "";
                    break;
            }

            return helpText;
        }

        private void MouseHover_Event(object sender, EventArgs e)
        {
            SetToolTip((Control)sender, GetHelpText_ProjectTracker(((Control)sender).Name));
        }
        #endregion

        #region Commented: Earlier mouse hover events
        //private void btnAddProject_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_ProjectTracker(((Control)sender).Name));
        //}

        //private void btnEditProject_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_ProjectTracker(((Control)sender).Name));
        //}

        //private void btnProjDashboard_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_ProjectTracker(((Control)sender).Name));
        //}

        //private void btnProjPortfolio_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_ProjectTracker(((Control)sender).Name));
        //}

        //private void btnShowHideCols_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_ProjectTracker(((Control)sender).Name));
        //}

        //private void advancedDataGridView1_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_ProjectTracker(((Control)sender).Name));
        //}

        //private void linkLabel1_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_ProjectTracker(((Control)sender).Name));
        //}

        //private void btnOpen_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_ProjectTracker(((Control)sender).Name));
        //}

        #endregion

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (advancedDataGridView1 == null || advancedDataGridView1.Rows.Count == 0)
                return;

            bool selectAll = linkLabel1.Text.Equals("Select All Projects");

            foreach (DataGridViewRow gridRow in advancedDataGridView1.Rows)
            {
                gridRow.Cells[GridColumns.Select].Value = selectAll.ToString();
            }

            linkLabel1.Text = selectAll ? "Unselect All Projects" : "Select All Projects";
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                //set path of workspace directory
                InitialDirectory = DashboardVisualization.Models.Constants.OutputFolderPath,
                Title = "Select excel file",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "xlsx",
                Filter = "Excel file (*.xlsx)|*.xlsx",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true,
                Multiselect = false
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string GraphFile = openFileDialog1.FileName;
                Process.Start(GraphFile);
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            string importFileName = string.Empty;

            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                //set path of workspace directory
                InitialDirectory = DashboardVisualization.Models.Constants.WorkSpaceFolderPath,
                Title = "Browse files to import",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "xlsx",
                Filter = "Excel file (*.xlsx)|*.xlsx",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true,
                Multiselect = false
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Constants.ImportFilePath = openFileDialog1.FileName;

                if (string.IsNullOrWhiteSpace(Constants.ImportFilePath) == false)
                {
                    //check file extension
                    string fileExtn = Path.GetExtension(Constants.ImportFilePath);
                    if (fileExtn != ".xlsx" && fileExtn != ".xls")
                    {
                        MessageBox.Show("Please select valid existing workspace file.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    FrmWaitForm loading = new FrmWaitForm();
                    loading.StartPosition = FormStartPosition.CenterScreen; ;
                    loading.Show(this);

                    // read all projects details from workspace in a collection
                    List<ProjectDetails> allProjectDetails = ReadAllProjectsFromWorkspace(true);

                    // Populate grid with the projects details from collection
                    AddAllProjectRecordsInGrid(allProjectDetails);

                    if (allProjectDetails != null && allProjectDetails.Count > 0)
                        AddImportedProjectRecordInWorkspace(allProjectDetails);                   
                    
                    loading.Close();
                    MessageBox.Show("Importing workspace file done.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}
