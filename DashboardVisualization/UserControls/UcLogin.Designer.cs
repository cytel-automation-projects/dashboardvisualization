﻿namespace DashboardVisualization.UserControls
{
    partial class UcLogin
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlCreatenewWrkspc = new System.Windows.Forms.Panel();
            this.grpCreateNewWrk = new System.Windows.Forms.GroupBox();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.lblClient = new System.Windows.Forms.Label();
            this.btnBrowseExisting = new System.Windows.Forms.Button();
            this.txtExistingPath = new System.Windows.Forms.TextBox();
            this.rdbLoadExisting = new System.Windows.Forms.RadioButton();
            this.rdbCreateNewWrkspc = new System.Windows.Forms.RadioButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pnlCreatenewWrkspc.SuspendLayout();
            this.grpCreateNewWrk.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlCreatenewWrkspc
            // 
            this.pnlCreatenewWrkspc.Controls.Add(this.grpCreateNewWrk);
            this.pnlCreatenewWrkspc.Controls.Add(this.btnBrowseExisting);
            this.pnlCreatenewWrkspc.Controls.Add(this.txtExistingPath);
            this.pnlCreatenewWrkspc.Controls.Add(this.rdbLoadExisting);
            this.pnlCreatenewWrkspc.Controls.Add(this.rdbCreateNewWrkspc);
            this.pnlCreatenewWrkspc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCreatenewWrkspc.Location = new System.Drawing.Point(0, 0);
            this.pnlCreatenewWrkspc.Name = "pnlCreatenewWrkspc";
            this.pnlCreatenewWrkspc.Size = new System.Drawing.Size(460, 242);
            this.pnlCreatenewWrkspc.TabIndex = 1;
            // 
            // grpCreateNewWrk
            // 
            this.grpCreateNewWrk.Controls.Add(this.txtClient);
            this.grpCreateNewWrk.Controls.Add(this.lblClient);
            this.grpCreateNewWrk.Location = new System.Drawing.Point(22, 150);
            this.grpCreateNewWrk.Name = "grpCreateNewWrk";
            this.grpCreateNewWrk.Size = new System.Drawing.Size(422, 78);
            this.grpCreateNewWrk.TabIndex = 4;
            this.grpCreateNewWrk.TabStop = false;
            // 
            // txtClient
            // 
            this.txtClient.Enabled = false;
            this.txtClient.Location = new System.Drawing.Point(11, 38);
            this.txtClient.Name = "txtClient";
            this.txtClient.Size = new System.Drawing.Size(405, 22);
            this.txtClient.TabIndex = 7;
            this.txtClient.MouseHover += new System.EventHandler(this.MouseHover_Login);
            // 
            // lblClient
            // 
            this.lblClient.AutoSize = true;
            this.lblClient.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClient.Location = new System.Drawing.Point(11, 16);
            this.lblClient.Name = "lblClient";
            this.lblClient.Size = new System.Drawing.Size(200, 14);
            this.lblClient.TabIndex = 6;
            this.lblClient.Text = "Enter &Workspace/Client Name:";
            // 
            // btnBrowseExisting
            // 
            this.btnBrowseExisting.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseExisting.Location = new System.Drawing.Point(360, 82);
            this.btnBrowseExisting.Name = "btnBrowseExisting";
            this.btnBrowseExisting.Size = new System.Drawing.Size(84, 27);
            this.btnBrowseExisting.TabIndex = 2;
            this.btnBrowseExisting.Text = "&Browse...";
            this.btnBrowseExisting.UseVisualStyleBackColor = true;
            this.btnBrowseExisting.Click += new System.EventHandler(this.btnBrowseExisting_Click);
            this.btnBrowseExisting.MouseHover += new System.EventHandler(this.MouseHover_Login);
            // 
            // txtExistingPath
            // 
            this.txtExistingPath.Location = new System.Drawing.Point(34, 37);
            this.txtExistingPath.Multiline = true;
            this.txtExistingPath.Name = "txtExistingPath";
            this.txtExistingPath.ReadOnly = true;
            this.txtExistingPath.Size = new System.Drawing.Size(410, 36);
            this.txtExistingPath.TabIndex = 1;
            this.txtExistingPath.MouseHover += new System.EventHandler(this.MouseHover_Login);
            // 
            // rdbLoadExisting
            // 
            this.rdbLoadExisting.AutoSize = true;
            this.rdbLoadExisting.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbLoadExisting.Location = new System.Drawing.Point(11, 12);
            this.rdbLoadExisting.Name = "rdbLoadExisting";
            this.rdbLoadExisting.Size = new System.Drawing.Size(216, 18);
            this.rdbLoadExisting.TabIndex = 0;
            this.rdbLoadExisting.TabStop = true;
            this.rdbLoadExisting.Text = "&Load an Existing Workspace:";
            this.rdbLoadExisting.UseVisualStyleBackColor = true;
            this.rdbLoadExisting.CheckedChanged += new System.EventHandler(this.rdbLoadExisting_CheckedChanged);
            this.rdbLoadExisting.MouseHover += new System.EventHandler(this.MouseHover_Login);
            // 
            // rdbCreateNewWrkspc
            // 
            this.rdbCreateNewWrkspc.AutoSize = true;
            this.rdbCreateNewWrkspc.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbCreateNewWrkspc.Location = new System.Drawing.Point(11, 123);
            this.rdbCreateNewWrkspc.Name = "rdbCreateNewWrkspc";
            this.rdbCreateNewWrkspc.Size = new System.Drawing.Size(198, 18);
            this.rdbCreateNewWrkspc.TabIndex = 3;
            this.rdbCreateNewWrkspc.TabStop = true;
            this.rdbCreateNewWrkspc.Text = "&Create a New Workspace:\t";
            this.rdbCreateNewWrkspc.UseVisualStyleBackColor = true;
            this.rdbCreateNewWrkspc.CheckedChanged += new System.EventHandler(this.rdbCreateNewWrkspc_CheckedChanged);
            this.rdbCreateNewWrkspc.MouseHover += new System.EventHandler(this.MouseHover_Login);
            // 
            // UcLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlCreatenewWrkspc);
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "UcLogin";
            this.Size = new System.Drawing.Size(460, 242);
            this.pnlCreatenewWrkspc.ResumeLayout(false);
            this.pnlCreatenewWrkspc.PerformLayout();
            this.grpCreateNewWrk.ResumeLayout(false);
            this.grpCreateNewWrk.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlCreatenewWrkspc;
        private System.Windows.Forms.GroupBox grpCreateNewWrk;
        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.Label lblClient;
        private System.Windows.Forms.Button btnBrowseExisting;
        private System.Windows.Forms.TextBox txtExistingPath;
        private System.Windows.Forms.RadioButton rdbLoadExisting;
        private System.Windows.Forms.RadioButton rdbCreateNewWrkspc;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
