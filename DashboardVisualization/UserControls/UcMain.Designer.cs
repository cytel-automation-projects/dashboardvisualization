﻿namespace DashboardVisualization.UserControls
{
    partial class UcMain
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tbMain = new System.Windows.Forms.TabControl();
            this.tbProjDetails = new System.Windows.Forms.TabPage();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.txtOtherCategory = new System.Windows.Forms.TextBox();
            this.lblOtherCategory = new System.Windows.Forms.Label();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtProjectName = new System.Windows.Forms.TextBox();
            this.lblCategory = new System.Windows.Forms.Label();
            this.lblProjectName = new System.Windows.Forms.Label();
            this.tbStudyInfo = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtOtherCountry = new System.Windows.Forms.TextBox();
            this.lblOtherCountry = new System.Windows.Forms.Label();
            this.txtOtherCRO = new System.Windows.Forms.TextBox();
            this.lblOtherCRO = new System.Windows.Forms.Label();
            this.cmbCountry = new System.Windows.Forms.ComboBox();
            this.cmbCRO = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCSSName = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtGCLName = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCSLName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDataManagement = new System.Windows.Forms.TextBox();
            this.lblPOC = new System.Windows.Forms.Label();
            this.lblDataMgmt = new System.Windows.Forms.Label();
            this.lblAwarded = new System.Windows.Forms.Label();
            this.lblCDISC = new System.Windows.Forms.Label();
            this.lblStatistics = new System.Windows.Forms.Label();
            this.txtStatisics = new System.Windows.Forms.TextBox();
            this.chkDataManagement = new System.Windows.Forms.CheckBox();
            this.txtCDISC = new System.Windows.Forms.TextBox();
            this.chkCDISC = new System.Windows.Forms.CheckBox();
            this.chkStatisics = new System.Windows.Forms.CheckBox();
            this.dtAssignedOn = new System.Windows.Forms.DateTimePicker();
            this.lblAssignedOn = new System.Windows.Forms.Label();
            this.tbMilestones = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtActualBDR = new System.Windows.Forms.TextBox();
            this.dtActualBDR = new System.Windows.Forms.DateTimePicker();
            this.txtActualSAR = new System.Windows.Forms.TextBox();
            this.txtActualBDRReview = new System.Windows.Forms.TextBox();
            this.txtActualSAP = new System.Windows.Forms.TextBox();
            this.txtActualSDTM = new System.Windows.Forms.TextBox();
            this.txtActualDBL = new System.Windows.Forms.TextBox();
            this.txtActualLPLV = new System.Windows.Forms.TextBox();
            this.txtActualFPFV = new System.Windows.Forms.TextBox();
            this.txtActualGoLive = new System.Windows.Forms.TextBox();
            this.dtActualSAR = new System.Windows.Forms.DateTimePicker();
            this.dtActualBDRReview = new System.Windows.Forms.DateTimePicker();
            this.dtActualSAP = new System.Windows.Forms.DateTimePicker();
            this.dtActualSDTM = new System.Windows.Forms.DateTimePicker();
            this.dtActualDBL = new System.Windows.Forms.DateTimePicker();
            this.dtActualLPLV = new System.Windows.Forms.DateTimePicker();
            this.dtActualFPFV = new System.Windows.Forms.DateTimePicker();
            this.dtActualGoLive = new System.Windows.Forms.DateTimePicker();
            this.lblActualDays = new System.Windows.Forms.Label();
            this.lblActualDates = new System.Windows.Forms.Label();
            this.chkAddActuals = new System.Windows.Forms.CheckBox();
            this.cmbStudyStatus = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkSameBDR = new System.Windows.Forms.CheckBox();
            this.txtPlanedBDR = new System.Windows.Forms.TextBox();
            this.dtPlanedBDR = new System.Windows.Forms.DateTimePicker();
            this.lblPlannedBDR = new System.Windows.Forms.Label();
            this.chkSameSAR = new System.Windows.Forms.CheckBox();
            this.chkSameBDRReview = new System.Windows.Forms.CheckBox();
            this.chkSameSAP = new System.Windows.Forms.CheckBox();
            this.chkSameSDTM = new System.Windows.Forms.CheckBox();
            this.chkSameDBL = new System.Windows.Forms.CheckBox();
            this.chkSameLPLV = new System.Windows.Forms.CheckBox();
            this.chkSameFPFV = new System.Windows.Forms.CheckBox();
            this.chkSameGoLive = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPlanedSAR = new System.Windows.Forms.TextBox();
            this.txtPlanedBDRReview = new System.Windows.Forms.TextBox();
            this.txtPlanedSAP = new System.Windows.Forms.TextBox();
            this.txtPlanedSDTM = new System.Windows.Forms.TextBox();
            this.txtPlanedDBL = new System.Windows.Forms.TextBox();
            this.txtPlanedLPLV = new System.Windows.Forms.TextBox();
            this.txtPlanedFPFV = new System.Windows.Forms.TextBox();
            this.txtPlanedGoLive = new System.Windows.Forms.TextBox();
            this.lblPlannedDays = new System.Windows.Forms.Label();
            this.dtPlanedSAR = new System.Windows.Forms.DateTimePicker();
            this.dtPlanedBDRReview = new System.Windows.Forms.DateTimePicker();
            this.dtPlanedSAP = new System.Windows.Forms.DateTimePicker();
            this.dtPlanedSDTM = new System.Windows.Forms.DateTimePicker();
            this.dtPlanedDBL = new System.Windows.Forms.DateTimePicker();
            this.dtPlanedLPLV = new System.Windows.Forms.DateTimePicker();
            this.dtPlanedFPFV = new System.Windows.Forms.DateTimePicker();
            this.dtPlanedGoLive = new System.Windows.Forms.DateTimePicker();
            this.lblPlannedSAR = new System.Windows.Forms.Label();
            this.lblPlannedDBL = new System.Windows.Forms.Label();
            this.lblPlannedBDRReview = new System.Windows.Forms.Label();
            this.lblPlannedSAP = new System.Windows.Forms.Label();
            this.lblPlannedCDISC = new System.Windows.Forms.Label();
            this.lblPlannedGoLive = new System.Windows.Forms.Label();
            this.lblPlannedLPLV = new System.Windows.Forms.Label();
            this.lblPlannedFPFV = new System.Windows.Forms.Label();
            this.lblPlannedDates = new System.Windows.Forms.Label();
            this.tbComment = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtCytelComment = new System.Windows.Forms.TextBox();
            this.txtClientComment = new System.Windows.Forms.TextBox();
            this.lblCytelComment = new System.Windows.Forms.Label();
            this.lblClientComment = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tbMain.SuspendLayout();
            this.tbProjDetails.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.tbStudyInfo.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tbMilestones.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tbComment.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbMain
            // 
            this.tbMain.Controls.Add(this.tbProjDetails);
            this.tbMain.Controls.Add(this.tbStudyInfo);
            this.tbMain.Controls.Add(this.tbMilestones);
            this.tbMain.Controls.Add(this.tbComment);
            this.tbMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMain.ItemSize = new System.Drawing.Size(103, 30);
            this.tbMain.Location = new System.Drawing.Point(0, 0);
            this.tbMain.Name = "tbMain";
            this.tbMain.SelectedIndex = 0;
            this.tbMain.Size = new System.Drawing.Size(662, 524);
            this.tbMain.TabIndex = 1;
            // 
            // tbProjDetails
            // 
            this.tbProjDetails.BackColor = System.Drawing.SystemColors.Control;
            this.tbProjDetails.Controls.Add(this.mainPanel);
            this.tbProjDetails.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbProjDetails.Location = new System.Drawing.Point(4, 34);
            this.tbProjDetails.Name = "tbProjDetails";
            this.tbProjDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tbProjDetails.Size = new System.Drawing.Size(654, 486);
            this.tbProjDetails.TabIndex = 0;
            this.tbProjDetails.Text = "Project Details";
            // 
            // mainPanel
            // 
            this.mainPanel.BackColor = System.Drawing.SystemColors.Control;
            this.mainPanel.Controls.Add(this.txtOtherCategory);
            this.mainPanel.Controls.Add(this.lblOtherCategory);
            this.mainPanel.Controls.Add(this.cmbCategory);
            this.mainPanel.Controls.Add(this.txtDescription);
            this.mainPanel.Controls.Add(this.lblDescription);
            this.mainPanel.Controls.Add(this.txtProjectName);
            this.mainPanel.Controls.Add(this.lblCategory);
            this.mainPanel.Controls.Add(this.lblProjectName);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(3, 3);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(648, 480);
            this.mainPanel.TabIndex = 0;
            // 
            // txtOtherCategory
            // 
            this.txtOtherCategory.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOtherCategory.Location = new System.Drawing.Point(216, 118);
            this.txtOtherCategory.Name = "txtOtherCategory";
            this.txtOtherCategory.Size = new System.Drawing.Size(402, 22);
            this.txtOtherCategory.TabIndex = 5;
            this.txtOtherCategory.Visible = false;
            // 
            // lblOtherCategory
            // 
            this.lblOtherCategory.AutoSize = true;
            this.lblOtherCategory.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOtherCategory.Location = new System.Drawing.Point(15, 117);
            this.lblOtherCategory.Name = "lblOtherCategory";
            this.lblOtherCategory.Size = new System.Drawing.Size(159, 14);
            this.lblOtherCategory.TabIndex = 4;
            this.lblOtherCategory.Text = "Specify \'Other\' Category";
            this.lblOtherCategory.Visible = false;
            // 
            // cmbCategory
            // 
            this.cmbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Items.AddRange(new object[] {
            "DEO",
            "SKIN",
            "HYG",
            "HAI",
            "ORL",
            "Other"});
            this.cmbCategory.Location = new System.Drawing.Point(216, 64);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(402, 24);
            this.cmbCategory.TabIndex = 3;
            this.cmbCategory.SelectedIndexChanged += new System.EventHandler(this.cmbCategory_SelectedIndexChanged);
            // 
            // txtDescription
            // 
            this.txtDescription.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescription.Location = new System.Drawing.Point(216, 170);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtDescription.Size = new System.Drawing.Size(402, 74);
            this.txtDescription.TabIndex = 7;
            this.txtDescription.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Location = new System.Drawing.Point(15, 199);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(77, 14);
            this.lblDescription.TabIndex = 6;
            this.lblDescription.Text = "&Description";
            // 
            // txtProjectName
            // 
            this.txtProjectName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProjectName.Location = new System.Drawing.Point(216, 13);
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.Size = new System.Drawing.Size(402, 22);
            this.txtProjectName.TabIndex = 1;
            this.txtProjectName.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategory.Location = new System.Drawing.Point(15, 68);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(65, 14);
            this.lblCategory.TabIndex = 2;
            this.lblCategory.Text = "Cate&gory";
            // 
            // lblProjectName
            // 
            this.lblProjectName.AutoSize = true;
            this.lblProjectName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProjectName.Location = new System.Drawing.Point(15, 16);
            this.lblProjectName.Name = "lblProjectName";
            this.lblProjectName.Size = new System.Drawing.Size(91, 14);
            this.lblProjectName.TabIndex = 0;
            this.lblProjectName.Text = "&Project Name";
            // 
            // tbStudyInfo
            // 
            this.tbStudyInfo.BackColor = System.Drawing.SystemColors.Control;
            this.tbStudyInfo.Controls.Add(this.panel1);
            this.tbStudyInfo.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbStudyInfo.Location = new System.Drawing.Point(4, 34);
            this.tbStudyInfo.Name = "tbStudyInfo";
            this.tbStudyInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tbStudyInfo.Size = new System.Drawing.Size(654, 486);
            this.tbStudyInfo.TabIndex = 1;
            this.tbStudyInfo.Text = "Study / Personnel Info";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtOtherCountry);
            this.panel1.Controls.Add(this.lblOtherCountry);
            this.panel1.Controls.Add(this.txtOtherCRO);
            this.panel1.Controls.Add(this.lblOtherCRO);
            this.panel1.Controls.Add(this.cmbCountry);
            this.panel1.Controls.Add(this.cmbCRO);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtCSSName);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.txtGCLName);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.txtCSLName);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.dtAssignedOn);
            this.panel1.Controls.Add(this.lblAssignedOn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(648, 480);
            this.panel1.TabIndex = 0;
            // 
            // txtOtherCountry
            // 
            this.txtOtherCountry.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOtherCountry.Location = new System.Drawing.Point(181, 282);
            this.txtOtherCountry.Name = "txtOtherCountry";
            this.txtOtherCountry.Size = new System.Drawing.Size(222, 22);
            this.txtOtherCountry.TabIndex = 16;
            this.txtOtherCountry.Visible = false;
            // 
            // lblOtherCountry
            // 
            this.lblOtherCountry.AutoSize = true;
            this.lblOtherCountry.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOtherCountry.Location = new System.Drawing.Point(15, 286);
            this.lblOtherCountry.Name = "lblOtherCountry";
            this.lblOtherCountry.Size = new System.Drawing.Size(151, 14);
            this.lblOtherCountry.TabIndex = 15;
            this.lblOtherCountry.Text = "Specify \'Other\' Country";
            this.lblOtherCountry.Visible = false;
            // 
            // txtOtherCRO
            // 
            this.txtOtherCRO.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOtherCRO.Location = new System.Drawing.Point(181, 204);
            this.txtOtherCRO.Name = "txtOtherCRO";
            this.txtOtherCRO.Size = new System.Drawing.Size(222, 22);
            this.txtOtherCRO.TabIndex = 14;
            this.txtOtherCRO.Visible = false;
            // 
            // lblOtherCRO
            // 
            this.lblOtherCRO.AutoSize = true;
            this.lblOtherCRO.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOtherCRO.Location = new System.Drawing.Point(15, 208);
            this.lblOtherCRO.Name = "lblOtherCRO";
            this.lblOtherCRO.Size = new System.Drawing.Size(128, 14);
            this.lblOtherCRO.TabIndex = 13;
            this.lblOtherCRO.Text = "Specify \'Other\' CRO";
            this.lblOtherCRO.Visible = false;
            // 
            // cmbCountry
            // 
            this.cmbCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCountry.FormattingEnabled = true;
            this.cmbCountry.Items.AddRange(new object[] {
            "Canada",
            "China",
            "Germany",
            "India",
            "Indonesia",
            "Pakistan",
            "Russia",
            "Scotland",
            "Thailand",
            "UK",
            "USA",
            "Other",
            "NA"});
            this.cmbCountry.Location = new System.Drawing.Point(181, 242);
            this.cmbCountry.Name = "cmbCountry";
            this.cmbCountry.Size = new System.Drawing.Size(325, 24);
            this.cmbCountry.TabIndex = 11;
            this.cmbCountry.SelectedIndexChanged += new System.EventHandler(this.cmbCountry_SelectedIndexChanged);
            // 
            // cmbCRO
            // 
            this.cmbCRO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCRO.FormattingEnabled = true;
            this.cmbCRO.Items.AddRange(new object[] {
            "Alba Science",
            "Alpha Clinical Development Limited",
            "BE Labs",
            "Bioscience Laboratories",
            "BSL",
            "BSLI",
            "Chengdu Huaxi Stomatology Research Institute",
            "China Medical University (School of Stomatology)",
            "CHSRI",
            "CIDP",
            "Cliantha Research",
            "CRG",
            "Eurofins",
            "Insect Research and Development Ltd",
            "Intertek",
            "KGL",
            "Lambda",
            "Lotus Labs",
            "Mascot",
            "MSCR",
            "proDERM",
            "RCTS",
            "School of Stomatology",
            "SGS",
            "Shanghai Skin Disease Hospital",
            "SIRO Clinpharm",
            "SPRIM",
            "Shanghai Skin Disease Hospital",
            "Stephens",
            "Therapeutic Drug Monitoring Laboratory",
            "TKL Research",
            "Sichuan University (West China College of Stomatology) ",
            "Xian Jiaotong University",
            "Other",
            "NA"});
            this.cmbCRO.Location = new System.Drawing.Point(181, 164);
            this.cmbCRO.Name = "cmbCRO";
            this.cmbCRO.Size = new System.Drawing.Size(325, 24);
            this.cmbCRO.TabIndex = 9;
            this.cmbCRO.SelectedIndexChanged += new System.EventHandler(this.cmbCRO_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 247);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 14);
            this.label3.TabIndex = 10;
            this.label3.Text = "Countr&y";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 14);
            this.label4.TabIndex = 8;
            this.label4.Text = "C&RO";
            // 
            // txtCSSName
            // 
            this.txtCSSName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCSSName.Location = new System.Drawing.Point(181, 126);
            this.txtCSSName.Name = "txtCSSName";
            this.txtCSSName.Size = new System.Drawing.Size(222, 22);
            this.txtCSSName.TabIndex = 7;
            this.txtCSSName.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(15, 130);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 14);
            this.label17.TabIndex = 6;
            this.label17.Text = "CSS &Name";
            // 
            // txtGCLName
            // 
            this.txtGCLName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGCLName.Location = new System.Drawing.Point(181, 88);
            this.txtGCLName.Name = "txtGCLName";
            this.txtGCLName.Size = new System.Drawing.Size(222, 22);
            this.txtGCLName.TabIndex = 5;
            this.txtGCLName.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(15, 92);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(72, 14);
            this.label16.TabIndex = 4;
            this.label16.Text = "&GCL Name";
            // 
            // txtCSLName
            // 
            this.txtCSLName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCSLName.Location = new System.Drawing.Point(181, 50);
            this.txtCSLName.Name = "txtCSLName";
            this.txtCSLName.Size = new System.Drawing.Size(222, 22);
            this.txtCSLName.TabIndex = 3;
            this.txtCSLName.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 54);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 14);
            this.label12.TabIndex = 2;
            this.label12.Text = "CS&L Name";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDataManagement);
            this.groupBox1.Controls.Add(this.lblPOC);
            this.groupBox1.Controls.Add(this.lblDataMgmt);
            this.groupBox1.Controls.Add(this.lblAwarded);
            this.groupBox1.Controls.Add(this.lblCDISC);
            this.groupBox1.Controls.Add(this.lblStatistics);
            this.groupBox1.Controls.Add(this.txtStatisics);
            this.groupBox1.Controls.Add(this.chkDataManagement);
            this.groupBox1.Controls.Add(this.txtCDISC);
            this.groupBox1.Controls.Add(this.chkCDISC);
            this.groupBox1.Controls.Add(this.chkStatisics);
            this.groupBox1.Location = new System.Drawing.Point(18, 307);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(546, 165);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // txtDataManagement
            // 
            this.txtDataManagement.Enabled = false;
            this.txtDataManagement.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDataManagement.Location = new System.Drawing.Point(306, 41);
            this.txtDataManagement.Name = "txtDataManagement";
            this.txtDataManagement.Size = new System.Drawing.Size(222, 22);
            this.txtDataManagement.TabIndex = 4;
            this.txtDataManagement.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // lblPOC
            // 
            this.lblPOC.AutoSize = true;
            this.lblPOC.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPOC.Location = new System.Drawing.Point(399, 16);
            this.lblPOC.Name = "lblPOC";
            this.lblPOC.Size = new System.Drawing.Size(36, 14);
            this.lblPOC.TabIndex = 19;
            this.lblPOC.Text = "POC";
            // 
            // lblDataMgmt
            // 
            this.lblDataMgmt.AutoSize = true;
            this.lblDataMgmt.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataMgmt.Location = new System.Drawing.Point(10, 45);
            this.lblDataMgmt.Name = "lblDataMgmt";
            this.lblDataMgmt.Size = new System.Drawing.Size(123, 14);
            this.lblDataMgmt.TabIndex = 2;
            this.lblDataMgmt.Text = "&Data Management";
            // 
            // lblAwarded
            // 
            this.lblAwarded.AutoSize = true;
            this.lblAwarded.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAwarded.Location = new System.Drawing.Point(189, 16);
            this.lblAwarded.Name = "lblAwarded";
            this.lblAwarded.Size = new System.Drawing.Size(66, 14);
            this.lblAwarded.TabIndex = 18;
            this.lblAwarded.Text = "Awarded";
            // 
            // lblCDISC
            // 
            this.lblCDISC.AutoSize = true;
            this.lblCDISC.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCDISC.Location = new System.Drawing.Point(10, 94);
            this.lblCDISC.Name = "lblCDISC";
            this.lblCDISC.Size = new System.Drawing.Size(47, 14);
            this.lblCDISC.TabIndex = 5;
            this.lblCDISC.Text = "&CDISC";
            // 
            // lblStatistics
            // 
            this.lblStatistics.AutoSize = true;
            this.lblStatistics.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatistics.Location = new System.Drawing.Point(10, 143);
            this.lblStatistics.Name = "lblStatistics";
            this.lblStatistics.Size = new System.Drawing.Size(31, 14);
            this.lblStatistics.TabIndex = 8;
            this.lblStatistics.Text = "&BSP";
            // 
            // txtStatisics
            // 
            this.txtStatisics.Enabled = false;
            this.txtStatisics.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatisics.Location = new System.Drawing.Point(306, 139);
            this.txtStatisics.Name = "txtStatisics";
            this.txtStatisics.Size = new System.Drawing.Size(222, 22);
            this.txtStatisics.TabIndex = 10;
            this.txtStatisics.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // chkDataManagement
            // 
            this.chkDataManagement.AutoSize = true;
            this.chkDataManagement.Location = new System.Drawing.Point(208, 45);
            this.chkDataManagement.Name = "chkDataManagement";
            this.chkDataManagement.Size = new System.Drawing.Size(15, 14);
            this.chkDataManagement.TabIndex = 3;
            this.chkDataManagement.UseVisualStyleBackColor = true;
            this.chkDataManagement.CheckedChanged += new System.EventHandler(this.chkDataManagement_CheckedChanged);
            this.chkDataManagement.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // txtCDISC
            // 
            this.txtCDISC.Enabled = false;
            this.txtCDISC.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCDISC.Location = new System.Drawing.Point(306, 90);
            this.txtCDISC.Name = "txtCDISC";
            this.txtCDISC.Size = new System.Drawing.Size(222, 22);
            this.txtCDISC.TabIndex = 7;
            this.txtCDISC.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // chkCDISC
            // 
            this.chkCDISC.AutoSize = true;
            this.chkCDISC.Location = new System.Drawing.Point(208, 94);
            this.chkCDISC.Name = "chkCDISC";
            this.chkCDISC.Size = new System.Drawing.Size(15, 14);
            this.chkCDISC.TabIndex = 6;
            this.chkCDISC.UseVisualStyleBackColor = true;
            this.chkCDISC.CheckedChanged += new System.EventHandler(this.chkCDISC_CheckedChanged);
            this.chkCDISC.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // chkStatisics
            // 
            this.chkStatisics.AutoSize = true;
            this.chkStatisics.Location = new System.Drawing.Point(208, 143);
            this.chkStatisics.Name = "chkStatisics";
            this.chkStatisics.Size = new System.Drawing.Size(15, 14);
            this.chkStatisics.TabIndex = 9;
            this.chkStatisics.UseVisualStyleBackColor = true;
            this.chkStatisics.CheckedChanged += new System.EventHandler(this.chkStatisics_CheckedChanged);
            this.chkStatisics.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // dtAssignedOn
            // 
            this.dtAssignedOn.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtAssignedOn.CustomFormat = "dd-MM-yyyy";
            this.dtAssignedOn.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtAssignedOn.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtAssignedOn.Location = new System.Drawing.Point(181, 12);
            this.dtAssignedOn.Name = "dtAssignedOn";
            this.dtAssignedOn.Size = new System.Drawing.Size(129, 22);
            this.dtAssignedOn.TabIndex = 1;
            this.dtAssignedOn.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtAssignedOn.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // lblAssignedOn
            // 
            this.lblAssignedOn.AutoSize = true;
            this.lblAssignedOn.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAssignedOn.Location = new System.Drawing.Point(15, 16);
            this.lblAssignedOn.Name = "lblAssignedOn";
            this.lblAssignedOn.Size = new System.Drawing.Size(86, 14);
            this.lblAssignedOn.TabIndex = 0;
            this.lblAssignedOn.Text = "&Assigned On";
            // 
            // tbMilestones
            // 
            this.tbMilestones.BackColor = System.Drawing.SystemColors.Control;
            this.tbMilestones.Controls.Add(this.panel2);
            this.tbMilestones.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMilestones.Location = new System.Drawing.Point(4, 34);
            this.tbMilestones.Name = "tbMilestones";
            this.tbMilestones.Size = new System.Drawing.Size(654, 486);
            this.tbMilestones.TabIndex = 2;
            this.tbMilestones.Text = "Milestones";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtActualBDR);
            this.panel2.Controls.Add(this.dtActualBDR);
            this.panel2.Controls.Add(this.txtActualSAR);
            this.panel2.Controls.Add(this.txtActualBDRReview);
            this.panel2.Controls.Add(this.txtActualSAP);
            this.panel2.Controls.Add(this.txtActualSDTM);
            this.panel2.Controls.Add(this.txtActualDBL);
            this.panel2.Controls.Add(this.txtActualLPLV);
            this.panel2.Controls.Add(this.txtActualFPFV);
            this.panel2.Controls.Add(this.txtActualGoLive);
            this.panel2.Controls.Add(this.dtActualSAR);
            this.panel2.Controls.Add(this.dtActualBDRReview);
            this.panel2.Controls.Add(this.dtActualSAP);
            this.panel2.Controls.Add(this.dtActualSDTM);
            this.panel2.Controls.Add(this.dtActualDBL);
            this.panel2.Controls.Add(this.dtActualLPLV);
            this.panel2.Controls.Add(this.dtActualFPFV);
            this.panel2.Controls.Add(this.dtActualGoLive);
            this.panel2.Controls.Add(this.lblActualDays);
            this.panel2.Controls.Add(this.lblActualDates);
            this.panel2.Controls.Add(this.chkAddActuals);
            this.panel2.Controls.Add(this.cmbStudyStatus);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.chkSameBDR);
            this.panel2.Controls.Add(this.txtPlanedBDR);
            this.panel2.Controls.Add(this.dtPlanedBDR);
            this.panel2.Controls.Add(this.lblPlannedBDR);
            this.panel2.Controls.Add(this.chkSameSAR);
            this.panel2.Controls.Add(this.chkSameBDRReview);
            this.panel2.Controls.Add(this.chkSameSAP);
            this.panel2.Controls.Add(this.chkSameSDTM);
            this.panel2.Controls.Add(this.chkSameDBL);
            this.panel2.Controls.Add(this.chkSameLPLV);
            this.panel2.Controls.Add(this.chkSameFPFV);
            this.panel2.Controls.Add(this.chkSameGoLive);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtPlanedSAR);
            this.panel2.Controls.Add(this.txtPlanedBDRReview);
            this.panel2.Controls.Add(this.txtPlanedSAP);
            this.panel2.Controls.Add(this.txtPlanedSDTM);
            this.panel2.Controls.Add(this.txtPlanedDBL);
            this.panel2.Controls.Add(this.txtPlanedLPLV);
            this.panel2.Controls.Add(this.txtPlanedFPFV);
            this.panel2.Controls.Add(this.txtPlanedGoLive);
            this.panel2.Controls.Add(this.lblPlannedDays);
            this.panel2.Controls.Add(this.dtPlanedSAR);
            this.panel2.Controls.Add(this.dtPlanedBDRReview);
            this.panel2.Controls.Add(this.dtPlanedSAP);
            this.panel2.Controls.Add(this.dtPlanedSDTM);
            this.panel2.Controls.Add(this.dtPlanedDBL);
            this.panel2.Controls.Add(this.dtPlanedLPLV);
            this.panel2.Controls.Add(this.dtPlanedFPFV);
            this.panel2.Controls.Add(this.dtPlanedGoLive);
            this.panel2.Controls.Add(this.lblPlannedSAR);
            this.panel2.Controls.Add(this.lblPlannedDBL);
            this.panel2.Controls.Add(this.lblPlannedBDRReview);
            this.panel2.Controls.Add(this.lblPlannedSAP);
            this.panel2.Controls.Add(this.lblPlannedCDISC);
            this.panel2.Controls.Add(this.lblPlannedGoLive);
            this.panel2.Controls.Add(this.lblPlannedLPLV);
            this.panel2.Controls.Add(this.lblPlannedFPFV);
            this.panel2.Controls.Add(this.lblPlannedDates);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(654, 486);
            this.panel2.TabIndex = 0;
            // 
            // txtActualBDR
            // 
            this.txtActualBDR.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualBDR.Location = new System.Drawing.Point(573, 317);
            this.txtActualBDR.MaxLength = 3;
            this.txtActualBDR.Name = "txtActualBDR";
            this.txtActualBDR.Size = new System.Drawing.Size(65, 22);
            this.txtActualBDR.TabIndex = 125;
            this.txtActualBDR.Visible = false;
            // 
            // dtActualBDR
            // 
            this.dtActualBDR.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtActualBDR.CustomFormat = "dd-MM-yyyy";
            this.dtActualBDR.Font = new System.Drawing.Font("Verdana", 9F);
            this.dtActualBDR.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtActualBDR.Location = new System.Drawing.Point(431, 317);
            this.dtActualBDR.Name = "dtActualBDR";
            this.dtActualBDR.Size = new System.Drawing.Size(125, 22);
            this.dtActualBDR.TabIndex = 124;
            this.dtActualBDR.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtActualBDR.Visible = false;
            // 
            // txtActualSAR
            // 
            this.txtActualSAR.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualSAR.Location = new System.Drawing.Point(573, 401);
            this.txtActualSAR.MaxLength = 3;
            this.txtActualSAR.Name = "txtActualSAR";
            this.txtActualSAR.Size = new System.Drawing.Size(65, 22);
            this.txtActualSAR.TabIndex = 129;
            this.txtActualSAR.Visible = false;
            // 
            // txtActualBDRReview
            // 
            this.txtActualBDRReview.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualBDRReview.Location = new System.Drawing.Point(573, 358);
            this.txtActualBDRReview.MaxLength = 3;
            this.txtActualBDRReview.Name = "txtActualBDRReview";
            this.txtActualBDRReview.Size = new System.Drawing.Size(65, 22);
            this.txtActualBDRReview.TabIndex = 127;
            this.txtActualBDRReview.Visible = false;
            // 
            // txtActualSAP
            // 
            this.txtActualSAP.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualSAP.Location = new System.Drawing.Point(573, 275);
            this.txtActualSAP.MaxLength = 3;
            this.txtActualSAP.Name = "txtActualSAP";
            this.txtActualSAP.Size = new System.Drawing.Size(65, 22);
            this.txtActualSAP.TabIndex = 123;
            this.txtActualSAP.Visible = false;
            // 
            // txtActualSDTM
            // 
            this.txtActualSDTM.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualSDTM.Location = new System.Drawing.Point(573, 232);
            this.txtActualSDTM.MaxLength = 3;
            this.txtActualSDTM.Name = "txtActualSDTM";
            this.txtActualSDTM.Size = new System.Drawing.Size(65, 22);
            this.txtActualSDTM.TabIndex = 121;
            this.txtActualSDTM.Visible = false;
            // 
            // txtActualDBL
            // 
            this.txtActualDBL.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualDBL.Location = new System.Drawing.Point(573, 189);
            this.txtActualDBL.MaxLength = 3;
            this.txtActualDBL.Name = "txtActualDBL";
            this.txtActualDBL.Size = new System.Drawing.Size(65, 22);
            this.txtActualDBL.TabIndex = 119;
            this.txtActualDBL.Visible = false;
            // 
            // txtActualLPLV
            // 
            this.txtActualLPLV.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualLPLV.Location = new System.Drawing.Point(573, 146);
            this.txtActualLPLV.MaxLength = 3;
            this.txtActualLPLV.Name = "txtActualLPLV";
            this.txtActualLPLV.Size = new System.Drawing.Size(65, 22);
            this.txtActualLPLV.TabIndex = 117;
            this.txtActualLPLV.Visible = false;
            // 
            // txtActualFPFV
            // 
            this.txtActualFPFV.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualFPFV.Location = new System.Drawing.Point(573, 107);
            this.txtActualFPFV.MaxLength = 3;
            this.txtActualFPFV.Name = "txtActualFPFV";
            this.txtActualFPFV.Size = new System.Drawing.Size(65, 22);
            this.txtActualFPFV.TabIndex = 113;
            this.txtActualFPFV.Visible = false;
            // 
            // txtActualGoLive
            // 
            this.txtActualGoLive.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualGoLive.Location = new System.Drawing.Point(573, 68);
            this.txtActualGoLive.MaxLength = 3;
            this.txtActualGoLive.Name = "txtActualGoLive";
            this.txtActualGoLive.Size = new System.Drawing.Size(65, 22);
            this.txtActualGoLive.TabIndex = 111;
            this.txtActualGoLive.Visible = false;
            // 
            // dtActualSAR
            // 
            this.dtActualSAR.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtActualSAR.CustomFormat = "dd-MM-yyyy";
            this.dtActualSAR.Font = new System.Drawing.Font("Verdana", 9F);
            this.dtActualSAR.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtActualSAR.Location = new System.Drawing.Point(431, 401);
            this.dtActualSAR.Name = "dtActualSAR";
            this.dtActualSAR.Size = new System.Drawing.Size(125, 22);
            this.dtActualSAR.TabIndex = 128;
            this.dtActualSAR.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtActualSAR.Visible = false;
            // 
            // dtActualBDRReview
            // 
            this.dtActualBDRReview.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtActualBDRReview.CustomFormat = "dd-MM-yyyy";
            this.dtActualBDRReview.Font = new System.Drawing.Font("Verdana", 9F);
            this.dtActualBDRReview.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtActualBDRReview.Location = new System.Drawing.Point(431, 358);
            this.dtActualBDRReview.Name = "dtActualBDRReview";
            this.dtActualBDRReview.Size = new System.Drawing.Size(125, 22);
            this.dtActualBDRReview.TabIndex = 126;
            this.dtActualBDRReview.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtActualBDRReview.Visible = false;
            // 
            // dtActualSAP
            // 
            this.dtActualSAP.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtActualSAP.CustomFormat = "dd-MM-yyyy";
            this.dtActualSAP.Font = new System.Drawing.Font("Verdana", 9F);
            this.dtActualSAP.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtActualSAP.Location = new System.Drawing.Point(431, 275);
            this.dtActualSAP.Name = "dtActualSAP";
            this.dtActualSAP.Size = new System.Drawing.Size(125, 22);
            this.dtActualSAP.TabIndex = 122;
            this.dtActualSAP.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtActualSAP.Visible = false;
            // 
            // dtActualSDTM
            // 
            this.dtActualSDTM.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtActualSDTM.CustomFormat = "dd-MM-yyyy";
            this.dtActualSDTM.Font = new System.Drawing.Font("Verdana", 9F);
            this.dtActualSDTM.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtActualSDTM.Location = new System.Drawing.Point(431, 232);
            this.dtActualSDTM.Name = "dtActualSDTM";
            this.dtActualSDTM.Size = new System.Drawing.Size(125, 22);
            this.dtActualSDTM.TabIndex = 120;
            this.dtActualSDTM.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtActualSDTM.Visible = false;
            // 
            // dtActualDBL
            // 
            this.dtActualDBL.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtActualDBL.CustomFormat = "dd-MM-yyyy";
            this.dtActualDBL.Font = new System.Drawing.Font("Verdana", 9F);
            this.dtActualDBL.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtActualDBL.Location = new System.Drawing.Point(431, 189);
            this.dtActualDBL.Name = "dtActualDBL";
            this.dtActualDBL.Size = new System.Drawing.Size(125, 22);
            this.dtActualDBL.TabIndex = 118;
            this.dtActualDBL.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtActualDBL.Visible = false;
            // 
            // dtActualLPLV
            // 
            this.dtActualLPLV.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtActualLPLV.CustomFormat = "dd-MM-yyyy";
            this.dtActualLPLV.Font = new System.Drawing.Font("Verdana", 9F);
            this.dtActualLPLV.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtActualLPLV.Location = new System.Drawing.Point(431, 146);
            this.dtActualLPLV.Name = "dtActualLPLV";
            this.dtActualLPLV.Size = new System.Drawing.Size(125, 22);
            this.dtActualLPLV.TabIndex = 116;
            this.dtActualLPLV.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtActualLPLV.Visible = false;
            // 
            // dtActualFPFV
            // 
            this.dtActualFPFV.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtActualFPFV.CustomFormat = "dd-MM-yyyy";
            this.dtActualFPFV.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtActualFPFV.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtActualFPFV.Location = new System.Drawing.Point(431, 107);
            this.dtActualFPFV.Name = "dtActualFPFV";
            this.dtActualFPFV.Size = new System.Drawing.Size(125, 22);
            this.dtActualFPFV.TabIndex = 112;
            this.dtActualFPFV.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtActualFPFV.Visible = false;
            // 
            // dtActualGoLive
            // 
            this.dtActualGoLive.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtActualGoLive.CustomFormat = "dd-MM-yyyy";
            this.dtActualGoLive.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtActualGoLive.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtActualGoLive.Location = new System.Drawing.Point(431, 68);
            this.dtActualGoLive.Name = "dtActualGoLive";
            this.dtActualGoLive.Size = new System.Drawing.Size(125, 22);
            this.dtActualGoLive.TabIndex = 110;
            this.dtActualGoLive.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtActualGoLive.Visible = false;
            // 
            // lblActualDays
            // 
            this.lblActualDays.AutoSize = true;
            this.lblActualDays.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActualDays.Location = new System.Drawing.Point(570, 46);
            this.lblActualDays.Name = "lblActualDays";
            this.lblActualDays.Size = new System.Drawing.Size(40, 14);
            this.lblActualDays.TabIndex = 131;
            this.lblActualDays.Text = "Days";
            // 
            // lblActualDates
            // 
            this.lblActualDates.AutoSize = true;
            this.lblActualDates.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActualDates.Location = new System.Drawing.Point(427, 46);
            this.lblActualDates.Name = "lblActualDates";
            this.lblActualDates.Size = new System.Drawing.Size(83, 14);
            this.lblActualDates.TabIndex = 130;
            this.lblActualDates.Text = "Actual Date";
            // 
            // chkAddActuals
            // 
            this.chkAddActuals.AutoSize = true;
            this.chkAddActuals.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAddActuals.Location = new System.Drawing.Point(421, 9);
            this.chkAddActuals.Name = "chkAddActuals";
            this.chkAddActuals.Size = new System.Drawing.Size(103, 18);
            this.chkAddActuals.TabIndex = 109;
            this.chkAddActuals.Text = "Add A&ctuals";
            this.chkAddActuals.UseVisualStyleBackColor = true;
            this.chkAddActuals.Visible = false;
            this.chkAddActuals.CheckedChanged += new System.EventHandler(this.chkAddActuals_CheckedChanged);
            // 
            // cmbStudyStatus
            // 
            this.cmbStudyStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStudyStatus.FormattingEnabled = true;
            this.cmbStudyStatus.Items.AddRange(new object[] {
            "Study Set-up",
            "Study Conduct",
            "DB Locked",
            "CDISC Delivered",
            "Closed",
            "Discontinued/Cancelled",
            "NA"});
            this.cmbStudyStatus.Location = new System.Drawing.Point(126, 6);
            this.cmbStudyStatus.Name = "cmbStudyStatus";
            this.cmbStudyStatus.Size = new System.Drawing.Size(188, 24);
            this.cmbStudyStatus.TabIndex = 54;
            this.cmbStudyStatus.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 14);
            this.label2.TabIndex = 53;
            this.label2.Text = "Study Status";
            // 
            // chkSameBDR
            // 
            this.chkSameBDR.AutoSize = true;
            this.chkSameBDR.Location = new System.Drawing.Point(374, 321);
            this.chkSameBDR.Name = "chkSameBDR";
            this.chkSameBDR.Size = new System.Drawing.Size(15, 14);
            this.chkSameBDR.TabIndex = 38;
            this.chkSameBDR.UseVisualStyleBackColor = true;
            this.chkSameBDR.CheckedChanged += new System.EventHandler(this.CheckBox_ValueChanged);
            // 
            // txtPlanedBDR
            // 
            this.txtPlanedBDR.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlanedBDR.Location = new System.Drawing.Point(249, 317);
            this.txtPlanedBDR.MaxLength = 3;
            this.txtPlanedBDR.Name = "txtPlanedBDR";
            this.txtPlanedBDR.Size = new System.Drawing.Size(65, 22);
            this.txtPlanedBDR.TabIndex = 37;
            this.txtPlanedBDR.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateKeyPress);
            this.txtPlanedBDR.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // dtPlanedBDR
            // 
            this.dtPlanedBDR.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPlanedBDR.CustomFormat = "dd-MM-yyyy";
            this.dtPlanedBDR.Font = new System.Drawing.Font("Verdana", 9F);
            this.dtPlanedBDR.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPlanedBDR.Location = new System.Drawing.Point(108, 317);
            this.dtPlanedBDR.Name = "dtPlanedBDR";
            this.dtPlanedBDR.Size = new System.Drawing.Size(125, 22);
            this.dtPlanedBDR.TabIndex = 36;
            this.dtPlanedBDR.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtPlanedBDR.ValueChanged += new System.EventHandler(this.dtPlaned_ValueChanged);
            this.dtPlanedBDR.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // lblPlannedBDR
            // 
            this.lblPlannedBDR.AutoSize = true;
            this.lblPlannedBDR.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedBDR.Location = new System.Drawing.Point(15, 321);
            this.lblPlannedBDR.Name = "lblPlannedBDR";
            this.lblPlannedBDR.Size = new System.Drawing.Size(32, 14);
            this.lblPlannedBDR.TabIndex = 35;
            this.lblPlannedBDR.Text = "&BDR";
            // 
            // chkSameSAR
            // 
            this.chkSameSAR.AutoSize = true;
            this.chkSameSAR.Location = new System.Drawing.Point(374, 405);
            this.chkSameSAR.Name = "chkSameSAR";
            this.chkSameSAR.Size = new System.Drawing.Size(15, 14);
            this.chkSameSAR.TabIndex = 50;
            this.chkSameSAR.UseVisualStyleBackColor = true;
            this.chkSameSAR.CheckedChanged += new System.EventHandler(this.CheckBox_ValueChanged);
            // 
            // chkSameBDRReview
            // 
            this.chkSameBDRReview.AutoSize = true;
            this.chkSameBDRReview.Location = new System.Drawing.Point(374, 363);
            this.chkSameBDRReview.Name = "chkSameBDRReview";
            this.chkSameBDRReview.Size = new System.Drawing.Size(15, 14);
            this.chkSameBDRReview.TabIndex = 44;
            this.chkSameBDRReview.UseVisualStyleBackColor = true;
            this.chkSameBDRReview.CheckedChanged += new System.EventHandler(this.CheckBox_ValueChanged);
            // 
            // chkSameSAP
            // 
            this.chkSameSAP.AutoSize = true;
            this.chkSameSAP.Location = new System.Drawing.Point(374, 279);
            this.chkSameSAP.Name = "chkSameSAP";
            this.chkSameSAP.Size = new System.Drawing.Size(15, 14);
            this.chkSameSAP.TabIndex = 108;
            this.chkSameSAP.UseVisualStyleBackColor = true;
            this.chkSameSAP.CheckedChanged += new System.EventHandler(this.CheckBox_ValueChanged);
            // 
            // chkSameSDTM
            // 
            this.chkSameSDTM.AutoSize = true;
            this.chkSameSDTM.Location = new System.Drawing.Point(374, 236);
            this.chkSameSDTM.Name = "chkSameSDTM";
            this.chkSameSDTM.Size = new System.Drawing.Size(15, 14);
            this.chkSameSDTM.TabIndex = 107;
            this.chkSameSDTM.UseVisualStyleBackColor = true;
            this.chkSameSDTM.CheckedChanged += new System.EventHandler(this.CheckBox_ValueChanged);
            // 
            // chkSameDBL
            // 
            this.chkSameDBL.AutoSize = true;
            this.chkSameDBL.Location = new System.Drawing.Point(374, 193);
            this.chkSameDBL.Name = "chkSameDBL";
            this.chkSameDBL.Size = new System.Drawing.Size(15, 14);
            this.chkSameDBL.TabIndex = 106;
            this.chkSameDBL.UseVisualStyleBackColor = true;
            this.chkSameDBL.CheckedChanged += new System.EventHandler(this.CheckBox_ValueChanged);
            // 
            // chkSameLPLV
            // 
            this.chkSameLPLV.AutoSize = true;
            this.chkSameLPLV.Location = new System.Drawing.Point(374, 150);
            this.chkSameLPLV.Name = "chkSameLPLV";
            this.chkSameLPLV.Size = new System.Drawing.Size(15, 14);
            this.chkSameLPLV.TabIndex = 105;
            this.chkSameLPLV.UseVisualStyleBackColor = true;
            this.chkSameLPLV.CheckedChanged += new System.EventHandler(this.CheckBox_ValueChanged);
            // 
            // chkSameFPFV
            // 
            this.chkSameFPFV.AutoSize = true;
            this.chkSameFPFV.Location = new System.Drawing.Point(374, 112);
            this.chkSameFPFV.Name = "chkSameFPFV";
            this.chkSameFPFV.Size = new System.Drawing.Size(15, 14);
            this.chkSameFPFV.TabIndex = 103;
            this.chkSameFPFV.UseVisualStyleBackColor = true;
            this.chkSameFPFV.CheckedChanged += new System.EventHandler(this.CheckBox_ValueChanged);
            // 
            // chkSameGoLive
            // 
            this.chkSameGoLive.AutoSize = true;
            this.chkSameGoLive.Location = new System.Drawing.Point(374, 72);
            this.chkSameGoLive.Name = "chkSameGoLive";
            this.chkSameGoLive.Size = new System.Drawing.Size(15, 14);
            this.chkSameGoLive.TabIndex = 102;
            this.chkSameGoLive.UseVisualStyleBackColor = true;
            this.chkSameGoLive.CheckedChanged += new System.EventHandler(this.CheckBox_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(316, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 14);
            this.label1.TabIndex = 101;
            this.label1.Text = "Add Actuals";
            // 
            // txtPlanedSAR
            // 
            this.txtPlanedSAR.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlanedSAR.Location = new System.Drawing.Point(249, 401);
            this.txtPlanedSAR.MaxLength = 3;
            this.txtPlanedSAR.Name = "txtPlanedSAR";
            this.txtPlanedSAR.Size = new System.Drawing.Size(65, 22);
            this.txtPlanedSAR.TabIndex = 49;
            this.txtPlanedSAR.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateKeyPress);
            this.txtPlanedSAR.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // txtPlanedBDRReview
            // 
            this.txtPlanedBDRReview.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlanedBDRReview.Location = new System.Drawing.Point(249, 358);
            this.txtPlanedBDRReview.MaxLength = 3;
            this.txtPlanedBDRReview.Name = "txtPlanedBDRReview";
            this.txtPlanedBDRReview.Size = new System.Drawing.Size(65, 22);
            this.txtPlanedBDRReview.TabIndex = 43;
            this.txtPlanedBDRReview.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateKeyPress);
            this.txtPlanedBDRReview.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // txtPlanedSAP
            // 
            this.txtPlanedSAP.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlanedSAP.Location = new System.Drawing.Point(249, 275);
            this.txtPlanedSAP.MaxLength = 3;
            this.txtPlanedSAP.Name = "txtPlanedSAP";
            this.txtPlanedSAP.Size = new System.Drawing.Size(65, 22);
            this.txtPlanedSAP.TabIndex = 32;
            this.txtPlanedSAP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateKeyPress);
            this.txtPlanedSAP.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // txtPlanedSDTM
            // 
            this.txtPlanedSDTM.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlanedSDTM.Location = new System.Drawing.Point(249, 232);
            this.txtPlanedSDTM.MaxLength = 3;
            this.txtPlanedSDTM.Name = "txtPlanedSDTM";
            this.txtPlanedSDTM.Size = new System.Drawing.Size(65, 22);
            this.txtPlanedSDTM.TabIndex = 27;
            this.txtPlanedSDTM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateKeyPress);
            this.txtPlanedSDTM.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // txtPlanedDBL
            // 
            this.txtPlanedDBL.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlanedDBL.Location = new System.Drawing.Point(249, 189);
            this.txtPlanedDBL.MaxLength = 3;
            this.txtPlanedDBL.Name = "txtPlanedDBL";
            this.txtPlanedDBL.Size = new System.Drawing.Size(65, 22);
            this.txtPlanedDBL.TabIndex = 22;
            this.txtPlanedDBL.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateKeyPress);
            this.txtPlanedDBL.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // txtPlanedLPLV
            // 
            this.txtPlanedLPLV.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlanedLPLV.Location = new System.Drawing.Point(249, 146);
            this.txtPlanedLPLV.MaxLength = 3;
            this.txtPlanedLPLV.Name = "txtPlanedLPLV";
            this.txtPlanedLPLV.Size = new System.Drawing.Size(65, 22);
            this.txtPlanedLPLV.TabIndex = 17;
            this.txtPlanedLPLV.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateKeyPress);
            this.txtPlanedLPLV.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // txtPlanedFPFV
            // 
            this.txtPlanedFPFV.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlanedFPFV.Location = new System.Drawing.Point(249, 107);
            this.txtPlanedFPFV.MaxLength = 3;
            this.txtPlanedFPFV.Name = "txtPlanedFPFV";
            this.txtPlanedFPFV.Size = new System.Drawing.Size(65, 22);
            this.txtPlanedFPFV.TabIndex = 7;
            this.txtPlanedFPFV.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateKeyPress);
            this.txtPlanedFPFV.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // txtPlanedGoLive
            // 
            this.txtPlanedGoLive.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlanedGoLive.Location = new System.Drawing.Point(249, 68);
            this.txtPlanedGoLive.MaxLength = 3;
            this.txtPlanedGoLive.Name = "txtPlanedGoLive";
            this.txtPlanedGoLive.Size = new System.Drawing.Size(65, 22);
            this.txtPlanedGoLive.TabIndex = 2;
            this.txtPlanedGoLive.TextChanged += new System.EventHandler(this.txtPlanedGoLive_TextChanged);
            this.txtPlanedGoLive.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateKeyPress);
            this.txtPlanedGoLive.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // lblPlannedDays
            // 
            this.lblPlannedDays.AutoSize = true;
            this.lblPlannedDays.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedDays.Location = new System.Drawing.Point(246, 46);
            this.lblPlannedDays.Name = "lblPlannedDays";
            this.lblPlannedDays.Size = new System.Drawing.Size(40, 14);
            this.lblPlannedDays.TabIndex = 98;
            this.lblPlannedDays.Text = "Days";
            // 
            // dtPlanedSAR
            // 
            this.dtPlanedSAR.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPlanedSAR.CustomFormat = "dd-MM-yyyy";
            this.dtPlanedSAR.Font = new System.Drawing.Font("Verdana", 9F);
            this.dtPlanedSAR.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPlanedSAR.Location = new System.Drawing.Point(108, 401);
            this.dtPlanedSAR.Name = "dtPlanedSAR";
            this.dtPlanedSAR.Size = new System.Drawing.Size(125, 22);
            this.dtPlanedSAR.TabIndex = 48;
            this.dtPlanedSAR.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtPlanedSAR.ValueChanged += new System.EventHandler(this.dtPlaned_ValueChanged);
            this.dtPlanedSAR.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // dtPlanedBDRReview
            // 
            this.dtPlanedBDRReview.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPlanedBDRReview.CustomFormat = "dd-MM-yyyy";
            this.dtPlanedBDRReview.Font = new System.Drawing.Font("Verdana", 9F);
            this.dtPlanedBDRReview.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPlanedBDRReview.Location = new System.Drawing.Point(108, 358);
            this.dtPlanedBDRReview.Name = "dtPlanedBDRReview";
            this.dtPlanedBDRReview.Size = new System.Drawing.Size(125, 22);
            this.dtPlanedBDRReview.TabIndex = 42;
            this.dtPlanedBDRReview.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtPlanedBDRReview.ValueChanged += new System.EventHandler(this.dtPlaned_ValueChanged);
            this.dtPlanedBDRReview.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // dtPlanedSAP
            // 
            this.dtPlanedSAP.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPlanedSAP.CustomFormat = "dd-MM-yyyy";
            this.dtPlanedSAP.Font = new System.Drawing.Font("Verdana", 9F);
            this.dtPlanedSAP.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPlanedSAP.Location = new System.Drawing.Point(108, 275);
            this.dtPlanedSAP.Name = "dtPlanedSAP";
            this.dtPlanedSAP.Size = new System.Drawing.Size(125, 22);
            this.dtPlanedSAP.TabIndex = 31;
            this.dtPlanedSAP.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtPlanedSAP.ValueChanged += new System.EventHandler(this.dtPlaned_ValueChanged);
            this.dtPlanedSAP.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // dtPlanedSDTM
            // 
            this.dtPlanedSDTM.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPlanedSDTM.CustomFormat = "dd-MM-yyyy";
            this.dtPlanedSDTM.Font = new System.Drawing.Font("Verdana", 9F);
            this.dtPlanedSDTM.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPlanedSDTM.Location = new System.Drawing.Point(108, 232);
            this.dtPlanedSDTM.Name = "dtPlanedSDTM";
            this.dtPlanedSDTM.Size = new System.Drawing.Size(125, 22);
            this.dtPlanedSDTM.TabIndex = 26;
            this.dtPlanedSDTM.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtPlanedSDTM.ValueChanged += new System.EventHandler(this.dtPlaned_ValueChanged);
            this.dtPlanedSDTM.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // dtPlanedDBL
            // 
            this.dtPlanedDBL.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPlanedDBL.CustomFormat = "dd-MM-yyyy";
            this.dtPlanedDBL.Font = new System.Drawing.Font("Verdana", 9F);
            this.dtPlanedDBL.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPlanedDBL.Location = new System.Drawing.Point(108, 189);
            this.dtPlanedDBL.Name = "dtPlanedDBL";
            this.dtPlanedDBL.Size = new System.Drawing.Size(125, 22);
            this.dtPlanedDBL.TabIndex = 21;
            this.dtPlanedDBL.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtPlanedDBL.ValueChanged += new System.EventHandler(this.dtPlaned_ValueChanged);
            this.dtPlanedDBL.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // dtPlanedLPLV
            // 
            this.dtPlanedLPLV.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPlanedLPLV.CustomFormat = "dd-MM-yyyy";
            this.dtPlanedLPLV.Font = new System.Drawing.Font("Verdana", 9F);
            this.dtPlanedLPLV.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPlanedLPLV.Location = new System.Drawing.Point(108, 146);
            this.dtPlanedLPLV.Name = "dtPlanedLPLV";
            this.dtPlanedLPLV.Size = new System.Drawing.Size(125, 22);
            this.dtPlanedLPLV.TabIndex = 16;
            this.dtPlanedLPLV.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtPlanedLPLV.ValueChanged += new System.EventHandler(this.dtPlaned_ValueChanged);
            this.dtPlanedLPLV.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // dtPlanedFPFV
            // 
            this.dtPlanedFPFV.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPlanedFPFV.CustomFormat = "dd-MM-yyyy";
            this.dtPlanedFPFV.Font = new System.Drawing.Font("Verdana", 9F);
            this.dtPlanedFPFV.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPlanedFPFV.Location = new System.Drawing.Point(108, 107);
            this.dtPlanedFPFV.Name = "dtPlanedFPFV";
            this.dtPlanedFPFV.Size = new System.Drawing.Size(125, 22);
            this.dtPlanedFPFV.TabIndex = 6;
            this.dtPlanedFPFV.Value = new System.DateTime(2021, 3, 15, 18, 32, 15, 0);
            this.dtPlanedFPFV.ValueChanged += new System.EventHandler(this.dtPlaned_ValueChanged);
            this.dtPlanedFPFV.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // dtPlanedGoLive
            // 
            this.dtPlanedGoLive.CalendarFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPlanedGoLive.CustomFormat = "dd-MM-yyyy";
            this.dtPlanedGoLive.Font = new System.Drawing.Font("Verdana", 9F);
            this.dtPlanedGoLive.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPlanedGoLive.Location = new System.Drawing.Point(108, 68);
            this.dtPlanedGoLive.Name = "dtPlanedGoLive";
            this.dtPlanedGoLive.Size = new System.Drawing.Size(125, 22);
            this.dtPlanedGoLive.TabIndex = 1;
            this.dtPlanedGoLive.Value = new System.DateTime(2021, 3, 15, 18, 40, 58, 0);
            this.dtPlanedGoLive.ValueChanged += new System.EventHandler(this.dtPlaned_ValueChanged);
            this.dtPlanedGoLive.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // lblPlannedSAR
            // 
            this.lblPlannedSAR.AutoSize = true;
            this.lblPlannedSAR.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedSAR.Location = new System.Drawing.Point(15, 405);
            this.lblPlannedSAR.Name = "lblPlannedSAR";
            this.lblPlannedSAR.Size = new System.Drawing.Size(31, 14);
            this.lblPlannedSAR.TabIndex = 47;
            this.lblPlannedSAR.Text = "SA&R";
            // 
            // lblPlannedDBL
            // 
            this.lblPlannedDBL.AutoSize = true;
            this.lblPlannedDBL.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedDBL.Location = new System.Drawing.Point(15, 193);
            this.lblPlannedDBL.Name = "lblPlannedDBL";
            this.lblPlannedDBL.Size = new System.Drawing.Size(31, 14);
            this.lblPlannedDBL.TabIndex = 20;
            this.lblPlannedDBL.Text = "&DBL";
            // 
            // lblPlannedBDRReview
            // 
            this.lblPlannedBDRReview.AutoSize = true;
            this.lblPlannedBDRReview.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedBDRReview.Location = new System.Drawing.Point(15, 362);
            this.lblPlannedBDRReview.Name = "lblPlannedBDRReview";
            this.lblPlannedBDRReview.Size = new System.Drawing.Size(81, 14);
            this.lblPlannedBDRReview.TabIndex = 41;
            this.lblPlannedBDRReview.Text = "BDR Revie&w";
            // 
            // lblPlannedSAP
            // 
            this.lblPlannedSAP.AutoSize = true;
            this.lblPlannedSAP.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedSAP.Location = new System.Drawing.Point(15, 279);
            this.lblPlannedSAP.Name = "lblPlannedSAP";
            this.lblPlannedSAP.Size = new System.Drawing.Size(31, 14);
            this.lblPlannedSAP.TabIndex = 30;
            this.lblPlannedSAP.Text = "S&AP";
            // 
            // lblPlannedCDISC
            // 
            this.lblPlannedCDISC.AutoSize = true;
            this.lblPlannedCDISC.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedCDISC.Location = new System.Drawing.Point(15, 236);
            this.lblPlannedCDISC.Name = "lblPlannedCDISC";
            this.lblPlannedCDISC.Size = new System.Drawing.Size(86, 14);
            this.lblPlannedCDISC.TabIndex = 25;
            this.lblPlannedCDISC.Text = "CD&ISC/SDTM";
            // 
            // lblPlannedGoLive
            // 
            this.lblPlannedGoLive.AutoSize = true;
            this.lblPlannedGoLive.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedGoLive.Location = new System.Drawing.Point(15, 72);
            this.lblPlannedGoLive.Name = "lblPlannedGoLive";
            this.lblPlannedGoLive.Size = new System.Drawing.Size(54, 14);
            this.lblPlannedGoLive.TabIndex = 0;
            this.lblPlannedGoLive.Text = "&Go-Live";
            // 
            // lblPlannedLPLV
            // 
            this.lblPlannedLPLV.AutoSize = true;
            this.lblPlannedLPLV.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedLPLV.Location = new System.Drawing.Point(15, 150);
            this.lblPlannedLPLV.Name = "lblPlannedLPLV";
            this.lblPlannedLPLV.Size = new System.Drawing.Size(36, 14);
            this.lblPlannedLPLV.TabIndex = 15;
            this.lblPlannedLPLV.Text = "L&PLV";
            // 
            // lblPlannedFPFV
            // 
            this.lblPlannedFPFV.AutoSize = true;
            this.lblPlannedFPFV.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedFPFV.Location = new System.Drawing.Point(15, 111);
            this.lblPlannedFPFV.Name = "lblPlannedFPFV";
            this.lblPlannedFPFV.Size = new System.Drawing.Size(37, 14);
            this.lblPlannedFPFV.TabIndex = 5;
            this.lblPlannedFPFV.Text = "&FPFV";
            // 
            // lblPlannedDates
            // 
            this.lblPlannedDates.AutoSize = true;
            this.lblPlannedDates.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedDates.Location = new System.Drawing.Point(105, 46);
            this.lblPlannedDates.Name = "lblPlannedDates";
            this.lblPlannedDates.Size = new System.Drawing.Size(95, 14);
            this.lblPlannedDates.TabIndex = 59;
            this.lblPlannedDates.Text = "Planned Date";
            // 
            // tbComment
            // 
            this.tbComment.Controls.Add(this.panel3);
            this.tbComment.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbComment.Location = new System.Drawing.Point(4, 34);
            this.tbComment.Name = "tbComment";
            this.tbComment.Size = new System.Drawing.Size(654, 486);
            this.tbComment.TabIndex = 3;
            this.tbComment.Text = "Comments";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtCytelComment);
            this.panel3.Controls.Add(this.txtClientComment);
            this.panel3.Controls.Add(this.lblCytelComment);
            this.panel3.Controls.Add(this.lblClientComment);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(654, 486);
            this.panel3.TabIndex = 0;
            // 
            // txtCytelComment
            // 
            this.txtCytelComment.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCytelComment.Location = new System.Drawing.Point(19, 263);
            this.txtCytelComment.Multiline = true;
            this.txtCytelComment.Name = "txtCytelComment";
            this.txtCytelComment.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtCytelComment.Size = new System.Drawing.Size(600, 150);
            this.txtCytelComment.TabIndex = 3;
            this.txtCytelComment.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // txtClientComment
            // 
            this.txtClientComment.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClientComment.Location = new System.Drawing.Point(19, 47);
            this.txtClientComment.Multiline = true;
            this.txtClientComment.Name = "txtClientComment";
            this.txtClientComment.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtClientComment.Size = new System.Drawing.Size(600, 150);
            this.txtClientComment.TabIndex = 1;
            this.txtClientComment.MouseHover += new System.EventHandler(this.MouseHover_AddProject);
            // 
            // lblCytelComment
            // 
            this.lblCytelComment.AutoSize = true;
            this.lblCytelComment.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCytelComment.Location = new System.Drawing.Point(16, 232);
            this.lblCytelComment.Name = "lblCytelComment";
            this.lblCytelComment.Size = new System.Drawing.Size(110, 14);
            this.lblCytelComment.TabIndex = 2;
            this.lblCytelComment.Text = "C&ytel Comments";
            // 
            // lblClientComment
            // 
            this.lblClientComment.AutoSize = true;
            this.lblClientComment.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClientComment.Location = new System.Drawing.Point(16, 16);
            this.lblClientComment.Name = "lblClientComment";
            this.lblClientComment.Size = new System.Drawing.Size(114, 14);
            this.lblClientComment.TabIndex = 0;
            this.lblClientComment.Text = "C&lient Comments";
            // 
            // UcMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tbMain);
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "UcMain";
            this.Size = new System.Drawing.Size(662, 524);
            this.tbMain.ResumeLayout(false);
            this.tbProjDetails.ResumeLayout(false);
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            this.tbStudyInfo.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tbMilestones.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tbComment.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tbMain;
        private System.Windows.Forms.TabPage tbProjDetails;
        private System.Windows.Forms.TabPage tbStudyInfo;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.TabPage tbMilestones;
        private System.Windows.Forms.TabPage tbComment;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtCytelComment;
        private System.Windows.Forms.TextBox txtClientComment;
        private System.Windows.Forms.Label lblCytelComment;
        private System.Windows.Forms.Label lblClientComment;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtDataManagement;
        private System.Windows.Forms.Label lblPOC;
        private System.Windows.Forms.Label lblDataMgmt;
        private System.Windows.Forms.Label lblAwarded;
        private System.Windows.Forms.Label lblCDISC;
        private System.Windows.Forms.Label lblStatistics;
        private System.Windows.Forms.TextBox txtStatisics;
        private System.Windows.Forms.CheckBox chkDataManagement;
        private System.Windows.Forms.TextBox txtCDISC;
        private System.Windows.Forms.CheckBox chkCDISC;
        private System.Windows.Forms.CheckBox chkStatisics;
        private System.Windows.Forms.DateTimePicker dtAssignedOn;
        private System.Windows.Forms.Label lblAssignedOn;
        //private System.Windows.Forms.TextBox txtStudyDuration;
        //private System.Windows.Forms.Label lblStudyDuration;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox txtProjectName;
        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.Label lblProjectName;
        private System.Windows.Forms.TextBox txtCSSName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtGCLName;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtCSLName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbCRO;
        private System.Windows.Forms.ComboBox cmbCountry;
        private System.Windows.Forms.ComboBox cmbCategory;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chkAddActuals;
        private System.Windows.Forms.ComboBox cmbStudyStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkSameBDR;
        private System.Windows.Forms.TextBox txtPlanedBDR;
        private System.Windows.Forms.DateTimePicker dtPlanedBDR;
        private System.Windows.Forms.Label lblPlannedBDR;
        private System.Windows.Forms.CheckBox chkSameSAR;
        private System.Windows.Forms.CheckBox chkSameBDRReview;
        private System.Windows.Forms.CheckBox chkSameSAP;
        private System.Windows.Forms.CheckBox chkSameSDTM;
        private System.Windows.Forms.CheckBox chkSameDBL;
        private System.Windows.Forms.CheckBox chkSameLPLV;
        //private System.Windows.Forms.CheckBox chkSameLPFV;
        private System.Windows.Forms.CheckBox chkSameFPFV;
        private System.Windows.Forms.CheckBox chkSameGoLive;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPlanedSAR;
        private System.Windows.Forms.TextBox txtPlanedBDRReview;
        private System.Windows.Forms.TextBox txtPlanedSAP;
        private System.Windows.Forms.TextBox txtPlanedSDTM;
        private System.Windows.Forms.TextBox txtPlanedDBL;
        private System.Windows.Forms.TextBox txtPlanedLPLV;
        //private System.Windows.Forms.TextBox txtPlanedLPFV;
        private System.Windows.Forms.TextBox txtPlanedFPFV;
        private System.Windows.Forms.TextBox txtPlanedGoLive;
        private System.Windows.Forms.Label lblPlannedDays;
        private System.Windows.Forms.DateTimePicker dtPlanedSAR;
        private System.Windows.Forms.DateTimePicker dtPlanedBDRReview;
        private System.Windows.Forms.DateTimePicker dtPlanedSAP;
        private System.Windows.Forms.DateTimePicker dtPlanedSDTM;
        private System.Windows.Forms.DateTimePicker dtPlanedDBL;
        private System.Windows.Forms.DateTimePicker dtPlanedLPLV;
        //private System.Windows.Forms.DateTimePicker dtPlanedLPFV;
        private System.Windows.Forms.DateTimePicker dtPlanedFPFV;
        private System.Windows.Forms.DateTimePicker dtPlanedGoLive;
        private System.Windows.Forms.Label lblPlannedSAR;
        private System.Windows.Forms.Label lblPlannedDBL;
        private System.Windows.Forms.Label lblPlannedBDRReview;
        private System.Windows.Forms.Label lblPlannedSAP;
        private System.Windows.Forms.Label lblPlannedCDISC;
        private System.Windows.Forms.Label lblPlannedGoLive;
        private System.Windows.Forms.Label lblPlannedLPLV;
        //private System.Windows.Forms.Label lblPlannedLPFV;
        private System.Windows.Forms.Label lblPlannedFPFV;
        private System.Windows.Forms.Label lblPlannedDates;
        private System.Windows.Forms.TextBox txtOtherCountry;
        private System.Windows.Forms.Label lblOtherCountry;
        private System.Windows.Forms.TextBox txtOtherCRO;
        private System.Windows.Forms.Label lblOtherCRO;
        private System.Windows.Forms.TextBox txtOtherCategory;
        private System.Windows.Forms.Label lblOtherCategory;
        private System.Windows.Forms.TextBox txtActualBDR;
        private System.Windows.Forms.DateTimePicker dtActualBDR;
        private System.Windows.Forms.TextBox txtActualSAR;
        private System.Windows.Forms.TextBox txtActualBDRReview;
        private System.Windows.Forms.TextBox txtActualSAP;
        private System.Windows.Forms.TextBox txtActualSDTM;
        private System.Windows.Forms.TextBox txtActualDBL;
        private System.Windows.Forms.TextBox txtActualLPLV;
        //private System.Windows.Forms.TextBox txtActualLPFV;
        private System.Windows.Forms.TextBox txtActualFPFV;
        private System.Windows.Forms.TextBox txtActualGoLive;
        private System.Windows.Forms.DateTimePicker dtActualSAR;
        private System.Windows.Forms.DateTimePicker dtActualBDRReview;
        private System.Windows.Forms.DateTimePicker dtActualSAP;
        private System.Windows.Forms.DateTimePicker dtActualSDTM;
        private System.Windows.Forms.DateTimePicker dtActualDBL;
        private System.Windows.Forms.DateTimePicker dtActualLPLV;
        //private System.Windows.Forms.DateTimePicker dtActualLPFV;
        private System.Windows.Forms.DateTimePicker dtActualFPFV;
        private System.Windows.Forms.DateTimePicker dtActualGoLive;
        private System.Windows.Forms.Label lblActualDays;
        private System.Windows.Forms.Label lblActualDates;
    }
}
