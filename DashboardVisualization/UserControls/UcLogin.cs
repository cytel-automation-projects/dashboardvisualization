﻿using DashboardVisualization.Models;
using System;
using System.IO;
using System.Windows.Forms;


namespace DashboardVisualization.UserControls
{
    public partial class UcLogin : UserControl
    {
        public UcLogin()
        {
            InitializeComponent();
        }

        private void btnBrowseExisting_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                //set path of workspace directory
                InitialDirectory = DashboardVisualization.Models.Constants.WorkSpaceFolderPath,
                Title = "Browse excel file",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "xlsx",
                Filter = "Excel file (*.xlsx)|*.xlsx",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true,
                Multiselect = false
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtExistingPath.Text = openFileDialog1.FileName;
            }
        }

        /// <summary>
        /// Validation of login Inputs
        /// </summary>
        /// <returns></returns>
        public bool ValidateLoginInputs()
        {
            bool inputsValid = true;

            if ((rdbCreateNewWrkspc.Checked == false) && (rdbLoadExisting.Checked == false))
            {
                MessageBox.Show("Please select atleast one option to proceed.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                inputsValid = false;
            }

            if (rdbLoadExisting.Checked)
            {
                //validation for path
                if (string.IsNullOrWhiteSpace(txtExistingPath.Text))
                {
                    MessageBox.Show("Please select an existing workspace file.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtExistingPath.Focus();
                    inputsValid = false;
                }
            }

            return inputsValid;
        }


        /// <summary>
        /// create new workspace i.e. excel workspace file
        /// </summary>
        private void CreateExcelSheet()
        {
            //create copy of workSpaceTmplatePath
            File.Copy(Constants.WorkSpaceTemplatePath, Constants.WorkSpaceFilePath);

            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook;
            Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet;

            try
            {
                excelWorkbook = app.Workbooks.Open(Constants.WorkSpaceFilePath);
                if (excelWorkbook.Sheets.Count > 0)
                {
                    excelWorkSheet = excelWorkbook.Sheets[1];
                    //Rename the sheet 
                    excelWorkSheet.Name = Constants.ShtProjData;
                }
                //Save the excel 
                excelWorkbook.Save();
                //Close the excel
                excelWorkbook.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Export Excel Failed: " + ex.Message);
            }
            finally
            {
                //Clean up objects 
                app.Quit();
                app = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        /// <summary>
        /// Perform workpace operations on login ok button click
        /// </summary>
        /// <returns></returns>
        public bool LoadWorkspace()
        {
            if (ValidateLoginInputs() == false)
                return false;

            if (rdbCreateNewWrkspc.Checked)
            {
                if (string.IsNullOrWhiteSpace(txtClient.Text))
                {
                    MessageBox.Show("Workspace/Client name is not specified for creating new workspace. Please specify a valid input for this workspace.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtClient.Focus();
                    return false;
                }

                //create new blank workspace in the 'workspace' folder      
                DateTime now = DateTime.Now;
                Constants.WorkSpaceFilePath = Path.Combine(Constants.WorkSpaceFolderPath, "WorkSpace" + now.ToString("_MMddyyyy_HHmm_") + txtClient.Text + ".xlsx");

                //create new workspace
                CreateExcelSheet();
            }

            if (rdbLoadExisting.Checked)
            {
                //check file extension
                string fileExtn = Path.GetExtension(txtExistingPath.Text);
                if (fileExtn != ".xlsx" && fileExtn != ".xls")
                {
                    MessageBox.Show("Please select valid existing workspace file.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtExistingPath.Focus();
                    return false;
                }

                // check in workspace file 
                if (EngineInvoker.LoadWorkspace(txtExistingPath.Text))
                {
                    Constants.WorkSpaceFilePath = txtExistingPath.Text;
                }
                else
                {
                    MessageBox.Show("Dashboard tool could not load the specified existing workspace. Please check if the file exists.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtExistingPath.Focus();
                    return false;
                }
            }

            return true;
        }

        private void rdbLoadExisting_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rdbCreateNewWrkspc_CheckedChanged(object sender, EventArgs e)
        {
            txtClient.Enabled = rdbCreateNewWrkspc.Checked;
        }

        #region Mouse hover mothods

        private void SetToolTip(Control control, string tooltipText = "Tooltip not available for this control")
        {
            if (control == null)
                return;

            toolTip1.SetToolTip(control, tooltipText);
        }

        private string GetHelpText_Login(string controlName)
        {
            if (string.IsNullOrWhiteSpace(controlName)) return string.Empty;

            string helpText = string.Empty;
            switch (controlName)
            {
                case Constants.ControlName_rdbLoadExisting:
                    helpText = Constants.HelpText_rdbLoadExisting;
                    break;

                case Constants.ControlName_txtExistingPath:
                    helpText = Constants.HelpText_txtExistingPath;
                    break;

                case Constants.ControlName_btnBrowseExisting:
                    helpText = Constants.HelpText_btnBrowseExisting;
                    break;

                case Constants.ControlName_rdbCreateNewWrkspc:
                    helpText = Constants.HelpText_rdbCreateNewWrkspc;
                    break;

                case Constants.ControlName_txtClient:
                    helpText = Constants.HelpText_txtClient;
                    break;

                default:
                    helpText = "";
                    break;
            }

            return helpText;
        }

        private void MouseHover_Login(object sender, EventArgs e)
        {
            SetToolTip((Control)sender, GetHelpText_Login(((Control)sender).Name));
        }

        #endregion

        #region Commented: Earlier mouse hover events

        //private void rdbLoadExisting_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_Login(((Control)sender).Name));
        //}

        //private void txtExistingPath_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_Login(((Control)sender).Name));
        //}

        //private void btnBrowseExisting_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_Login(((Control)sender).Name));
        //}

        //private void rdbCreateNewWrkspc_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_Login(((Control)sender).Name));
        //}

        //private void txtClient_MouseHover(object sender, EventArgs e)
        //{
        //    SetToolTip((Control)sender, GetHelpText_Login(((Control)sender).Name));
        //}
        #endregion
    }
}
