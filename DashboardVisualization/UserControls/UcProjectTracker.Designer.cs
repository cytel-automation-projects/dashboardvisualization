﻿namespace DashboardVisualization.UserControls
{
    partial class UcProjectTracker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnAddProject = new System.Windows.Forms.Button();
            this.btnEditProject = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnProjPortfolio = new System.Windows.Forms.Button();
            this.btnProjDashboard = new System.Windows.Forms.Button();
            this.advancedDataGridView1 = new Zuby.ADGV.AdvancedDataGridView();
            this.bindingSource_main = new System.Windows.Forms.BindingSource(this.components);
            this.btnShowHideCols = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.grpOpen = new System.Windows.Forms.GroupBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.advancedDataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_main)).BeginInit();
            this.grpOpen.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAddProject
            // 
            this.btnAddProject.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddProject.Location = new System.Drawing.Point(12, 12);
            this.btnAddProject.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddProject.Name = "btnAddProject";
            this.btnAddProject.Size = new System.Drawing.Size(112, 31);
            this.btnAddProject.TabIndex = 0;
            this.btnAddProject.Text = "&Add Project...";
            this.btnAddProject.UseVisualStyleBackColor = true;
            this.btnAddProject.Click += new System.EventHandler(this.btnAddProject_Click);
            this.btnAddProject.MouseHover += new System.EventHandler(this.MouseHover_Event);
            // 
            // btnEditProject
            // 
            this.btnEditProject.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditProject.Location = new System.Drawing.Point(144, 12);
            this.btnEditProject.Margin = new System.Windows.Forms.Padding(2);
            this.btnEditProject.Name = "btnEditProject";
            this.btnEditProject.Size = new System.Drawing.Size(112, 31);
            this.btnEditProject.TabIndex = 1;
            this.btnEditProject.Text = "&Edit Project...";
            this.btnEditProject.UseVisualStyleBackColor = true;
            this.btnEditProject.Click += new System.EventHandler(this.btnEditProject_Click);
            this.btnEditProject.MouseHover += new System.EventHandler(this.MouseHover_Event);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnProjPortfolio);
            this.groupBox1.Controls.Add(this.btnProjDashboard);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 294);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(406, 72);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Generate Visualization";
            // 
            // btnProjPortfolio
            // 
            this.btnProjPortfolio.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProjPortfolio.Location = new System.Drawing.Point(211, 25);
            this.btnProjPortfolio.Margin = new System.Windows.Forms.Padding(2);
            this.btnProjPortfolio.Name = "btnProjPortfolio";
            this.btnProjPortfolio.Size = new System.Drawing.Size(185, 31);
            this.btnProjPortfolio.TabIndex = 1;
            this.btnProjPortfolio.Text = "&Portfolio Dashboard...";
            this.btnProjPortfolio.UseVisualStyleBackColor = true;
            this.btnProjPortfolio.Click += new System.EventHandler(this.btnProjPortfolio_Click);
            this.btnProjPortfolio.MouseHover += new System.EventHandler(this.MouseHover_Event);
            // 
            // btnProjDashboard
            // 
            this.btnProjDashboard.AutoEllipsis = true;
            this.btnProjDashboard.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProjDashboard.Location = new System.Drawing.Point(6, 25);
            this.btnProjDashboard.Margin = new System.Windows.Forms.Padding(2);
            this.btnProjDashboard.Name = "btnProjDashboard";
            this.btnProjDashboard.Size = new System.Drawing.Size(185, 31);
            this.btnProjDashboard.TabIndex = 0;
            this.btnProjDashboard.Text = "Project &Dashboard...";
            this.btnProjDashboard.UseVisualStyleBackColor = true;
            this.btnProjDashboard.Click += new System.EventHandler(this.btnProjDashboard_Click);
            this.btnProjDashboard.MouseHover += new System.EventHandler(this.MouseHover_Event);
            // 
            // advancedDataGridView1
            // 
            this.advancedDataGridView1.AllowUserToAddRows = false;
            this.advancedDataGridView1.AllowUserToDeleteRows = false;
            this.advancedDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.advancedDataGridView1.FilterAndSortEnabled = true;
            this.advancedDataGridView1.Location = new System.Drawing.Point(12, 85);
            this.advancedDataGridView1.Name = "advancedDataGridView1";
            this.advancedDataGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.advancedDataGridView1.Size = new System.Drawing.Size(838, 198);
            this.advancedDataGridView1.TabIndex = 4;
            this.advancedDataGridView1.SortStringChanged += new System.EventHandler<Zuby.ADGV.AdvancedDataGridView.SortEventArgs>(this.advancedDataGridView1_SortStringChanged);
            this.advancedDataGridView1.FilterStringChanged += new System.EventHandler<Zuby.ADGV.AdvancedDataGridView.FilterEventArgs>(this.advancedDataGridView1_FilterStringChanged);
            this.advancedDataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.advancedDataGridView1_CellContentClick);
            this.advancedDataGridView1.MouseHover += new System.EventHandler(this.MouseHover_Event);
            // 
            // bindingSource_main
            // 
            this.bindingSource_main.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.bindingSource_main_ListChanged);
            // 
            // btnShowHideCols
            // 
            this.btnShowHideCols.Font = new System.Drawing.Font("Verdana", 9F);
            this.btnShowHideCols.Location = new System.Drawing.Point(690, 12);
            this.btnShowHideCols.Name = "btnShowHideCols";
            this.btnShowHideCols.Size = new System.Drawing.Size(160, 31);
            this.btnShowHideCols.TabIndex = 5;
            this.btnShowHideCols.Text = "Show/Hide Columns...";
            this.btnShowHideCols.UseVisualStyleBackColor = true;
            this.btnShowHideCols.Click += new System.EventHandler(this.btnShowHideCols_Click);
            this.btnShowHideCols.MouseHover += new System.EventHandler(this.MouseHover_Event);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(12, 55);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(104, 15);
            this.linkLabel1.TabIndex = 6;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Select All Projects";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            this.linkLabel1.MouseHover += new System.EventHandler(this.MouseHover_Event);
            // 
            // grpOpen
            // 
            this.grpOpen.Controls.Add(this.btnOpen);
            this.grpOpen.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpOpen.Location = new System.Drawing.Point(422, 294);
            this.grpOpen.Margin = new System.Windows.Forms.Padding(2);
            this.grpOpen.Name = "grpOpen";
            this.grpOpen.Padding = new System.Windows.Forms.Padding(2);
            this.grpOpen.Size = new System.Drawing.Size(428, 72);
            this.grpOpen.TabIndex = 4;
            this.grpOpen.TabStop = false;
            this.grpOpen.Text = "Open Visualization";
            // 
            // btnOpen
            // 
            this.btnOpen.AutoEllipsis = true;
            this.btnOpen.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpen.Location = new System.Drawing.Point(6, 25);
            this.btnOpen.Margin = new System.Windows.Forms.Padding(2);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(198, 31);
            this.btnOpen.TabIndex = 0;
            this.btnOpen.Text = "&Open Graph Workbook...";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            this.btnOpen.MouseHover += new System.EventHandler(this.MouseHover_Event);
            // 
            // btnImport
            // 
            this.btnImport.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImport.Location = new System.Drawing.Point(506, 12);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(167, 31);
            this.btnImport.TabIndex = 7;
            this.btnImport.Text = "Import Project Details...";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // UcProjectTracker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.grpOpen);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.btnShowHideCols);
            this.Controls.Add(this.advancedDataGridView1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnEditProject);
            this.Controls.Add(this.btnAddProject);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "UcProjectTracker";
            this.Size = new System.Drawing.Size(871, 380);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.advancedDataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_main)).EndInit();
            this.grpOpen.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddProject;
        private System.Windows.Forms.Button btnEditProject;
        //private System.Windows.Forms.BindingSource bindingSource_main;

        //private Zuby.ADGV.AdvancedDataGridView advancedDataGridView;
        //private Zuby.ADGV.AdvancedDataGridViewSearchToolBar advancedDataGridViewSearchToolBar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnProjPortfolio;
        private System.Windows.Forms.Button btnProjDashboard;
        private System.Windows.Forms.BindingSource bindingSource_main;
        private Zuby.ADGV.AdvancedDataGridView advancedDataGridView1;
        private System.Windows.Forms.Button btnShowHideCols;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.GroupBox grpOpen;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Button btnImport;
    }
}
