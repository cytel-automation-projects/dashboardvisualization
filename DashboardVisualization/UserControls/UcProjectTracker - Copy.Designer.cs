﻿namespace DashboardVisualization.UserControls
{
    partial class UcProjectTracker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddProject = new System.Windows.Forms.Button();
            this.btnEditProject = new System.Windows.Forms.Button();
            this.gvProjectData = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnProjPortfolio = new System.Windows.Forms.Button();
            this.btnProjDashboard = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gvProjectData)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAddProject
            // 
            this.btnAddProject.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddProject.Location = new System.Drawing.Point(13, 17);
            this.btnAddProject.Name = "btnAddProject";
            this.btnAddProject.Size = new System.Drawing.Size(128, 27);
            this.btnAddProject.TabIndex = 0;
            this.btnAddProject.Text = "&Add Project...";
            this.btnAddProject.UseVisualStyleBackColor = true;
            this.btnAddProject.Click += new System.EventHandler(this.btnAddProject_Click);
            // 
            // btnEditProject
            // 
            this.btnEditProject.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditProject.Location = new System.Drawing.Point(164, 17);
            this.btnEditProject.Name = "btnEditProject";
            this.btnEditProject.Size = new System.Drawing.Size(116, 27);
            this.btnEditProject.TabIndex = 1;
            this.btnEditProject.Text = "&Edit Project...";
            this.btnEditProject.UseVisualStyleBackColor = true;
            this.btnEditProject.Click += new System.EventHandler(this.btnEditProject_Click);
            // 
            // gvProjectData
            // 
            this.gvProjectData.AllowUserToAddRows = false;
            this.gvProjectData.AllowUserToDeleteRows = false;
            this.gvProjectData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvProjectData.Location = new System.Drawing.Point(13, 60);
            this.gvProjectData.Name = "gvProjectData";
            this.gvProjectData.RowTemplate.Height = 24;
            this.gvProjectData.Size = new System.Drawing.Size(964, 236);
            this.gvProjectData.TabIndex = 2;
            this.gvProjectData.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gvProjectData_CellMouseClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnProjPortfolio);
            this.groupBox1.Controls.Add(this.btnProjDashboard);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(13, 302);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(961, 65);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Generate";
            // 
            // btnProjPortfolio
            // 
            this.btnProjPortfolio.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProjPortfolio.Location = new System.Drawing.Point(206, 23);
            this.btnProjPortfolio.Name = "btnProjPortfolio";
            this.btnProjPortfolio.Size = new System.Drawing.Size(160, 27);
            this.btnProjPortfolio.TabIndex = 1;
            this.btnProjPortfolio.Text = "Project &Portfolio...";
            this.btnProjPortfolio.UseVisualStyleBackColor = true;
            this.btnProjPortfolio.Click += new System.EventHandler(this.btnProjPortfolio_Click);
            // 
            // btnProjDashboard
            // 
            this.btnProjDashboard.AutoEllipsis = true;
            this.btnProjDashboard.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProjDashboard.Location = new System.Drawing.Point(7, 23);
            this.btnProjDashboard.Name = "btnProjDashboard";
            this.btnProjDashboard.Size = new System.Drawing.Size(172, 27);
            this.btnProjDashboard.TabIndex = 0;
            this.btnProjDashboard.Text = "Project &Dashboard...";
            this.btnProjDashboard.UseVisualStyleBackColor = true;
            this.btnProjDashboard.Click += new System.EventHandler(this.btnProjDashboard_Click);
            // 
            // UcProjectTracker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gvProjectData);
            this.Controls.Add(this.btnEditProject);
            this.Controls.Add(this.btnAddProject);
            this.Name = "UcProjectTracker";
            this.Size = new System.Drawing.Size(996, 387);
            ((System.ComponentModel.ISupportInitialize)(this.gvProjectData)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddProject;
        private System.Windows.Forms.Button btnEditProject;
        private System.Windows.Forms.DataGridView gvProjectData;
        //private System.Windows.Forms.BindingSource bindingSource_main;

        //private Zuby.ADGV.AdvancedDataGridView advancedDataGridView;
        //private Zuby.ADGV.AdvancedDataGridViewSearchToolBar advancedDataGridViewSearchToolBar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnProjPortfolio;
        private System.Windows.Forms.Button btnProjDashboard;
    }
}
