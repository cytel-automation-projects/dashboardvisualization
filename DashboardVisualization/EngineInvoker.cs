﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DashboardVisualization.Models
{
    public class EngineInvoker
    {
        /// <summary>
        /// This function return true if existing file is valid
        /// Validation 1: check if selected file is not blank 
        /// Validation 2: Check existing workspace columns check validation
        /// </summary>
        /// <param name="filepath">selected existing workspace file path</param>
        /// <returns></returns>
        public static bool LoadWorkspace(string filepath)
        {
            int rowCount = 0;
            int minRowCountInNonBlankWorkspace = 2;
            string TempltCopyDataRow_1 = "4";//, TempltCopyDataRow_2 = "5"; Commented by Chetna as header row has changed to single row
            int templtFormulaRw_1 = 6; //, templtFormulaRw_2 = 7; Commented by Chetna as header row has changed to single row
            bool inputsValid = true;
            string outputWorkbookPath = string.Empty;

            Microsoft.Office.Interop.Excel.Application ExcelApp = new Microsoft.Office.Interop.Excel.Application();       
            Workbook ExcelWorkBook = ExcelApp.Workbooks.Open(filepath);
            Workbook ExcelWorkBook_2 = ExcelApp.Workbooks.Open(Constants.WorkSpaceTemplatePath);
            object misValue = System.Reflection.Missing.Value;            

            try
            {
                ExcelApp.DisplayAlerts = false;
                ExcelApp.Visible = false;
                ExcelApp.ActiveWindow.WindowState = XlWindowState.xlMinimized;

                //Get the active worksheet object of sheet project data
                Worksheet excelSheet = ExcelWorkBook.Worksheets[Constants.ShtProjData];

                //This will return the whole range where you data is used in it
                //Get the used Range of 
                Range usedRange1 = excelSheet.UsedRange;
                rowCount = usedRange1.Rows.Count;
                int ColCnt = usedRange1.Columns.Count;

                //*********Validation 1: check if selected file is not blank 
                //----------------------------------------------------------------
                
                //if (2 <= rowCount)
                //{
                /*Commented by chetna - 12-Jan-22 ( Unable to open workspace as it has only header row - 
                 * commented below single line to allow the workspace to be open and add the project details  
                 by commeting the below line does not impact on file validation check of empty workspace i.e. no header no data*/
                
                    ////inputsValid = (rowCount >= minRowCountInNonBlankWorkspace);
                //}
                //else
                //{
                //    inputsValid = false;
                //}

                //*********Validation 2: Check existing workspace column check validation
                //---------------------------------------------------
                
                //assign sheet to variable 
                Worksheet excelSheetTmplt = ExcelWorkBook_2.Worksheets[Constants.ShtTemplate];
                //This will return the whole range where you data is used in it 
                Range usedRange_2 = excelSheetTmplt.UsedRange;

                int templteLstRw = usedRange_2.Rows.Count;
                int templtLstCol = usedRange_2.Columns.Count;

                //set range of existing opened workbook
                Microsoft.Office.Interop.Excel.Range from = excelSheet.Range[excelSheet.Cells[1, 1], excelSheet.Cells[1, ColCnt]].Cells;
                //paste data from 4 and 5 row
                Microsoft.Office.Interop.Excel.Range to = excelSheetTmplt.Range["A" + TempltCopyDataRow_1];

                //copy header in template file to compare
                from.Copy(to);

                //apply formula on 6 and 7 row
                //excelSheetTmplt.Cells[6, 1].Value = "=A1=A" + "4";
                excelSheetTmplt.Range[excelSheetTmplt.Cells[templtFormulaRw_1, 1], excelSheetTmplt.Cells[templtFormulaRw_1, templtLstCol]].Value = "=A1=A" + TempltCopyDataRow_1;
                //Commented by Chetna as header row has changed to single row
                //excelSheetTmplt.Range[excelSheetTmplt.Cells[templtFormulaRw_2, 1], excelSheetTmplt.Cells[templtFormulaRw_2, templtLstCol]].Value = "=A2=A" + TempltCopyDataRow_2;

                //set serach range where formula has applied
                Microsoft.Office.Interop.Excel.Range searchedRange = excelSheetTmplt.Range[excelSheetTmplt.Cells[templtFormulaRw_1, 1], excelSheetTmplt.Cells[templtFormulaRw_1, templtLstCol]].Cells;
                // search searchString in the range, if find result, return a range
                Microsoft.Office.Interop.Excel.Range currentFind = searchedRange.Find(What: "FALSE", LookIn: Microsoft.Office.Interop.Excel.XlFindLookIn.xlValues, LookAt: Microsoft.Office.Interop.Excel.XlLookAt.xlPart,
                                SearchOrder: Microsoft.Office.Interop.Excel.XlSearchOrder.xlByRows, SearchDirection: Microsoft.Office.Interop.Excel.XlSearchDirection.xlNext);                
                
                //Microsoft.Office.Interop.Excel.Range currentFind = searchedRange.Find("FALSE");

                //if false found then some columns are not matching
                if (currentFind != null)
                {
                    inputsValid = false;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                ExcelWorkBook.Close(false, misValue, misValue);
                ExcelWorkBook_2.Close(false, misValue, misValue);
                ExcelApp.Quit();
                if (ExcelWorkBook != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelWorkBook); }
                if (ExcelWorkBook_2 != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelWorkBook_2); }
                if (ExcelApp != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelApp); }
            }
            return inputsValid;
        }


        /// <summary>
        /// Run Excel VBA code - Generate Graph
        /// </summary>
        /// <param name="selectedProjectIDs"></param>
        /// <param name="renderPortfolioRequested"></param>
        /// <returns></returns>
        public static string RunTemplate(string[] selectedProjectIDs, bool renderPortfolioRequested = false)
        {
            Microsoft.Office.Interop.Excel.Application ExcelApp = new Microsoft.Office.Interop.Excel.Application();
            Workbook ExcelWorkBook = ExcelApp.Workbooks.Open(Constants.EngineFolderPath);
            object misValue = System.Reflection.Missing.Value;
            string outputWorkbookPath = string.Empty;

            try
            {
                ExcelApp.DisplayAlerts = false;
                ExcelApp.ScreenUpdating = false;
                ExcelApp.Visible = false;
                ExcelWorkBook = ExcelApp.Workbooks.Open(Constants.EngineFolderPath);
                ExcelApp.ActiveWindow.WindowState = XlWindowState.xlMinimized;

                int fileIndex = -1;
                List<string> inputFiles = new List<string>();

                inputFiles = new List<string>() { Constants.WorkSpaceFilePath };

                if (CheckFileAvailability(inputFiles, out fileIndex))
                {
                    outputWorkbookPath = ExcelApp.Run("GenerateGraph", Constants.WorkSpaceFilePath, selectedProjectIDs, renderPortfolioRequested);
                }
                else
                {
                    MessageBox.Show("Path missing for required file: " + inputFiles[fileIndex], Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                return outputWorkbookPath;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return outputWorkbookPath;
            }
            finally
            {
                ExcelWorkBook.Close(false, misValue, misValue);
                ExcelApp.Quit();
                if (ExcelWorkBook != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelWorkBook); }
                if (ExcelApp != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelApp); }
            }
        }

        private static bool CheckFileAvailability(List<string> inputFiles, out int fileIndex)
        {
            fileIndex = -1;

            if (inputFiles.Count == 0) return true;

            int index = 0;
            foreach (var file in inputFiles)
            {
                if (string.IsNullOrWhiteSpace(file))
                {
                    fileIndex = index;
                    return false;
                }

                index++;
            }
            return true;
        }
    }
}
