﻿namespace DashboardVisualization
{
    partial class FrmProjTracker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblWorkSpceName = new System.Windows.Forms.Label();
            this.ucProjectTracker = new DashboardVisualization.UserControls.UcProjectTracker();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(752, 374);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(102, 31);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "E&xit";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(626, 374);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(102, 31);
            this.btnOk.TabIndex = 12;
            this.btnOk.Text = "Back to &Login";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 387);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 14);
            this.label1.TabIndex = 14;
            this.label1.Text = "Current Workspace: ";
            // 
            // lblWorkSpceName
            // 
            this.lblWorkSpceName.AutoSize = true;
            this.lblWorkSpceName.Location = new System.Drawing.Point(155, 387);
            this.lblWorkSpceName.Name = "lblWorkSpceName";
            this.lblWorkSpceName.Size = new System.Drawing.Size(45, 14);
            this.lblWorkSpceName.TabIndex = 15;
            this.lblWorkSpceName.Text = "label2";
            // 
            // ucProjectTracker
            // 
            this.ucProjectTracker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucProjectTracker.Location = new System.Drawing.Point(0, 0);
            this.ucProjectTracker.Margin = new System.Windows.Forms.Padding(2);
            this.ucProjectTracker.Name = "ucProjectTracker";
            this.ucProjectTracker.Size = new System.Drawing.Size(868, 388);
            this.ucProjectTracker.TabIndex = 0;
            // 
            // FrmProjTracker
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(862, 410);
            this.Controls.Add(this.lblWorkSpceName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.ucProjectTracker);
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(878, 449);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(878, 449);
            this.Name = "FrmProjTracker";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dashboard Visulization > Project Tracker";
            this.Load += new System.EventHandler(this.FrmProjTracker_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UserControls.UcProjectTracker ucProjectTracker;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblWorkSpceName;

    }
}

