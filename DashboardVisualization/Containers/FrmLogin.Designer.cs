﻿namespace DashboardVisualization.Containers
{
    partial class FrmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancelLogin = new System.Windows.Forms.Button();
            this.btnOkLogin = new System.Windows.Forms.Button();
            this.ucLogin1 = new DashboardVisualization.UserControls.UcLogin();
            this.SuspendLayout();
            // 
            // btnCancelLogin
            // 
            this.btnCancelLogin.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelLogin.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelLogin.Location = new System.Drawing.Point(360, 236);
            this.btnCancelLogin.Name = "btnCancelLogin";
            this.btnCancelLogin.Size = new System.Drawing.Size(84, 30);
            this.btnCancelLogin.TabIndex = 17;
            this.btnCancelLogin.Text = "E&xit";
            this.btnCancelLogin.UseVisualStyleBackColor = true;
            this.btnCancelLogin.Click += new System.EventHandler(this.btnCancelLogin_Click);
            // 
            // btnOkLogin
            // 
            this.btnOkLogin.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOkLogin.Location = new System.Drawing.Point(248, 236);
            this.btnOkLogin.Name = "btnOkLogin";
            this.btnOkLogin.Size = new System.Drawing.Size(88, 30);
            this.btnOkLogin.TabIndex = 16;
            this.btnOkLogin.Text = "&Proceed";
            this.btnOkLogin.UseVisualStyleBackColor = true;
            this.btnOkLogin.Click += new System.EventHandler(this.btnOkLogin_Click);
            // 
            // ucLogin1
            // 
            this.ucLogin1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucLogin1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucLogin1.Location = new System.Drawing.Point(0, 0);
            this.ucLogin1.Name = "ucLogin1";
            this.ucLogin1.Size = new System.Drawing.Size(456, 275);
            this.ucLogin1.TabIndex = 0;
            // 
            // FrmLogin
            // 
            this.AcceptButton = this.btnOkLogin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this.btnCancelLogin;
            this.ClientSize = new System.Drawing.Size(456, 275);
            this.Controls.Add(this.btnCancelLogin);
            this.Controls.Add(this.btnOkLogin);
            this.Controls.Add(this.ucLogin1);
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmLogin";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dashboard Visualization";
            this.ResumeLayout(false);

        }

        #endregion

        private UserControls.UcLogin ucLogin1;
        private System.Windows.Forms.Button btnCancelLogin;
        private System.Windows.Forms.Button btnOkLogin;
    }
}