﻿using DashboardVisualization.Models;
using System;
using System.Windows.Forms;

namespace DashboardVisualization.Containers
{
    public partial class FrmMain : Form
    {
        public bool EditMode;

        private void SetForm()
        {
            // use the data received here for setting up values for UI elements in this form
            //ucMain1.SetForm(_projectData);
        }


        public FrmMain(bool editMode = false, ProjectDetails projectData = null)
        {
            InitializeComponent();

            EditMode = editMode;
            
            // set form title based on whether it is displayed for adding a new project or for editing an existing one.
            Text = editMode ? "Dashboard Visulization > Edit Project" : "Dashboard Visulization > Add Project";
            
            if (editMode == false)
            {
                // set all datepicker controls to today's date
                ucMain1.SetDefaultDates(true);

                //set default no of days 5 for each activity
                ucMain1.SetDefaultDay();

                Constants.ProjectDetails = null;
            }

            if (editMode && projectData != null)
            {
                Constants.flgDtPicker = false;
                ucMain1.SetProjectDetails(projectData);                
            }

            Constants.flgDtPicker = true;
        }   
        
        private void btnOkMain_Click(object sender, EventArgs e)
        {
            this.AcceptButton = null;

            Constants.ValidFormData = true;

            if (ucMain1.ValidateInputs() == false)
            {
                //to avoid error added flag "object reference not set to an instance"
                Constants.ValidFormData = false;
                //return;
            }
            else
            {
                btnOkMain.DialogResult = DialogResult.OK;
                this.AcceptButton = this.btnOkMain;

                Constants.ProjectDetails = ucMain1.CollectData();
                return;
            }
            //this.Dispose();
            //this.Hide();
        }

        private void btnCancelMain_Click(object sender, EventArgs e)
        {
            Constants.ProjectDetails = null;
            this.Dispose();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            //SetDefaulltDate(this);
        }

    }
}
