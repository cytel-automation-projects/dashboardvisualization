﻿namespace DashboardVisualization.Containers
{
    partial class FrmShowHideCols
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chkDMAwarded = new System.Windows.Forms.CheckBox();
            this.chkCDISCAwarded = new System.Windows.Forms.CheckBox();
            this.btnShowHideOk = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.chkStatsAwarded = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkStatsPoC = new System.Windows.Forms.CheckBox();
            this.chkCDISCPoC = new System.Windows.Forms.CheckBox();
            this.chkDMPoC = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.chkActualFPFV = new System.Windows.Forms.CheckBox();
            this.chkActualGoLive = new System.Windows.Forms.CheckBox();
            this.chkPlanFPFV = new System.Windows.Forms.CheckBox();
            this.chkPlanGoLive = new System.Windows.Forms.CheckBox();
            this.chkActualCDISC = new System.Windows.Forms.CheckBox();
            this.chkActualDBL = new System.Windows.Forms.CheckBox();
            this.chkActualLPLV = new System.Windows.Forms.CheckBox();
            this.chkPlanCDISC = new System.Windows.Forms.CheckBox();
            this.chkPlanDBL = new System.Windows.Forms.CheckBox();
            this.chkPlanLPLV = new System.Windows.Forms.CheckBox();
            this.chkActualSAR = new System.Windows.Forms.CheckBox();
            this.chkActualBDR = new System.Windows.Forms.CheckBox();
            this.chkActualSAP = new System.Windows.Forms.CheckBox();
            this.chkPlanSAR = new System.Windows.Forms.CheckBox();
            this.chkPlanBDR = new System.Windows.Forms.CheckBox();
            this.chkPlanSAP = new System.Windows.Forms.CheckBox();
            this.chkCytelComment = new System.Windows.Forms.CheckBox();
            this.chkClientComment = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblPlannedSAR = new System.Windows.Forms.Label();
            this.lblPlannedDBL = new System.Windows.Forms.Label();
            this.lblPlannedBDR = new System.Windows.Forms.Label();
            this.lblPlannedSAP = new System.Windows.Forms.Label();
            this.lblPlannedCDISC = new System.Windows.Forms.Label();
            this.lblPlannedGoLive = new System.Windows.Forms.Label();
            this.lblPlannedLPLV = new System.Windows.Forms.Label();
            this.lblPlannedFPFV = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(291, 14);
            this.label2.TabIndex = 52;
            this.label2.Text = "Select to show column and unselect to hide";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "Data Management";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 14);
            this.label4.TabIndex = 3;
            this.label4.Text = "CDISC";
            // 
            // chkDMAwarded
            // 
            this.chkDMAwarded.AutoSize = true;
            this.chkDMAwarded.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDMAwarded.Location = new System.Drawing.Point(160, 62);
            this.chkDMAwarded.Name = "chkDMAwarded";
            this.chkDMAwarded.Size = new System.Drawing.Size(15, 14);
            this.chkDMAwarded.TabIndex = 1;
            this.chkDMAwarded.UseVisualStyleBackColor = true;
            // 
            // chkCDISCAwarded
            // 
            this.chkCDISCAwarded.AutoSize = true;
            this.chkCDISCAwarded.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCDISCAwarded.Location = new System.Drawing.Point(160, 87);
            this.chkCDISCAwarded.Name = "chkCDISCAwarded";
            this.chkCDISCAwarded.Size = new System.Drawing.Size(15, 14);
            this.chkCDISCAwarded.TabIndex = 4;
            this.chkCDISCAwarded.UseVisualStyleBackColor = true;
            // 
            // btnShowHideOk
            // 
            this.btnShowHideOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnShowHideOk.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShowHideOk.Location = new System.Drawing.Point(142, 491);
            this.btnShowHideOk.Name = "btnShowHideOk";
            this.btnShowHideOk.Size = new System.Drawing.Size(87, 27);
            this.btnShowHideOk.TabIndex = 40;
            this.btnShowHideOk.Text = "OK";
            this.btnShowHideOk.UseVisualStyleBackColor = true;
            this.btnShowHideOk.Click += new System.EventHandler(this.btnShowHideOk_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(248, 491);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(87, 27);
            this.button2.TabIndex = 41;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // chkStatsAwarded
            // 
            this.chkStatsAwarded.AutoSize = true;
            this.chkStatsAwarded.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkStatsAwarded.Location = new System.Drawing.Point(160, 114);
            this.chkStatsAwarded.Name = "chkStatsAwarded";
            this.chkStatsAwarded.Size = new System.Drawing.Size(15, 14);
            this.chkStatsAwarded.TabIndex = 7;
            this.chkStatsAwarded.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 14);
            this.label1.TabIndex = 6;
            this.label1.Text = "Statistics";
            // 
            // chkStatsPoC
            // 
            this.chkStatsPoC.AutoSize = true;
            this.chkStatsPoC.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkStatsPoC.Location = new System.Drawing.Point(234, 114);
            this.chkStatsPoC.Name = "chkStatsPoC";
            this.chkStatsPoC.Size = new System.Drawing.Size(15, 14);
            this.chkStatsPoC.TabIndex = 8;
            this.chkStatsPoC.UseVisualStyleBackColor = true;
            // 
            // chkCDISCPoC
            // 
            this.chkCDISCPoC.AutoSize = true;
            this.chkCDISCPoC.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCDISCPoC.Location = new System.Drawing.Point(234, 87);
            this.chkCDISCPoC.Name = "chkCDISCPoC";
            this.chkCDISCPoC.Size = new System.Drawing.Size(15, 14);
            this.chkCDISCPoC.TabIndex = 5;
            this.chkCDISCPoC.UseVisualStyleBackColor = true;
            // 
            // chkDMPoC
            // 
            this.chkDMPoC.AutoSize = true;
            this.chkDMPoC.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDMPoC.Location = new System.Drawing.Point(234, 62);
            this.chkDMPoC.Name = "chkDMPoC";
            this.chkDMPoC.Size = new System.Drawing.Size(15, 14);
            this.chkDMPoC.TabIndex = 2;
            this.chkDMPoC.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(140, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 14);
            this.label5.TabIndex = 50;
            this.label5.Text = "Awarded";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(226, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 14);
            this.label6.TabIndex = 51;
            this.label6.Text = "PoC";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(221, 161);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 14);
            this.label7.TabIndex = 54;
            this.label7.Text = "Actual";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(141, 160);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 14);
            this.label8.TabIndex = 53;
            this.label8.Text = "Planned";
            // 
            // chkActualFPFV
            // 
            this.chkActualFPFV.AutoSize = true;
            this.chkActualFPFV.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkActualFPFV.Location = new System.Drawing.Point(234, 213);
            this.chkActualFPFV.Name = "chkActualFPFV";
            this.chkActualFPFV.Size = new System.Drawing.Size(15, 14);
            this.chkActualFPFV.TabIndex = 14;
            this.chkActualFPFV.UseVisualStyleBackColor = true;
            // 
            // chkActualGoLive
            // 
            this.chkActualGoLive.AutoSize = true;
            this.chkActualGoLive.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkActualGoLive.Location = new System.Drawing.Point(234, 188);
            this.chkActualGoLive.Name = "chkActualGoLive";
            this.chkActualGoLive.Size = new System.Drawing.Size(15, 14);
            this.chkActualGoLive.TabIndex = 11;
            this.chkActualGoLive.UseVisualStyleBackColor = true;
            // 
            // chkPlanFPFV
            // 
            this.chkPlanFPFV.AutoSize = true;
            this.chkPlanFPFV.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPlanFPFV.Location = new System.Drawing.Point(160, 213);
            this.chkPlanFPFV.Name = "chkPlanFPFV";
            this.chkPlanFPFV.Size = new System.Drawing.Size(15, 14);
            this.chkPlanFPFV.TabIndex = 13;
            this.chkPlanFPFV.UseVisualStyleBackColor = true;
            // 
            // chkPlanGoLive
            // 
            this.chkPlanGoLive.AutoSize = true;
            this.chkPlanGoLive.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPlanGoLive.Location = new System.Drawing.Point(160, 188);
            this.chkPlanGoLive.Name = "chkPlanGoLive";
            this.chkPlanGoLive.Size = new System.Drawing.Size(15, 14);
            this.chkPlanGoLive.TabIndex = 10;
            this.chkPlanGoLive.UseVisualStyleBackColor = true;
            // 
            // chkActualCDISC
            // 
            this.chkActualCDISC.AutoSize = true;
            this.chkActualCDISC.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkActualCDISC.Location = new System.Drawing.Point(234, 292);
            this.chkActualCDISC.Name = "chkActualCDISC";
            this.chkActualCDISC.Size = new System.Drawing.Size(15, 14);
            this.chkActualCDISC.TabIndex = 26;
            this.chkActualCDISC.UseVisualStyleBackColor = true;
            // 
            // chkActualDBL
            // 
            this.chkActualDBL.AutoSize = true;
            this.chkActualDBL.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkActualDBL.Location = new System.Drawing.Point(234, 265);
            this.chkActualDBL.Name = "chkActualDBL";
            this.chkActualDBL.Size = new System.Drawing.Size(15, 14);
            this.chkActualDBL.TabIndex = 23;
            this.chkActualDBL.UseVisualStyleBackColor = true;
            // 
            // chkActualLPLV
            // 
            this.chkActualLPLV.AutoSize = true;
            this.chkActualLPLV.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkActualLPLV.Location = new System.Drawing.Point(234, 240);
            this.chkActualLPLV.Name = "chkActualLPLV";
            this.chkActualLPLV.Size = new System.Drawing.Size(15, 14);
            this.chkActualLPLV.TabIndex = 20;
            this.chkActualLPLV.UseVisualStyleBackColor = true;
            // 
            // chkPlanCDISC
            // 
            this.chkPlanCDISC.AutoSize = true;
            this.chkPlanCDISC.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPlanCDISC.Location = new System.Drawing.Point(160, 292);
            this.chkPlanCDISC.Name = "chkPlanCDISC";
            this.chkPlanCDISC.Size = new System.Drawing.Size(15, 14);
            this.chkPlanCDISC.TabIndex = 25;
            this.chkPlanCDISC.UseVisualStyleBackColor = true;
            // 
            // chkPlanDBL
            // 
            this.chkPlanDBL.AutoSize = true;
            this.chkPlanDBL.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPlanDBL.Location = new System.Drawing.Point(160, 265);
            this.chkPlanDBL.Name = "chkPlanDBL";
            this.chkPlanDBL.Size = new System.Drawing.Size(15, 14);
            this.chkPlanDBL.TabIndex = 22;
            this.chkPlanDBL.UseVisualStyleBackColor = true;
            // 
            // chkPlanLPLV
            // 
            this.chkPlanLPLV.AutoSize = true;
            this.chkPlanLPLV.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPlanLPLV.Location = new System.Drawing.Point(160, 240);
            this.chkPlanLPLV.Name = "chkPlanLPLV";
            this.chkPlanLPLV.Size = new System.Drawing.Size(15, 14);
            this.chkPlanLPLV.TabIndex = 19;
            this.chkPlanLPLV.UseVisualStyleBackColor = true;
            // 
            // chkActualSAR
            // 
            this.chkActualSAR.AutoSize = true;
            this.chkActualSAR.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkActualSAR.Location = new System.Drawing.Point(234, 370);
            this.chkActualSAR.Name = "chkActualSAR";
            this.chkActualSAR.Size = new System.Drawing.Size(15, 14);
            this.chkActualSAR.TabIndex = 35;
            this.chkActualSAR.UseVisualStyleBackColor = true;
            // 
            // chkActualBDR
            // 
            this.chkActualBDR.AutoSize = true;
            this.chkActualBDR.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkActualBDR.Location = new System.Drawing.Point(234, 343);
            this.chkActualBDR.Name = "chkActualBDR";
            this.chkActualBDR.Size = new System.Drawing.Size(15, 14);
            this.chkActualBDR.TabIndex = 32;
            this.chkActualBDR.UseVisualStyleBackColor = true;
            // 
            // chkActualSAP
            // 
            this.chkActualSAP.AutoSize = true;
            this.chkActualSAP.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkActualSAP.Location = new System.Drawing.Point(234, 318);
            this.chkActualSAP.Name = "chkActualSAP";
            this.chkActualSAP.Size = new System.Drawing.Size(15, 14);
            this.chkActualSAP.TabIndex = 29;
            this.chkActualSAP.UseVisualStyleBackColor = true;
            // 
            // chkPlanSAR
            // 
            this.chkPlanSAR.AutoSize = true;
            this.chkPlanSAR.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPlanSAR.Location = new System.Drawing.Point(160, 370);
            this.chkPlanSAR.Name = "chkPlanSAR";
            this.chkPlanSAR.Size = new System.Drawing.Size(15, 14);
            this.chkPlanSAR.TabIndex = 34;
            this.chkPlanSAR.UseVisualStyleBackColor = true;
            // 
            // chkPlanBDR
            // 
            this.chkPlanBDR.AutoSize = true;
            this.chkPlanBDR.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPlanBDR.Location = new System.Drawing.Point(160, 343);
            this.chkPlanBDR.Name = "chkPlanBDR";
            this.chkPlanBDR.Size = new System.Drawing.Size(15, 14);
            this.chkPlanBDR.TabIndex = 31;
            this.chkPlanBDR.UseVisualStyleBackColor = true;
            // 
            // chkPlanSAP
            // 
            this.chkPlanSAP.AutoSize = true;
            this.chkPlanSAP.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPlanSAP.Location = new System.Drawing.Point(160, 318);
            this.chkPlanSAP.Name = "chkPlanSAP";
            this.chkPlanSAP.Size = new System.Drawing.Size(15, 14);
            this.chkPlanSAP.TabIndex = 28;
            this.chkPlanSAP.UseVisualStyleBackColor = true;
            // 
            // chkCytelComment
            // 
            this.chkCytelComment.AutoSize = true;
            this.chkCytelComment.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCytelComment.Location = new System.Drawing.Point(160, 453);
            this.chkCytelComment.Name = "chkCytelComment";
            this.chkCytelComment.Size = new System.Drawing.Size(15, 14);
            this.chkCytelComment.TabIndex = 39;
            this.chkCytelComment.UseVisualStyleBackColor = true;
            // 
            // chkClientComment
            // 
            this.chkClientComment.AutoSize = true;
            this.chkClientComment.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkClientComment.Location = new System.Drawing.Point(160, 419);
            this.chkClientComment.Name = "chkClientComment";
            this.chkClientComment.Size = new System.Drawing.Size(15, 14);
            this.chkClientComment.TabIndex = 37;
            this.chkClientComment.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(12, 418);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(114, 14);
            this.label18.TabIndex = 36;
            this.label18.Text = "Client Comments";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(12, 452);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(110, 14);
            this.label19.TabIndex = 38;
            this.label19.Text = "Cytel Comments";
            // 
            // lblPlannedSAR
            // 
            this.lblPlannedSAR.AutoSize = true;
            this.lblPlannedSAR.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedSAR.Location = new System.Drawing.Point(12, 369);
            this.lblPlannedSAR.Name = "lblPlannedSAR";
            this.lblPlannedSAR.Size = new System.Drawing.Size(31, 14);
            this.lblPlannedSAR.TabIndex = 33;
            this.lblPlannedSAR.Text = "SA&R";
            // 
            // lblPlannedDBL
            // 
            this.lblPlannedDBL.AutoSize = true;
            this.lblPlannedDBL.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedDBL.Location = new System.Drawing.Point(12, 264);
            this.lblPlannedDBL.Name = "lblPlannedDBL";
            this.lblPlannedDBL.Size = new System.Drawing.Size(31, 14);
            this.lblPlannedDBL.TabIndex = 21;
            this.lblPlannedDBL.Text = "&DBL";
            // 
            // lblPlannedBDR
            // 
            this.lblPlannedBDR.AutoSize = true;
            this.lblPlannedBDR.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedBDR.Location = new System.Drawing.Point(12, 342);
            this.lblPlannedBDR.Name = "lblPlannedBDR";
            this.lblPlannedBDR.Size = new System.Drawing.Size(32, 14);
            this.lblPlannedBDR.TabIndex = 30;
            this.lblPlannedBDR.Text = "&BDR";
            // 
            // lblPlannedSAP
            // 
            this.lblPlannedSAP.AutoSize = true;
            this.lblPlannedSAP.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedSAP.Location = new System.Drawing.Point(12, 317);
            this.lblPlannedSAP.Name = "lblPlannedSAP";
            this.lblPlannedSAP.Size = new System.Drawing.Size(31, 14);
            this.lblPlannedSAP.TabIndex = 27;
            this.lblPlannedSAP.Text = "S&AP";
            // 
            // lblPlannedCDISC
            // 
            this.lblPlannedCDISC.AutoSize = true;
            this.lblPlannedCDISC.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedCDISC.Location = new System.Drawing.Point(12, 291);
            this.lblPlannedCDISC.Name = "lblPlannedCDISC";
            this.lblPlannedCDISC.Size = new System.Drawing.Size(86, 14);
            this.lblPlannedCDISC.TabIndex = 24;
            this.lblPlannedCDISC.Text = "CD&ISC/SDTM";
            // 
            // lblPlannedGoLive
            // 
            this.lblPlannedGoLive.AutoSize = true;
            this.lblPlannedGoLive.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedGoLive.Location = new System.Drawing.Point(12, 187);
            this.lblPlannedGoLive.Name = "lblPlannedGoLive";
            this.lblPlannedGoLive.Size = new System.Drawing.Size(54, 14);
            this.lblPlannedGoLive.TabIndex = 9;
            this.lblPlannedGoLive.Text = "&Go-Live";
            // 
            // lblPlannedLPLV
            // 
            this.lblPlannedLPLV.AutoSize = true;
            this.lblPlannedLPLV.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedLPLV.Location = new System.Drawing.Point(12, 239);
            this.lblPlannedLPLV.Name = "lblPlannedLPLV";
            this.lblPlannedLPLV.Size = new System.Drawing.Size(36, 14);
            this.lblPlannedLPLV.TabIndex = 18;
            this.lblPlannedLPLV.Text = "L&PLV";
            // 
            // lblPlannedFPFV
            // 
            this.lblPlannedFPFV.AutoSize = true;
            this.lblPlannedFPFV.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlannedFPFV.Location = new System.Drawing.Point(12, 212);
            this.lblPlannedFPFV.Name = "lblPlannedFPFV";
            this.lblPlannedFPFV.Size = new System.Drawing.Size(37, 14);
            this.lblPlannedFPFV.TabIndex = 12;
            this.lblPlannedFPFV.Text = "&FPFV";
            // 
            // FrmShowHideCols
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 528);
            this.Controls.Add(this.lblPlannedSAR);
            this.Controls.Add(this.lblPlannedDBL);
            this.Controls.Add(this.lblPlannedBDR);
            this.Controls.Add(this.lblPlannedSAP);
            this.Controls.Add(this.lblPlannedCDISC);
            this.Controls.Add(this.lblPlannedGoLive);
            this.Controls.Add(this.lblPlannedLPLV);
            this.Controls.Add(this.lblPlannedFPFV);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.chkCytelComment);
            this.Controls.Add(this.chkClientComment);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.chkActualSAR);
            this.Controls.Add(this.chkActualBDR);
            this.Controls.Add(this.chkActualSAP);
            this.Controls.Add(this.chkPlanSAR);
            this.Controls.Add(this.chkPlanBDR);
            this.Controls.Add(this.chkPlanSAP);
            this.Controls.Add(this.chkActualCDISC);
            this.Controls.Add(this.chkActualDBL);
            this.Controls.Add(this.chkActualLPLV);
            this.Controls.Add(this.chkPlanCDISC);
            this.Controls.Add(this.chkPlanDBL);
            this.Controls.Add(this.chkPlanLPLV);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.chkActualFPFV);
            this.Controls.Add(this.chkActualGoLive);
            this.Controls.Add(this.chkPlanFPFV);
            this.Controls.Add(this.chkPlanGoLive);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chkStatsPoC);
            this.Controls.Add(this.chkCDISCPoC);
            this.Controls.Add(this.chkDMPoC);
            this.Controls.Add(this.chkStatsAwarded);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnShowHideOk);
            this.Controls.Add(this.chkCDISCAwarded);
            this.Controls.Add(this.chkDMAwarded);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(363, 567);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(363, 567);
            this.Name = "FrmShowHideCols";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Show/Hide Columns";
            this.Load += new System.EventHandler(this.FrmShowHideCols_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkDMAwarded;
        private System.Windows.Forms.CheckBox chkCDISCAwarded;
        private System.Windows.Forms.Button btnShowHideOk;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox chkStatsAwarded;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkStatsPoC;
        private System.Windows.Forms.CheckBox chkCDISCPoC;
        private System.Windows.Forms.CheckBox chkDMPoC;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        //private System.Windows.Forms.CheckBox chkActualLPFV;
        private System.Windows.Forms.CheckBox chkActualFPFV;
        private System.Windows.Forms.CheckBox chkActualGoLive;
        //private System.Windows.Forms.CheckBox chkPlanLPFV;
        private System.Windows.Forms.CheckBox chkPlanFPFV;
        private System.Windows.Forms.CheckBox chkPlanGoLive;
        private System.Windows.Forms.CheckBox chkActualCDISC;
        private System.Windows.Forms.CheckBox chkActualDBL;
        private System.Windows.Forms.CheckBox chkActualLPLV;
        private System.Windows.Forms.CheckBox chkPlanCDISC;
        private System.Windows.Forms.CheckBox chkPlanDBL;
        private System.Windows.Forms.CheckBox chkPlanLPLV;
        private System.Windows.Forms.CheckBox chkActualSAR;
        private System.Windows.Forms.CheckBox chkActualBDR;
        private System.Windows.Forms.CheckBox chkActualSAP;
        private System.Windows.Forms.CheckBox chkPlanSAR;
        private System.Windows.Forms.CheckBox chkPlanBDR;
        private System.Windows.Forms.CheckBox chkPlanSAP;
        private System.Windows.Forms.CheckBox chkCytelComment;
        private System.Windows.Forms.CheckBox chkClientComment;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblPlannedSAR;
        private System.Windows.Forms.Label lblPlannedDBL;
        private System.Windows.Forms.Label lblPlannedBDR;
        private System.Windows.Forms.Label lblPlannedSAP;
        private System.Windows.Forms.Label lblPlannedCDISC;
        private System.Windows.Forms.Label lblPlannedGoLive;
        private System.Windows.Forms.Label lblPlannedLPLV;
        //private System.Windows.Forms.Label lblPlannedLPFV;
        private System.Windows.Forms.Label lblPlannedFPFV;
    }
}