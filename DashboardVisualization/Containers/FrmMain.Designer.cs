﻿namespace DashboardVisualization.Containers
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancelMain = new System.Windows.Forms.Button();
            this.btnOkMain = new System.Windows.Forms.Button();
            this.ucMain1 = new DashboardVisualization.UserControls.UcMain();
            this.SuspendLayout();
            // 
            // btnCancelMain
            // 
            this.btnCancelMain.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelMain.Location = new System.Drawing.Point(575, 534);
            this.btnCancelMain.Name = "btnCancelMain";
            this.btnCancelMain.Size = new System.Drawing.Size(84, 30);
            this.btnCancelMain.TabIndex = 15;
            this.btnCancelMain.Text = "&Cancel";
            this.btnCancelMain.UseVisualStyleBackColor = true;
            this.btnCancelMain.Click += new System.EventHandler(this.btnCancelMain_Click);
            // 
            // btnOkMain
            // 
            this.btnOkMain.Location = new System.Drawing.Point(479, 534);
            this.btnOkMain.Name = "btnOkMain";
            this.btnOkMain.Size = new System.Drawing.Size(78, 30);
            this.btnOkMain.TabIndex = 14;
            this.btnOkMain.Text = "&OK";
            this.btnOkMain.UseVisualStyleBackColor = true;
            this.btnOkMain.Click += new System.EventHandler(this.btnOkMain_Click);
            // 
            // ucMain1
            // 
            this.ucMain1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucMain1.Location = new System.Drawing.Point(-1, 1);
            this.ucMain1.Name = "ucMain1";
            this.ucMain1.Size = new System.Drawing.Size(662, 524);
            this.ucMain1.TabIndex = 0;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this.btnCancelMain;
            this.ClientSize = new System.Drawing.Size(669, 573);
            this.Controls.Add(this.btnCancelMain);
            this.Controls.Add(this.btnOkMain);
            this.Controls.Add(this.ucMain1);
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMain";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmMain";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancelMain;
        private System.Windows.Forms.Button btnOkMain;
        private UserControls.UcMain ucMain1;

    }
}