﻿using DashboardVisualization.Models;
using DashboardVisualization.UserControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace DashboardVisualization.Containers
{
    public partial class FrmLogin : Form
    {
        
        public FrmLogin()
        {
            InitializeComponent();
        }

        

        private void btnOkLogin_Click(object sender, EventArgs e)
        {
            //string desktopPath = Process.GetCurrentProcess().MainModule.FileName.Substring(0, Process.GetCurrentProcess().MainModule.FileName.LastIndexOf("\\"));

            //string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            //string shortcutfile = desktopPath + "\\Dashboard Visualization Launcher.lnk";

            //MessageBox.Show(desktopPath +"\n\n" + Constants.CurrentDirPath);
            
            if (ucLogin1.LoadWorkspace())
            {
                this.Hide();

                //open projectTracker form
                FrmProjTracker _track = new FrmProjTracker();
                _track.ShowDialog();

                this.Dispose();
            }
           
        }

        private void btnCancelLogin_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        
      
    }
}
