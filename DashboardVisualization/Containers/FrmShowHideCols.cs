﻿using DashboardVisualization.Models;
using System.Windows.Forms;

namespace DashboardVisualization.Containers
{
    public partial class FrmShowHideCols : Form
    {
        public FrmShowHideCols()
        {
            InitializeComponent();
        }

        private void FrmShowHideCols_Load(object sender, System.EventArgs e)
        {
            foreach (var control in Controls)
            {
                if(control is CheckBox)
                {
                    ((CheckBox)control).Checked = true;
                }
            }
        }

        /// <summary>
        /// set check box value to grid columns  for visibility of grid columns
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnShowHideOk_Click(object sender, System.EventArgs e)
        {
            Constants.ColumnVisibilityStates[Constants.GridColumns.DMAwarded] = chkDMAwarded.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.DMPOC] = chkDMPoC.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.CDISC_Awarded] = chkCDISCAwarded.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.CDISC_POC] = chkCDISCPoC.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.Stat_Awarded] = chkStatsAwarded.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.Stat_POC] = chkStatsPoC.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.GoLivePlanned] = chkPlanGoLive.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.GoLiveActual] = chkActualGoLive.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.FPFVPlanned] = chkPlanFPFV.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.FPFVActual] = chkActualFPFV.Checked;
            //Constants.ColumnVisibilityStates[Constants.GridColumns.LPFVPlanned] = chkPlanLPFV.Checked;
            //Constants.ColumnVisibilityStates[Constants.GridColumns.LPFVActual] = chkActualLPFV.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.LPLVPlanned] = chkPlanLPLV.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.LPLVActual] = chkActualLPLV.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.DBLPlanned] = chkPlanDBL.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.DBLActual] = chkActualDBL.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.CDISCPlanned] = chkPlanCDISC.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.CDISCActual] = chkActualCDISC.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.SAPPlanned] = chkPlanSAP.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.SAPActual] = chkActualSAP.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.BDRPlanned] = chkPlanBDR.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.BDRActual] = chkActualBDR.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.SARPlanned] = chkPlanSAR.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.SARActual] = chkActualSAR.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.Client_Comments] = chkCytelComment.Checked;
            Constants.ColumnVisibilityStates[Constants.GridColumns.Cytel_Comments] = chkClientComment.Checked;
        }
    }
}
