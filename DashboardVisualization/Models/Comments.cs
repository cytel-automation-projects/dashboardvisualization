﻿namespace DashboardVisualization.Models
{
    public class Comments
    {
        private string _clientComments;

        public string ClientComments
        {
            get { return _clientComments; }
            set { _clientComments = value; }
        }

        private string _cytelComments;

        public string CytelComments 
        {
            get { return _cytelComments; }
            set { _cytelComments = value; }
        }

        public Comments()
        {

        }

        public Comments(string clientComments, string cytelComments)
        {
            _clientComments = clientComments;
            _cytelComments = cytelComments;
        }
    }
}
