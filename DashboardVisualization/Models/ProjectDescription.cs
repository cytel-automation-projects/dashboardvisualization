﻿namespace DashboardVisualization.Models
{
    public class ProjectDescription
    {
        private string projectName;

        public string ProjectName
        {
            get { return projectName; }
            set { projectName = value; }
        }

        private string projectCategory;

        public string ProjectCategory
        {
            get { return projectCategory; }
            set { projectCategory = value; }
        }

        private string otherCategory;
        public string OtherCategory
        {
            get { return otherCategory; }
            set { otherCategory = value; }
        }

        private string projectDescription;

        public string Description
        {
            get { return projectDescription; }
            set { projectDescription = value; }
        }

        //private string studyDuration;

        //public string StudyDuration
        //{
        //    get { return studyDuration; }
        //    set { studyDuration = value; }
        //}

        //private int studyDurationUnit;

        //public int StudyDurationUnit
        //{
        //    get { return studyDurationUnit; }
        //    set { studyDurationUnit = value; }
        //}

        //private string cssName;

        //public string CSSName
        //{
        //    get { return cssName; }
        //    set { cssName = value; }
        //}

        public ProjectDescription()
        {

        }

        public ProjectDescription(string name, string category, string description /*, string duration, Constants.StudyDurationUnit durationUnit*/, string otherCategory = null)//, string css)
        {
            projectName = name;
            projectCategory = category;
            OtherCategory=otherCategory;
            projectDescription = description;
            //studyDuration = duration;
            //studyDurationUnit = (int)durationUnit;
            //cssName = css;
        }
    }
}
