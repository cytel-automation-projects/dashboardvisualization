﻿using System;

namespace DashboardVisualization.Models
{
    public class StudyInfo
    {
        private DateTime studyAssignedOnDate;

        public DateTime StudyAssignedOnDate
        {
            get { return studyAssignedOnDate; }
            set { studyAssignedOnDate = value; }
        }

        private string cslName;

        public string CSLName
        {
            get { return cslName; }
            set { cslName = value; }
        }

        private string gclName;

        public string GCLName
        {
            get { return gclName; }
            set { gclName = value; }
        }

        private string cssName;

        public string CSSName
        {
            get { return cssName; }
            set { cssName = value; }
        }

        private string cro;

        public string Cro
        {
            get { return cro; }
            set { cro = value; }
        }

        private string country;

        public string Country
        {
            get { return country; }
            set { country = value; }
        }

        private string otherCRO;
        public string OtherCRO 
        { 
            get { return otherCRO; }
            set { otherCRO = value; } 
        }

        private string otherCountry;
        public string OtherCountry
        {
            get { return otherCountry; }
            set { otherCountry = value; }
        }

        private bool dataManagementAwarded;

        public bool DataManagementAwarded
        {
            get { return dataManagementAwarded; }
            set { dataManagementAwarded = value; }
        }

        private string dataManagementPoC;

        public string DataManagementPoCName
        {
            get { return dataManagementPoC; }
            set { dataManagementPoC = value; }
        }

        private bool cdiscAwarded;

        public bool CDISCAwarded
        {
            get { return cdiscAwarded; }
            set { cdiscAwarded = value; }
        }

        private string cdiscPoC;

        public string CDISCPoCName
        {
            get { return cdiscPoC; }
            set { cdiscPoC = value; }
        }

        private bool statisticsAwarded;

        public bool StatisticsAwarded
        {
            get { return statisticsAwarded; }
            set { statisticsAwarded = value; }
        }

        private string statisticsPoC;

        public string StatisticsPoCName
        {
            get { return statisticsPoC; }
            set { statisticsPoC = value; }
        }

        public StudyInfo()
        {

        }

        public StudyInfo(DateTime studyAssignedOnDate, string csl, string gcl, string css, string cr, string contry, string dataMgmtPoCName, string cdiscPoCName,string statisticsPoCName, string otherCro = null, string otherCntry = null)
        {
            this.studyAssignedOnDate = studyAssignedOnDate;

            cslName = csl;
            gclName = gcl;
            cssName = css;

            cro = cr;
            country = contry;

            otherCRO = otherCro;
            otherCountry = otherCntry;

            dataManagementAwarded = string.IsNullOrWhiteSpace(dataMgmtPoCName).Equals(false);
            dataManagementPoC = dataMgmtPoCName;

            cdiscAwarded = string.IsNullOrWhiteSpace(cdiscPoCName).Equals(false);
            cdiscPoC = cdiscPoCName;

            statisticsAwarded = string.IsNullOrWhiteSpace(statisticsPoCName).Equals(false);
            statisticsPoC = statisticsPoCName;
        }
    }
}
