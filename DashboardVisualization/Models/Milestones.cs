﻿using System;

namespace DashboardVisualization.Models
{
    public class Milestones
    {
        private string studyStatus;

        public string StudyStatus
        {
            get { return studyStatus; }
            set { studyStatus = value; }
        }

        private DateTime plannedGoLive;

        public DateTime PlannedGoLive
        {
            get { return plannedGoLive; }
            set { plannedGoLive = value; }
        }

        
        private DateTime actualGoLive;

        public DateTime ActualGoLive
        {
            get { return actualGoLive; }
            set { actualGoLive = value; }
        }

        
        private DateTime plannedFPFV;

        public DateTime PlannedFPFV
        {
            get { return plannedFPFV; }
            set { plannedFPFV = value; }
        }

        private DateTime actualFPFV;

        public DateTime ActualFPFV
        {
            get { return actualFPFV; }
            set { actualFPFV = value; }
        }

        //private DateTime plannedLPFV;

        //public DateTime PlannedLPFV
        //{
        //    get { return plannedLPFV; }
        //    set { plannedLPFV = value; }
        //}

        //private DateTime actualLPFV;

        //public DateTime ActualLPFV
        //{
        //    get { return actualLPFV; }
        //    set { actualLPFV = value; }
        //}

        private DateTime plannedLPLV;

        public DateTime PlannedLPLV
        {
            get { return plannedLPLV; }
            set { plannedLPLV = value; }
        }

        private DateTime actualLPLV;

        public DateTime ActualLPLV
        {
            get { return actualLPLV; }
            set { actualLPLV = value; }
        }

        private DateTime plannedDBL;

        public DateTime PlannedDBL
        {
            get { return plannedDBL; }
            set { plannedDBL = value; }
        }

        private DateTime actualDBL;

        public DateTime ActualDBL
        {
            get { return actualDBL; }
            set { actualDBL = value; }
        }

        private DateTime plannedCDISCSDTM;

        public DateTime PlannedCDISCSDTM
        {
            get { return plannedCDISCSDTM; }
            set { plannedCDISCSDTM = value; }
        }

        private DateTime actualCDISCSDTM;

        public DateTime ActualCDISCSDTM
        {
            get { return actualCDISCSDTM; }
            set { actualCDISCSDTM = value; }
        }

        private DateTime plannedSAP;

        public DateTime PlannedSAP
        {
            get { return plannedSAP; }
            set { plannedSAP = value; }
        }

        private DateTime actualSAP;

        public DateTime ActualSAP
        {
            get { return actualSAP; }
            set { actualSAP = value; }
        }

        private DateTime plannedBDR;

        public DateTime PlannedBDR
        {
            get { return plannedBDR; }
            set { plannedBDR = value; }
        }

        private DateTime actualBDR;

        public DateTime ActualBDR
        {
            get { return actualBDR; }
            set { actualBDR = value; }
        }

        private DateTime plannedBDRReview;

        public DateTime PlannedBDRReview
        {
            get { return plannedBDRReview; }
            set { plannedBDRReview = value; }
        }

        private DateTime actualBDRReview;

        public DateTime ActualBDRReview
        {
            get { return actualBDRReview; }
            set { actualBDRReview = value; }
        }

        private DateTime plannedSAR;

        public DateTime PlannedSAR
        {
            get { return plannedSAR; }
            set { plannedSAR = value; }
        }

        private DateTime actualSAR;

        public DateTime ActualSAR
        {
            get { return actualSAR; }
            set { actualSAR = value; }
        }

        //days
        private int plannedGoLiveDays;

        public int PlannedGoLiveDays
        {
            get { return plannedGoLiveDays; }
            set { plannedGoLiveDays = value; }
        }

        private int actualGoLiveDays;

        public int ActualGoLiveDays
        {
            get { return actualGoLiveDays; }
            set { actualGoLiveDays = value; }
        }


        private int plannedFPFVDays;

        public int PlannedFPFVDays
        {
            get { return plannedFPFVDays; }
            set { plannedFPFVDays = value; }
        }

        private int actualFPFVDays;

        public int ActualFPFVDays
        {
            get { return actualFPFVDays; }
            set { actualFPFVDays = value; }
        }

        //private int plannedLPFVDays;

        //public int PlannedLPFVDays
        //{
        //    get { return plannedLPFVDays; }
        //    set { plannedLPFVDays = value; }
        //}

        //private int actualLPFVDays;

        //public int ActualLPFVDays
        //{
        //    get { return actualLPFVDays; }
        //    set { actualLPFVDays = value; }
        //}

        private int plannedLPLVDays;

        public int PlannedLPLVDays
        {
            get { return plannedLPLVDays; }
            set { plannedLPLVDays = value; }
        }

        private int actualLPLVDays;

        public int ActualLPLVDays
        {
            get { return actualLPLVDays; }
            set { actualLPLVDays = value; }
        }

        private int plannedDBLDays;

        public int PlannedDBLDays
        {
            get { return plannedDBLDays; }
            set { plannedDBLDays = value; }
        }

        private int actualDBLDays;

        public int ActualDBLDays
        {
            get { return actualDBLDays; }
            set { actualDBLDays = value; }
        }

        private int plannedCDISCSDTMDays;

        public int PlannedCDISCSDTMDays
        {
            get { return plannedCDISCSDTMDays; }
            set { plannedCDISCSDTMDays = value; }
        }

        private int actualCDISCSDTMDays;

        public int ActualCDISCSDTMDays
        {
            get { return actualCDISCSDTMDays; }
            set { actualCDISCSDTMDays = value; }
        }

        private int plannedSAPDays;

        public int PlannedSAPDays
        {
            get { return plannedSAPDays; }
            set { plannedSAPDays = value; }
        }

        private int actualSAPDays;

        public int ActualSAPDays
        {
            get { return actualSAPDays; }
            set { actualSAPDays = value; }
        }

        private int plannedBDRDays;

        public int PlannedBDRDays
        {
            get { return plannedBDRDays; }
            set { plannedBDRDays = value; }
        }

        private int actualBDRDays;

        public int ActualBDRDays
        {
            get { return actualBDRDays; }
            set { actualBDRDays = value; }
        }

        private int plannedBDRReviewDays;

        public int PlannedBDRReviewDays
        {
            get { return plannedBDRReviewDays; }
            set { plannedBDRReviewDays = value; }
        }

        private int actualBDRReviewDays;

        public int ActualBDRReviewDays
        {
            get { return actualBDRReviewDays; }
            set { actualBDRReviewDays = value; }
        }

        private int plannedSARDays;

        public int PlannedSARDays
        {
            get { return plannedSARDays; }
            set { plannedSARDays = value; }
        }

        private int actualSARDays;

        public int ActualSARDays
        {
            get { return actualSARDays; }
            set { actualSARDays = value; }
        }

        public bool AreActualDatesIncluded { get; set; }

        //Flags for Actuals
        public bool IsActualGoLiveSpecified { get; set; }
        public bool IsActualFPFVSpecified { get; set; }
        //public bool IsActualLPFVSpecified{ get; set; }
	    public bool IsActualLPLVSpecified{ get; set; }
	    public bool IsActualDBLSpecified{ get; set; }
	    public bool IsActualCDISCSDTMSpecified{ get; set; }
	    public bool IsActualSAPSpecified{ get; set; }
	    public bool IsActualBDRSpecified{ get; set; }
	    public bool IsActualBDRReviewSpecified{ get; set; }
        public bool IsActualSARSpecified { get; set; }

        public bool IsPlannedGoLiveSpecified { get; set; }
        public bool IsPlannedFPFVSpecified { get; set; }
        //public bool IsPlannedLPFVSpecified{ get; set; }
        public bool IsPlannedLPLVSpecified { get; set; }
        public bool IsPlannedDBLSpecified { get; set; }
        public bool IsPlannedCDISCSDTMSpecified { get; set; }
        public bool IsPlannedSAPSpecified { get; set; }
        public bool IsPlannedBDRSpecified { get; set; }
        public bool IsPlannedBDRReviewSpecified { get; set; }
        public bool IsPlannedSARSpecified { get; set; }

        public Milestones()
        {

        }

        public Milestones(string studyStat, 
                            DateTime plannedGoLive, DateTime actualGoLive,
                            DateTime plannedFPFV, DateTime actualFPFV,
                            //DateTime plannedLPFV, DateTime actualLPFV,
                            DateTime plannedLPLV, DateTime actualLPLV,
                            DateTime plannedDBL, DateTime actualDBL,
                            DateTime plannedCDISCSDTM, DateTime actualCDISCSDTM,
                            DateTime plannedSAP, DateTime actualSAP,
                            DateTime plannedBDR, DateTime actualBDR,
                            DateTime plannedBDRReview, DateTime actualBDRReview,
                            DateTime plannedSAR, DateTime actualSAR,
                            int plannedGoLiveDays, int actualGoLiveDays,
                            int plannedFPFVDays, int actualFPFVDays,
                            //int plannedLPFVDays, int actualLPFVDays,
                            int plannedLPLVDays, int actualLPLVDays,
                            int plannedDBLDays, int actualDBLDays,
                            int plannedCDISCSDTMDays, int actualCDISCSDTMDays,
                            int plannedSAPDays, int actualSAPDays,
                            int plannedBDRDays, int actualBDRDays,
                            int plannedBDRReviewDays, int actualBDRReviewDays,
                            int plannedSARDays, int actualSARDays, 
                            bool isActualGoLiveSpecified, 
                            bool isActualFPFVSpecified,
	                        //bool isActualLPFVSpecified,
	                        bool isActualLPLVSpecified,
	                        bool isActualDBLSpecified,
	                        bool isActualCDISCSDTMSpecified,
	                        bool isActualSAPSpecified,
	                        bool isActualBDRSpecified,
	                        bool isActualBDRReviewSpecified,
	                        bool isActualSARSpecified,
                            bool areActualDatesIncluded)
        {
            studyStatus = studyStat;

            //this.studyAssignedOnDate = studyAssignedOn;
            this.plannedGoLive = plannedGoLive;
            this.actualGoLive = actualGoLive;

            this.plannedFPFV = plannedFPFV;
            this.actualFPFV = actualFPFV;

            //this.plannedLPFV = plannedLPFV;
            //this.actualLPFV = actualLPFV;

            this.plannedLPLV = plannedLPLV;
            this.actualLPLV = actualLPLV;

            this.plannedDBL = plannedDBL;
            this.actualDBL = actualDBL;

            this.plannedCDISCSDTM = plannedCDISCSDTM;
            this.actualCDISCSDTM = actualCDISCSDTM;

            this.plannedSAP = plannedSAP;
            this.actualSAP = actualSAP;

            this.plannedBDR = plannedBDR;
            this.actualBDR = actualBDR;

            this.plannedBDRReview = plannedBDRReview;
            this.actualBDRReview = actualBDRReview;

            this.plannedSAR = plannedSAR;
            this.actualSAR = actualSAR;

            //Days
            this.plannedGoLiveDays = plannedGoLiveDays;
            this.actualGoLiveDays = actualGoLiveDays;

            this.plannedFPFVDays = plannedFPFVDays;
            this.actualFPFVDays = actualFPFVDays;

            //this.plannedLPFVDays = plannedLPFVDays;
            //this.actualLPFVDays = actualLPFVDays;

            this.plannedLPLVDays = plannedLPLVDays;
            this.actualLPLVDays = actualLPLVDays;

            this.plannedDBLDays = plannedDBLDays;
            this.actualDBLDays = actualDBLDays;

            this.plannedCDISCSDTMDays = plannedCDISCSDTMDays;
            this.actualCDISCSDTMDays = actualCDISCSDTMDays;

            this.plannedSAPDays = plannedSAPDays;
            this.actualSAPDays = actualSAPDays;

            this.plannedBDRDays = plannedBDRDays;
            this.actualBDRDays = actualBDRDays;

            this.plannedBDRReviewDays = plannedBDRReviewDays;
            this.actualBDRReviewDays = actualBDRReviewDays;

            this.plannedSARDays = plannedSARDays;
            this.actualSARDays = actualSARDays;
            //Flag for Actual
            this.IsActualGoLiveSpecified = isActualGoLiveSpecified;
            this.IsActualFPFVSpecified = isActualFPFVSpecified;
            //this.IsActualLPFVSpecified= isActualLPFVSpecified;
	        this.IsActualLPLVSpecified= isActualLPLVSpecified;
	        this.IsActualDBLSpecified=isActualDBLSpecified;
	        this.IsActualCDISCSDTMSpecified=isActualCDISCSDTMSpecified;
	        this.IsActualSAPSpecified=isActualSAPSpecified;
	        this.IsActualBDRSpecified=isActualBDRSpecified;
	        this.IsActualBDRReviewSpecified=isActualBDRReviewSpecified;
            this.IsActualSARSpecified = isActualSARSpecified;

            AreActualDatesIncluded = areActualDatesIncluded;
        }
    }
}
