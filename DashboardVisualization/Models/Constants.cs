﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DashboardVisualization.Models
{
    public static class Constants
    {
        public const string ProductName = "Dashboard Visualization";


        public static string CurrentDirPath = Directory.GetCurrentDirectory();      //Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"..\"));
       
        
        #region workspace declaration
        public static string WorkSpaceFolderPath = Path.Combine(CurrentDirPath, "WorkSpaces");
        public const string WorkSpaceTmplate = "WorkSpace_Template.xlsx";
        public static string WorkSpaceTmplatePath = Path.Combine(Constants.WorkSpaceFolderPath, "WorkSpace_Template.xlsx");
        public const string ShtProjData = "ProjectData";
        public const string ShtOutputDet = "OutputDetails";
        public static string WorkSpceFilePath = string.Empty;
        public static string ExistingWrkspace = string.Empty;
        #endregion

        #region engine declaration
        public const string EngineFilename = "_Engine.xlsm";
        public static string EngineFolderPath = Path.Combine(CurrentDirPath, "Engine", EngineFilename);
        #endregion

        public static string MainFormData = string.Empty;
        public const string Seperator = "###";
        public const string NewLineChar = "\n";
        public static string[] SelctedProjects;
        public static bool flgPortfolio = false;


        public static int projIdCol = 2;
        //Header Row
        public static int Header_Row = 2;

        #region form tabs

        public const string tbProjectDetails = "Project Details";
        public const string tbStudyInfo = "Study Info";

        #endregion

        #region grid columns
        public static class ADGVGridColumns
        {
            //public static string Select = "Select";
            //public static string ProjectName = "Project name";
            //public static string CSLName = "CSL Name";
            //public static string GCLName = "GCL Name";
            //public static string CSSName = "CSS Name";

            public static string DMAwarded = "Data Management Awarded";
            public static string DMPOC = "Data Management POC";
            public static string CDISC_Awarded = "CDISC Awarded";
            public static string CDISC_POC = "CDISC POC";
            public static string Stat_Awarded = "Statistics Awarded";
            public static string Stat_POC = "Statistics POC";

            public static string ProgAssignedOn = "Project Assigned On";

            public static string GoLivePlanned = "Go-Live Planned";
            public static string GoLiveActual = "Go-Live Actual";
            public static string FPFVPlanned = "FPFV Planned";
            public static string FPFVActual = "FPFV Actual";
            public static string LPFVPlanned = "LPFV Planned";
            public static string LPFVActual = "LPFV Actual";

            public static string StudyDuration = "Study Duration in days";

            public static string LPLVPlanned = "LPLV Planned";
            public static string LPLVActual = "LPLV Actual";
            public static string DBLPlanned = "DBL Planned";
            public static string DBLActual = "DBL Actual";
            public static string CDISCPlanned = "CDISC/SDTM Planned";
            public static string CDISCActual = "CDISC/SDTM Actual";
            public static string SAPPlanned = "SAP Planned";
            public static string SAPActual = "SAP Actual";
            public static string BDRPlanned = "BDR Planned";
            public static string BDRActual = "BDR Actual";
            public static string SARPlanned = "SAR Planned";
            public static string SARActual = "SAR Actual";
            //Added by Chetna  Date:23-Mar-2021
            public static string Client_Comments = "Client Comments";
            public static string Cytel_Comments = "Cytel Comments";
        }

        public enum UCGridColumns
        {
            [Description("Select")]
            Select = 0,
            [Description("Project name")]
            ProjectName,
            [Description("CSL Name")]
            CSLName,
             [Description("GCL Name")]
            GCLName,
            [Description("CSS Name")]
             CSSName,
            
            //public static string DMAwarded = "Data Management Awarded";
            //public static string DMPOC = "Data Management POC";
            //public static string CDISC_Awarded = "CDISC Awarded";
            //public static string CDISC_POC = "CDISC POC";
            //public static string Stat_Awarded = "Statistics Awarded";
            //public static string Stat_POC = "Statistics POC";

            //public static string ProgAssignedOn = "Project Assigned On";

            //public static string GoLivePlanned = "Go-Live Planned";
            //public static string GoLiveActual = "Go-Live Actual";
            //public static string FPFVPlanned = "FPFV Planned";
            //public static string FPFVActual = "FPFV Actual";
            //public static string LPFVPlanned = "LPFV Planned";
            //public static string LPFVActual = "LPFV Actual";

            //public static string StudyDuration = "Study Duration in days";

            //public static string LPLVPlanned = "LPLV Planned";
            //public static string LPLVActual = "LPLV Actual";
            //public static string DBLPlanned = "DBL Planned";
            //public static string DBLActual = "DBL Actual";
            //public static string CDISCPlanned = "CDISC/SDTM Planned";
            //public static string CDISCActual = "CDISC/SDTM Actual";
            //public static string SAPPlanned = "SAP Planned";
            //public static string SAPActual = "SAP Actual";
            //public static string BDRPlanned = "BDR Planned";
            //public static string BDRActual = "BDR Actual";
            //public static string SARPlanned = "SAR Planned";
            //public static string SARActual = "SAR Actual";
        }
        #endregion

        #region extension method for enum
        public static string GetEnumDescription(this UCGridColumns enumValue)
        {
            var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());

            var descriptionAttributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return descriptionAttributes.Length > 0 ? descriptionAttributes[0].Description : enumValue.ToString();
        }
        #endregion

    }
}
