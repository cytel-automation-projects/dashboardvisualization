﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;

namespace DashboardVisualization.Models
{
    public class EngineInvoker
    {

        public static bool RunTemplate()
        {
            Microsoft.Office.Interop.Excel.Application ExcelApp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook ExcelWorkBook = ExcelApp.Workbooks.Open(Constants.EngineFolderPath);
            object misValue = System.Reflection.Missing.Value;

            try
            {
                ExcelApp.DisplayAlerts = false;
                ExcelApp.ScreenUpdating = false;
                ExcelApp.Visible = false;
                ExcelWorkBook = ExcelApp.Workbooks.Open(Constants.EngineFolderPath);
                ExcelApp.ActiveWindow.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMinimized;


                int fileIndex = -1;
                List<string> inputFiles = new List<string>();


                inputFiles = new List<string>() { Constants.WorkSpceFilePath };

                if (CheckFileAvailability(inputFiles, out fileIndex))
                {
                    ExcelApp.Run("GeneratePortfolio", Constants.WorkSpceFilePath, Constants.SelctedProjects, Constants.flgPortfolio);
                }
                else
                    MessageBox.Show("Path missing for required file: " + inputFiles[fileIndex], Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);


                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                ExcelWorkBook.Close(false, misValue, misValue);
                ExcelApp.Quit();
                if (ExcelWorkBook != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelWorkBook); }
                if (ExcelApp != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelApp); }
            }
        }


        private static bool CheckFileAvailability(List<string> inputFiles, out int fileIndex)
        {
            fileIndex = -1;

            if (inputFiles.Count == 0) return true;

            int index = 0;
            foreach (var file in inputFiles)
            {
                if (string.IsNullOrWhiteSpace(file))
                {
                    fileIndex = index;
                    return false;
                }

                index++;
            }
            return true;
        }


        public static bool LoadWorkspace(string filepath)
        {
            int rowCount = 0;
            bool inputsValid = true;

            try
            {
                Microsoft.Office.Interop.Excel.Application ExcelApp = new Microsoft.Office.Interop.Excel.Application();
                ExcelApp.DisplayAlerts = false;
                object misValue = System.Reflection.Missing.Value;
                ExcelApp.Visible = false;
                Microsoft.Office.Interop.Excel.Workbook ExcelWorkBook = ExcelApp.Workbooks.Open(filepath);
                ExcelApp.ActiveWindow.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMinimized;

                //_*_*_*_*_Checking for sheet io*_*_*_*_*_**_*

                //Get the active worksheet object of sheet project data
                Worksheet excelSheet = ExcelWorkBook.Worksheets[Constants.ShtProjData];

                //This will return the whole range where you data is used in it
                //Get the used Range of 
                Microsoft.Office.Interop.Excel.Range usedRange1 = excelSheet.UsedRange;
                rowCount = usedRange1.Rows.Count;

                //check if selected file is not blank 
                if (2 <= rowCount)
                {
                    inputsValid = true;
                }
                else
                {
                    inputsValid = false;
                }

                ExcelWorkBook.Close(false, misValue, misValue);
                ExcelApp.Quit();
                if (ExcelWorkBook != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelWorkBook); }
                if (ExcelApp != null) { System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelApp); }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, Constants.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return false;
            }
           
            return inputsValid;

        }

    }
}
