﻿namespace DashboardVisualization.Models
{
    public class ProjectDetails
    {
        public ProjectDescription ProjectDescription;

        public StudyInfo StudyInfo;

        public Milestones Milestones;

        public Comments Comments;
    }
}
