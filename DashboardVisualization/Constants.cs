﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;

namespace DashboardVisualization.Models
{
    public static class Constants
    {
        public const string ProductName = "Dashboard Visualization";

        public static string CurrentDirPath = Process.GetCurrentProcess().MainModule.FileName.Substring(0, Process.GetCurrentProcess().MainModule.FileName.LastIndexOf("\\"));
        //commented by Priya
        //If we create shortcut using relative path below statement will not work due to windir mentioned in the path
        //so above statement gives the path of current running dir folder path
        //public static string CurrentDirPath = Directory.GetCurrentDirectory();      //Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"..\"));
       
        public static ProjectDetails ProjectDetails;

        public static Dictionary<string, bool> ColumnVisibilityStates;

        #region workspace declaration
        public static string WorkSpaceFolderPath = Path.Combine(CurrentDirPath, "WorkSpaces");
        public static string OutputFolderPath = Path.Combine(CurrentDirPath, "Outputs");
        public const string WorkSpaceTemplate = "WorkSpace_Template.xlsx";
        public static string WorkSpaceTemplatePath = Path.Combine(Constants.WorkSpaceFolderPath, "WorkSpace_Template.xlsx");
        public const string ShtProjData = "ProjectData";
        public const string ShtTemplate = "Template";
        public static string WorkSpaceFilePath = string.Empty;
        public static string ImportFilePath = string.Empty;

        //public static string ExistingWrkspace = string.Empty;
        #endregion

        #region engine declaration
        public const string EngineFilename = "_Engine.xlsm";
        public static string EngineFolderPath = Path.Combine(CurrentDirPath, "Engine", EngineFilename);
        #endregion

        public static string MainFormData = string.Empty;
        //public static int studyDurationValue = 0;
        public static bool flgDtPicker = true;  // at the time of edit mode value fetech gives crash for calculations of dates so added flag here
        public static bool ValidFormData = false;
        public const string Seperator = "###";
        public const string NewLineChar = "\n";
        public const string DateFormat = "dd-MMM-yy";
        public const string DefaultDate = "01-Jan-1990";
        public static int ProjectIdCol = 2;
        //Header Row
        public static int Header_Row = 1; //2

        public static int DefaultNoOfDays = 2;

        //public static string[] SelctedProjectsID;               //selected project IDS
        //      public static bool RenderPortfolioRequested = false;  
        public static int GridColumnCount = 27; //30;     // Count of columns displayed in grid
        public static int FirstDataRowIndex = 2; //3    // Index of first row, in project workspace workbook, containing project data 

        #region form tabs

        public const string TabProjectDetails = "Project Details";
        public const string TabStudyInfo = "Study/Personnel Info";

        #endregion

        #region grid columns
        public static class GridColumns
        {
            public static string Select = "Select";
            public static string ProjectName = "Project Name";
            //public static string CSLName = "CSL Name";
            //public static string GCLName = "GCL Name";
            //public static string CSSName = "CSS Name";

            public static string DMAwarded = "Data Management Awarded";
            public static string DMPOC = "Data Management POC";
            public static string CDISC_Awarded = "CDISC Awarded";
            public static string CDISC_POC = "CDISC POC";
            public static string Stat_Awarded = "BSP Awarded";
            public static string Stat_POC = "BSP POC";
            public static string ProgAssignedOn = "Project Assigned On";
            public static string GoLivePlanned = "Go-Live Planned";
            public static string GoLiveActual = "Go-Live Actual";
            public static string FPFVPlanned = "FPFV Planned";
            public static string FPFVActual = "FPFV Actual";
            //public static string LPFVPlanned = "LPFV Planned";
            //public static string LPFVActual = "LPFV Actual";
            //public static string StudyDuration = "Study Duration in days";
            public static string LPLVPlanned = "LPLV Planned";
            public static string LPLVActual = "LPLV Actual";
            public static string DBLPlanned = "DBL Planned";
            public static string DBLActual = "DBL Actual";
            public static string CDISCPlanned = "CDISC/SDTM Planned";
            public static string CDISCActual = "CDISC/SDTM Actual";
            public static string SAPPlanned = "SAP Planned";
            public static string SAPActual = "SAP Actual";
            public static string BDRPlanned = "BDR Planned";
            public static string BDRActual = "BDR Actual";
            public static string SARPlanned = "SAR Planned";
            public static string SARActual = "SAR Actual";
            public static string Client_Comments = "Client Comments";
            public static string Cytel_Comments = "Cytel Comments";
        }

        public enum WorkspaceFields
        {
            [Description("Serial No")]
            Select = 1,

            [Description("Project name")]
            ProjectName,

            [Description("Category")]
            ProjectCategory,

            [Description("Description")]
            ProjectDescription,

            [Description("CSL Name")]
            CSLName,

            [Description("GCL Name")]
            GCLName,

            [Description("CSS Name")]
            CSSName,


            [Description("CRO")]
            CROName,

            [Description("Country")]
            CountryName,

            [Description("Study Status")]
            StudyStatus,

            [Description("Data Management Awarded")]
            DMAwarded,

            [Description("Data Management POC")]
            DMPOC,

            [Description("CDISC Awarded")]
            CDISC_Awarded,

            [Description("CDISC POC")]
            CDISC_POC,

            [Description("Statistics Awarded")]
            Stat_Awarded,

            [Description("Statistics POC")]
            Stat_POC,

            [Description("Project Assigned On")]
            ProgAssignedOn,

            [Description("Go-Live Planned")]
            GoLivePlanned,

            [Description("Go-Live Actual")]
            GoLiveActual,

            [Description("FPFV Planned")]
            FPFVPlanned,

            [Description("FPFV Actual")]
            FPFVActual,

            //[Description("LPFV Planned")]
            //LPFVPlanned,

            //[Description("LPFV Actual")]
            //LPFVActual,

            //[Description("Study Duration in days")]
            //StudyDuration,

            [Description("LPLV Planned")]
            LPLVPlanned,

            [Description("LPLV Actual")]
            LPLVActual,

            [Description("DBL Planned")]
            DBLPlanned,

            [Description("DBL Actual")]
            DBLActual,

            [Description("CDISC/SDTM Planned")]
            CDISCPlanned,

            [Description("CDISC/SDTM Actual")]
            CDISCActual,

            [Description("SAP Planned")]
            SAPPlanned,

            [Description("SAP Actual")]
            SAPActual,

            [Description("BDR Planned")]
            BDRPlanned,

            [Description("BDR Actual")]
            BDRActual,

            [Description("BDR Review Planned")]
            BDRReviewPlanned,

            [Description("BDR Review Actual")]
            BDRReviewActual,

            [Description("SAR Planned")]
            SARPlanned,

            [Description("SAR Actual")]
            SARActual,

            [Description("Client Comments")]
            Client_Comments,

            [Description("Cytel Comments")]
            Cytel_Comments,

            //Days columns
            [Description("Go-Live Planned Days")]
            GoLivePlannedDays,

            [Description("Go-Live Actual Days")]
            GoLiveActualDays,

            [Description("FPFV Planned Days")]
            FPFVPlannedDays,

            [Description("FPFV Actual Days")]
            FPFVActualDays,

            //[Description("LPFV Planned Days")]
            //LPFVPlannedDays,

            //[Description("LPFV Actual Days")]
            //LPFVActualDays,

            [Description("LPLV Planned Days")]
            LPLVPlannedDays,

            [Description("LPLV Actual Days")]
            LPLVActualDays,

            [Description("DBL Planned Days")]
            DBLPlannedDays,

            [Description("DBL Actual Days")]
            DBLActualDays,

            [Description("CDISC/SDTM Planned Days")]
            CDISCPlannedDays,

            [Description("CDISC/SDTM Actual Days")]
            CDISCActualDays,

            [Description("SAP Planned Days")]
            SAPPlannedDays,

            [Description("SAP Actual Days")]
            SAPActualDays,

            [Description("BDR Planned Days")]
            BDRPlannedDays,

            [Description("BDR Actual Days")]
            BDRActualDays,

            [Description("BDR Review Planned Days")]
            BDRReviewPlannedDays,

            [Description("BDR Actual Days")]
            BDRReviewActualDays,

            [Description("SAR Planned Days")]
            SARPlannedDays,

            [Description("SAR Actual Days")]
            SARActualDays,

            [Description("Actual Dates Included")]
            ActualDatesIncluded
        }
        #endregion

        #region extension method for enum
        public static string GetEnumDescription(this WorkspaceFields enumValue)
        {
            var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());

            var descriptionAttributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return descriptionAttributes.Length > 0 ? descriptionAttributes[0].Description : enumValue.ToString();
        }
        #endregion

        #region Control names and help text
        public const string ControlName_btnAddProject = "btnAddProject";
        public const string ControlName_btnEditProject = "btnEditProject";
        public const string ControlName_btnProjDashboard = "btnProjDashboard";
        public const string ControlName_btnProjPortfolio = "btnProjPortfolio";
        public const string ControlName_btnShowHideCols = "btnShowHideCols";
        public const string ControlName_advancedDataGridView1 = "advancedDataGridView1";
        public const string ControlName_btnOpen = "btnOpen";

        public const string HelpText_btnAddProject = "Click this button to add a new project to current workspace.";
        public const string HelpText_btnEditProject = "Click this button to edit project selected in table.";
        public const string HelpText_btnProjDashboard = "Click this button to generate project dashboard for selected project(s).";
        public const string HelpText_btnProjPortfolio = "Click this button to generate project portfolio.";
        public const string HelpText_btnShowHideCols = "Click this button to show or hide columns in table.";
        public const string HelpText_advancedDataGridView1 = "This table displays all projects from current workspace.";
        public const string HelpText_btnOpen = "Click this button to open a generated graph file.";

        //UC Main - Add project
        public const string ControlName_txtProjectName = "txtProjectName";
        public const string ControlName_txtCategory = "txtCategory";
        public const string ControlName_txtDescription = "txtDescription";
        public const string ControlName_txtStudyDuration = "txtStudyDuration";

        public const string HelpText_txtProjectName = "Specify a valid name for project.";
        public const string HelpText_txtCategory = "Specify category for this project.";
        public const string HelpText_txtDescription = "Specify a brief description about this project.";
        public const string HelpText_txtStudyDuration = "Specify study duration in days for this project.";

        //study/Personnel Info----
        public const string ControlName_dtAssignedOn = "dtAssignedOn";
        public const string ControlName_txtCSLName = "txtCSLName";
        public const string ControlName_txtGCLName = "txtGCLName";
        public const string ControlName_txtCSSName = "txtCSSName";
        public const string ControlName_txtCROName = "txtCRO";
        public const string ControlName_txtCountryName = "txtCountry";
        public const string ControlName_chkDataManagement = "chkDataManagement";
        public const string ControlName_txtDataManagement = "txtDataManagement";
        public const string ControlName_chkCDISC = "chkCDISC";
        public const string ControlName_txtCDISC = "txtCDISC";
        public const string ControlName_chkStatisics = "chkStatisics";
        public const string ControlName_txtStatisics = "txtStatisics";

        public const string HelpText_dtAssignedOn = "Specify a date on which this Study has been assigned.";
        public const string HelpText_txtCSLName = "Specify name of a person designated as CSL.";
        public const string HelpText_txtGCLName = "Specify name of a person designated as GCL.";
        public const string HelpText_txtCSSName = "Specify name of a person designated as CSS.";
        public const string HelpText_txtCROName = "Specify CRO.";
        public const string HelpText_txtCountryName = "Specify Country name.";
        public const string HelpText_chkDataManagement = "Please select the check box if Data Management is awarded.";
        public const string HelpText_txtDataManagement = "Specify name of a person alloted as Data Management PoC.";
        public const string HelpText_chkCDISC = "Please select the check box if CDISC is awarded";
        public const string HelpText_txtCDISC = "Specify name of a person alloted as CDISC PoC.";
        public const string HelpText_chkStatisics = "Please select the check box if Statistics is awarded.";
        public const string HelpText_txtStatisics = "Specify name of a person alloted as a Statisics PoC.";

        //Milestones---
        public const string ControlName_cmbStudyStatus = "cmbStudyStatus";
        //public const string ControlName_chkDefaultDays = "chkDefaultDays";
        //public const string ControlName_txtDefaultDays = "txtDefaultDays";
        public const string ControlName_dtPlanedGoLive = "dtPlanedGoLive";
        public const string ControlName_txtPlanedGoLive = "txtPlanedGoLive";
        public const string ControlName_dtActualGoLive = "dtActualGoLive";
        public const string ControlName_txtActualGoLive = "txtActualGoLive";
        public const string ControlName_dtPlannedFPFV = "dtPlannedFPFV";
        public const string ControlName_txtPlanedFPFV = "txtPlanedFPFV";
        public const string ControlName_dtActualFPFV = "dtActualFPFV";
        public const string ControlName_textActualFPFV = "textActualFPFV";

        public const string ControlName_dtPlannedLPFV = "dtPlannedLPFV";
        public const string ControlName_txtPlanedLPFV = "txtPlanedLPFV";
        public const string ControlName_dtActualLPFV = "dtActualLPFV";
        public const string ControlName_textActualLPFV = "textActualLPFV";

        public const string ControlName_dtPlannedLPLV = "dtPlannedLPLV";
        public const string ControlName_txtPlanedLPLV = "txtPlanedLPLV";
        public const string ControlName_dtActualLPLV = "dtActualLPLV";
        public const string ControlName_textActualLPLV = "textActualLPLV";

        public const string ControlName_dtPlannedDBL = "dtPlannedDBL";
        public const string ControlName_txtPlanedDBL = "txtPlanedDBL";
        public const string ControlName_dtActualDBL = "dtActualDBL";
        public const string ControlName_textActualDBL = "textActualDBL";

        public const string ControlName_dtPlanedSDTM = "dtPlanedSDTM";
        public const string ControlName_txtPlanedSDTM = "txtPlanedSDTM";
        public const string ControlName_dtActualSDTM = "dtActualSDTM";
        public const string ControlName_textActualSDTM = "textActualSDTM";

        public const string ControlName_dtPlanedSAP = "dtPlanedSAP";
        public const string ControlName_txtPlanedSAP = "txtPlanedSAP";
        public const string ControlName_dtActualSAP = "dtActualSAP";
        public const string ControlName_textActualSAP = "textActualSAP";

        public const string ControlName_dtPlanedBDR = "dtPlanedBDR";
        public const string ControlName_txtPlanedBDR = "txtPlanedBDR";
        public const string ControlName_dtActualBDR = "dtActualBDR";
        public const string ControlName_textActualBDR = "textActualBDR";

        public const string ControlName_dtPlanedBDRReview = "dtPlanedBDRReview";
        public const string ControlName_txtPlanedBDRReview = "txtPlanedBDRReview";
        public const string ControlName_dtActualBDRReview = "dtActualBDRReview";
        public const string ControlName_textActualBDRReview = "textActualBDRReview";

        public const string ControlName_dtPlanedSAR = "dtPlanedSAR";
        public const string ControlName_txtPlanedSAR = "txtPlanedSAR";
        public const string ControlName_dtActualSAR = "dtActualSAR";
        public const string ControlName_textActualSAR = "textActualSAR";

        //HELP TEXT Milestones---
        public const string HelpText_cmbStudyStatus = "Specify Study status";
        //public const string HelpText_chkDefaultDays = "Select this check box to have default no. of days set for all activities below.";
        //public const string HelpText_DefaultDays = "Enter default no. of days to be set for all activities.";
        public const string HelpText_textBox2 = "Specify default no. of days, which will be set as a default duration for all stages below.";
        public const string HelpText_dtPlanedGoLive = "Specify a date on which GoLive activity is planned to start.";
        public const string HelpText_txtPlanedGoLive = "Specify number of days that may be required to complete this activity.";
        public const string HelpText_dtActualGoLive = "Specify an actual date on which GoLive activity started.";
        public const string HelpText_txtActualGoLive = "Specify actual number of days required to complete this activity.";

        public const string HelpText_dtPlannedFPFV = "Specify a date on which FPFV activity is planned to start.";
        public const string HelpText_txtPlanedFPFV = "Specify number of days that may be required to complete this activity.";
        public const string HelpText_dtActualFPFV = "Specify an actual date on which FPFV activity started.";
        public const string HelpText_textActualFPFV = "Specify actual number of days required to complete this activity.";

        public const string HelpText_dtPlannedLPFV = "Specify a date on which LPFV activity is planned to start.";
        public const string HelpText_txtPlanedLPFV = "Specify number of days that may be required to complete this activity.";
        public const string HelpText_dtActualLPFV = "Specify an actual date on which LPFV activity started.";
        public const string HelpText_textActualLPFV = "Specify actual number of days required to complete this activity.";

        public const string HelpText_dtPlannedLPLV = "Specify a date on which LPLV activity is planned to start.";
        public const string HelpText_txtPlanedLPLV = "Specify number of days that may be required to complete this activity.";
        public const string HelpText_dtActualLPLV = "Specify an actual date on which LPLV activity started.";
        public const string HelpText_textActualLPLV = "Specify actual number of days required to complete this activity.";

        public const string HelpText_dtPlannedDBL = "Specify a date on which DBL activity is planned to start.";
        public const string HelpText_txtPlanedDBL = "Specify number of days that may be required to complete this activity.";
        public const string HelpText_dtActualDBL = "Specify an actual date on which DBL activity started.";
        public const string HelpText_textActualDBL = "Specify actual number of days required to complete this activity.";

        public const string HelpText_dtPlanedSDTM = "Specify a date on which SDTM activity is planned to start.";
        public const string HelpText_txtPlanedSDTM = "Specify number of days that may be required to complete this activity.";
        public const string HelpText_dtActualSDTM = "Specify an actual date on which SDTM activity started.";
        public const string HelpText_textActualSDTM = "Specify actual number of days required to complete this activity.";

        public const string HelpText_dtPlanedSAP = "Specify a date on which SAP activity is planned to start.";
        public const string HelpText_txtPlanedSAP = "Specify number of days that may be required to complete this activity.";
        public const string HelpText_dtActualSAP = "Specify an actual date on which SAP activity started.";
        public const string HelpText_textActualSAP = "Specify actual number of days required to complete this activity.";

        public const string HelpText_dtPlanedBDR = "Specify a date on which BDR activity is planned to start.";
        public const string HelpText_txtPlanedBDR = "Specify number of days that may be required to complete this activity.";
        public const string HelpText_dtActualBDR = "Specify an actual date on which BDR activity started.";
        public const string HelpText_textActualBDR = "Specify actual number of days required to complete this activity.";

        public const string HelpText_dtPlanedBDRReview = "Specify a date on which BDR Review activity is planned to start.";
        public const string HelpText_txtPlanedBDRReview = "Specify number of days that may be required to complete this activity.";
        public const string HelpText_dtActualBDRReview = "Specify an actual date on which BDR Review activity started.";
        public const string HelpText_textActualBDRReview = "Specify actual number of days required to complete this activity.";

        public const string HelpText_dtPlanedSAR = "Specify a date on which SAR activity is planned to start.";
        public const string HelpText_txtPlanedSAR = "Specify number of days that may be required to complete this activity.";
        public const string HelpText_dtActualSAR = "Specify an actual date on which SAR activity started.";
        public const string HelpText_textActualSAR = "Specify actual number of days required to complete this activity.";

        public const string ControlName_txtClientComment = "txtClientComment";
        public const string HelpText_txtClientComment = "Specify comments from the Client here.";
        public const string ControlName_txtCytelComment = "txtCytelComment";
        public const string HelpText_txtCytelComment = "Specify comment from Cytel here.";

        //Login
        public const string ControlName_rdbLoadExisting = "rdbLoadExisting";
        public const string ControlName_txtExistingPath = "txtExistingPath";
        public const string ControlName_btnBrowseExisting = "btnBrowseExisting";
        public const string ControlName_rdbCreateNewWrkspc = "rdbCreateNewWrkspc";
        public const string ControlName_txtClient = "txtClient";

        public const string HelpText_rdbLoadExisting = "Select this option to select an existing workspace.";
        public const string HelpText_txtExistingPath = "Displays selected workspace's full path.";
        public const string HelpText_btnBrowseExisting = "Click this button to browse and select an existing workspace.";
        public const string HelpText_rdbCreateNewWrkspc = "Select this option to specify a new workspace.";
        public const string HelpText_txtClient = "Specify a name for new workspace to be created.";

        #endregion

        public enum StudyDurationUnit
        {
            Days = 0,
            Months,
            Years
        }

        
    }

    /// <summary>
    /// extension method for addition of working days
    /// </summary>
    public static class DateTimeExtensions
    {
        public static DateTime AddWorkdays(this DateTime originalDate, int workDays)
        {
            DateTime tmpDate = originalDate;
            while (workDays > 0)
            {
                tmpDate = tmpDate.AddDays(1);
                if (tmpDate.DayOfWeek < DayOfWeek.Saturday &&
                    tmpDate.DayOfWeek > DayOfWeek.Sunday)
                    workDays--;
            }
            return tmpDate;
        }
    }
}
