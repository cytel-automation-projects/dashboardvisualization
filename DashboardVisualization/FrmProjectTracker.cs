﻿using DashboardVisualization.Containers;
using DashboardVisualization.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace DashboardVisualization
{
    public partial class FrmProjTracker : Form
    {
        public FrmProjTracker()
        {
            InitializeComponent();

            Constants.ColumnVisibilityStates = new Dictionary<string, bool>();
           
            Type type = typeof(Constants.GridColumns);

            foreach (var field in type.GetFields()) // (System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic))
            {
                var v = field.GetValue(null); // static classes cannot be instanced, so use null...

                Constants.ColumnVisibilityStates.Add(v.ToString(), true);
            }

            //set file name to label
            string FilePath_Wrokspace = Path.GetFileName(Constants.WorkSpaceFilePath);
            lblWorkSpceName.Text = FilePath_Wrokspace;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you want to close this form and return to the login form?",  Constants.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            //ucProjectTracker.WriteGridDataIntoExcel();
            if (result == DialogResult.Yes)
            {               
                this.Hide();

                // open main form
                FrmLogin mainForm = new FrmLogin();
                mainForm.ShowDialog();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

        }

        private void FrmProjTracker_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(Constants.WorkSpaceFilePath))
            {
                //ucProjectTracker.LoadExcelData();
                ucProjectTracker.LoadExistingWorkspace();
            }
        }
    }
}
